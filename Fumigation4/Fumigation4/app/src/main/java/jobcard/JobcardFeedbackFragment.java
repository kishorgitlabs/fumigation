package jobcard;


import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import com.brainmagic.fumigation.AttendanceViewNewActivity;
import com.brainmagic.fumigation.ContainerDetailsActivity;
import com.brainmagic.fumigation.CustomViewPager;
import com.brainmagic.fumigation.R;
import com.brainmagic.fumigation.ViewReportActivity;
import com.brainmagic.fumigation.ViewReportFilesFragment;
import com.brainmagic.fumigation.VisitReportFragment;
import com.google.zxing.integration.android.IntentIntegrator;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import logout.Logout;

public class JobcardFeedbackFragment extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    CustomViewPager viewPager;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ImageView menu;
    private  String value;
    ViewReportFilesFragment v;
    private LinearLayout containertab;
    private String containernumber,chemicallist,Quantitylist;
    private int cusid;
    private String jobcarid;
     BasicFragment basicFragment;
     AddFilesFragment addFilesFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobcard_feedback_fragment);
        menu=(ImageView)findViewById(R.id.menu);
        containertab=(LinearLayout) findViewById(R.id.containertab);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        containernumber = getIntent().getStringExtra("containernumber");
        chemicallist = myshare.getString("chemicals", "");
        Quantitylist = myshare.getString("quantity", "");
        cusid = myshare.getInt("id", 0);
        jobcarid = getIntent().getStringExtra("jobcardod");
        basicFragment=new BasicFragment();
        addFilesFragment=new AddFilesFragment();
        basicFragment.value(containernumber);
        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(JobcardFeedbackFragment.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(JobcardFeedbackFragment.this).log_out();
                                return true;
                            case R.id.jobcard:
                                Intent i=new Intent(getApplicationContext(), ContainerDetailsActivity.class);
                                startActivity(i);
                                return true;
                            case R.id.visitreport:
                                Intent i4=new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i4);
                                return true;
                            case R.id.attendance:
                                Intent i7=new Intent(getApplicationContext(), AttendanceViewNewActivity.class);
                                startActivity(i7);
                                return true;
                            case R.id.viewvisit:
                                Intent i6=new Intent(getApplicationContext(), ViewReportActivity.class);
                                startActivity(i6);
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menu);
                popupMenu.show();
                ;
            }
        });
        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.setPagingEnabled(false);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
//        LinearLayout l=((LinearLayout)tabLayout.getChildAt(0));
//        l.setEnabled(false);
//        for(int i=0;i< l.getChildCount();i++){
//            l.getChildAt(i).setClickable(false);
//        }
    }

    private void setupViewPager(ViewPager viewPager) {
       ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(basicFragment,"Jobcard");
        adapter.addFragment(addFilesFragment, "Add Files");

        viewPager.setAdapter(adapter);

    }


    public void next() {
        viewPager.setCurrentItem(getItemofviewpager(+1), true);
        //movew_Forward();
    }
    private int getItemofviewpager(int i) {
        return viewPager.getCurrentItem() + i;
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    private class ScreenSlidePagerAdapter extends ViewPagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }
    }
}
