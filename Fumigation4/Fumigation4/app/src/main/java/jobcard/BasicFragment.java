package jobcard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.brainmagic.fumigation.AnyOrientationCaptureActivity;
import com.brainmagic.fumigation.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;
import uploadService.ProgressRequestBody;

import static android.content.ContentValues.TAG;

public class BasicFragment extends Fragment{
    private String containerno,selectreport,selectchemicals1,selectchemicals,serialno;
    private EditText edcontainer,quantityt,serialnumber;
    private MaterialSpinner edspinner,chemicalssp;
    private List<String> worklist,chemicallist1;
    private Button Addchemicals,add1,scanning2,scanning;
    private LinearLayout addllinear,scanninglin;
    private ScrollView scrollView1;
    private IntentIntegrator qrScan;
    private Context context;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_basic_fragment2, container, false);
        qrScan = new IntentIntegrator(getActivity());
        edcontainer=(EditText)rootView.findViewById(R.id.edcont);
        edspinner = (MaterialSpinner)rootView. findViewById(R.id.edspinner);
        chemicalssp = (MaterialSpinner)rootView. findViewById(R.id.chemicalssp);
        quantityt=(EditText)rootView.findViewById(R.id.quantityt);
        serialnumber=(EditText)rootView.findViewById(R.id.serialnumber);
        Addchemicals=(Button) rootView.findViewById(R.id.addchemicals);
        scanninglin=(LinearLayout)rootView.findViewById(R.id.scanninglin);
        add1=(Button) rootView.findViewById(R.id.add1);
        addllinear=(LinearLayout) rootView.findViewById(R.id.addllinear);
        scanning = (Button)rootView. findViewById(R.id.scanning);
        scanning2 = (Button)rootView. findViewById(R.id.scanning1);
        scrollView1 = (ScrollView) rootView.findViewById(R.id.scrollView1);
        edcontainer.setText(containerno);
        //worklist
        worklist=new ArrayList<>();
        worklist.add("Select WorkReport");
        worklist.add("ND");
        worklist.add("FUMI");
        worklist.add("SPARYING");
        edspinner.setItems(worklist);
        //chemicallist
        chemicallist1 =new ArrayList<>();
        chemicallist1.add("Select Chemicals");
        chemicallist1.add("Phostoxin");
        chemicallist1.add("Talunex Tablets");
        chemicallist1.add("Topex Applicator");
        chemicalssp.setItems(chemicallist1);
        chemicalssp.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectchemicals = item.toString();
            }
        });
        scanning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrScan.setBeepEnabled(true);
                qrScan.setOrientationLocked(false);
                qrScan.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                qrScan.setCaptureActivity(AnyOrientationCaptureActivity.class);
                qrScan.initiateScan();
            }

        });
        scanning2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrScan.setBeepEnabled(true);
                qrScan.setOrientationLocked(false);
                qrScan.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                qrScan.setCaptureActivity(AnyOrientationCaptureActivity.class);
                qrScan.initiateScan();
            }

        });
        edspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectreport = item.toString();
                switch (selectreport) {
                    case "ND":
                        addllinear.setVisibility(View.VISIBLE);
                        serialnumber.setVisibility(View.GONE);
                        scanning2.setVisibility(View.GONE);
                        break;
                    case "FUMI":

                        addllinear.setVisibility(View.VISIBLE);
                        serialnumber.setVisibility(View.VISIBLE);
                        scanning2.setVisibility(View.VISIBLE);
                        break;
                    default:
                        addllinear.setVisibility(View.VISIBLE);
                        serialnumber.setVisibility(View.VISIBLE);
                        scanning2.setVisibility(View.VISIBLE);

                }
            }
        });
        Addchemicals.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(selectreport !=null){
                    final LinearLayout linearLayoutForm = (LinearLayout)rootView. findViewById(R.id.linearLayoutForm);


                    scrollView1.setVisibility(View.VISIBLE);
               /* scanning.setVisibility(View.GONE);
                scanning2.setVisibility(View.VISIBLE);*/
                    final LinearLayout newView = (LinearLayout) getLayoutInflater().inflate(R.layout.chemicals, null);
                    newView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    ImageButton btnRemove = (ImageButton) newView.findViewById(R.id.btnRemove);
                    EditText quantityedit = (EditText)newView.findViewById(R.id.quantityedit);
                    EditText serialno = (EditText)newView.findViewById(R.id.serialno);
                    MaterialSpinner chemicals_spin = (MaterialSpinner) newView.findViewById(R.id.chemicalsedit);
                    switch (selectreport){
                        case "ND":
                            serialno.setVisibility(View.GONE);
                            break;
                        default:
                            serialno.setVisibility(View.VISIBLE);
                            break;
                    }
                    chemicallist1=new ArrayList<>();
                    chemicallist1.add("Select Chemicals");
                    chemicallist1.add("Phostoxin");
                    chemicallist1.add("Talunex Tablets");
                    chemicallist1.add("Topex Applicator");
                    chemicals_spin.setItems(chemicallist1);
                    chemicals_spin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                            selectchemicals1=item.toString();
                        }
                    });

                    btnRemove.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            scanning.setVisibility(View.VISIBLE);
                            scanning2.setVisibility(View.GONE);
                            linearLayoutForm.removeView(newView);
                        }
                    });
                    linearLayoutForm.addView(newView);
                }else{
                    StyleableToast st = new StyleableToast(getActivity(), "Select WorkReport", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                }


        });

        return rootView;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode ==49374){
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            serialno = result.getContents();
            if (result != null) {


                if (serialnumber.getText().length() == 0) {
                    //if qrcode has nothing in it
                    if (result.getContents() == null) {
                        //Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
                    } else {
                        //if qr contains data
                        try {
                            Log.d(TAG, "onActivityResult: serialnumber" + serialnumber);
                            //if (quatity!=null)
                            serialnumber.setText(serialno);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } else {

                    final LinearLayout linearLayoutForm = (LinearLayout)getView().findViewById(R.id.linearLayoutForm);
                    scrollView1.setVisibility(View.VISIBLE);
                    scanning.setVisibility(View.GONE);
                    scanning2.setVisibility(View.VISIBLE);
                    final LinearLayout newView = (LinearLayout) getLayoutInflater().inflate(R.layout.chemicals, null);
                    newView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    ImageButton btnRemove = (ImageButton) newView.findViewById(R.id.btnRemove);
                    MaterialSpinner chemicals_spin = (MaterialSpinner) newView.findViewById(R.id.chemicalsedit);
                    EditText chemicals_serial = (EditText) newView.findViewById(R.id.quantityedit);
                    EditText serialno1 = (EditText) newView.findViewById(R.id.serialno);
                    serialno1.setText(serialno);
                    chemicallist1 = new ArrayList<>();
                    chemicallist1.add("Select Chemicals");
                    chemicallist1.add("Phostoxin");
                    chemicallist1.add("Talunex Tablets");
                    chemicallist1.add("Topex Applicator");
                    chemicals_spin.setItems(chemicallist1);

                    btnRemove.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            scanning.setVisibility(View.VISIBLE);
                            scanning2.setVisibility(View.GONE);
                            linearLayoutForm.removeView(newView);
                        }
                    });
                    linearLayoutForm.addView(newView);


                }
            }else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }


    }

    public void value(String containernumber) {
        this.containerno=containernumber;

    }



}



