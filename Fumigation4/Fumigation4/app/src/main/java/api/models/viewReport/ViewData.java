package api.models.viewReport;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class ViewData {
  @SerializedName("MobileNo")
  @Expose
  private String MobileNo;
  @SerializedName("Audio1")
  @Expose
  private String Audio1;
  @SerializedName("PlaceOfVisit")
  @Expose
  private String PlaceOfVisit;
  @SerializedName("Remarks")
  @Expose
  private String Remarks;
  @SerializedName("Video")
  @Expose
  private String Video;
  @SerializedName("Image1")
  @Expose
  private String Image1;
  @SerializedName("Time")
  @Expose
  private String Time;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("Image")
  @Expose
  private String Image;
  @SerializedName("Audio")
  @Expose
  private String Audio;
  @SerializedName("Date")
  @Expose
  private String Date;
  @SerializedName("Name")
  @Expose
  private String Name;
  public void setMobileNo(String MobileNo){
   this.MobileNo=MobileNo;
  }
  public String getMobileNo(){
   return MobileNo;
  }
  public void setAudio1(String Audio1){
   this.Audio1=Audio1;
  }
  public String getAudio1(){
   return Audio1;
  }
  public void setPlaceOfVisit(String PlaceOfVisit){
   this.PlaceOfVisit=PlaceOfVisit;
  }
  public String getPlaceOfVisit(){
   return PlaceOfVisit;
  }
  public void setRemarks(String Remarks){
   this.Remarks=Remarks;
  }
  public String getRemarks(){
   return Remarks;
  }
  public void setVideo(String Video){
   this.Video=Video;
  }
  public String getVideo(){
   return Video;
  }
  public void setImage1(String Image1){
   this.Image1=Image1;
  }
  public String getImage1(){
   return Image1;
  }
  public void setTime(String Time){
   this.Time=Time;
  }
  public String getTime(){
   return Time;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setImage(String Image){
   this.Image=Image;
  }
  public String getImage(){
   return Image;
  }
  public void setAudio(String Audio){
   this.Audio=Audio;
  }
  public String getAudio(){
   return Audio;
  }
  public void setDate(String Date){
   this.Date=Date;
  }
  public String getDate(){
   return Date;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }
}