package api.models.UploadAudio;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class AudioUploadResult{
  @SerializedName("Message")
  @Expose
  private String Message;
  public void setMessage(String Message){
   this.Message=Message;
  }
  public String getMessage(){
   return Message;
  }
}