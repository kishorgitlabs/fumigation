package api.models.loginbranch;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class loginBranch {
  @SerializedName("Zip")
  @Expose
  private String Zip;
  @SerializedName("Email")
  @Expose
  private String Email;
  @SerializedName("UserName")
  @Expose
  private String UserName;
  @SerializedName("SavePassword")
  @Expose
  private String SavePassword;
  @SerializedName("InsertDate")
  @Expose
  private String InsertDate;
  @SerializedName("Address2")
  @Expose
  private String Address2;
  @SerializedName("MobileNumber1")
  @Expose
  private String MobileNumber1;
  @SerializedName("Address1")
  @Expose
  private String Address1;
  @SerializedName("MobileNumber2")
  @Expose
  private String MobileNumber2;
  @SerializedName("City")
  @Expose
  private String City;
  @SerializedName("UpdateDate")
  @Expose
  private String UpdateDate;
  @SerializedName("IMINumber2")
  @Expose
  private String IMINumber2;
  @SerializedName("IMINumber1")
  @Expose
  private String IMINumber1;
  @SerializedName("State")
  @Expose
  private String State;
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("BranchName")
  @Expose
  private String BranchName;
  @SerializedName("BranchCode")
  @Expose
  private String BranchCode;
  @SerializedName("Password")
  @Expose
  private String Password;
  public void setZip(String Zip){
   this.Zip=Zip;
  }
  public String getZip(){
   return Zip;
  }
  public void setEmail(String Email){
   this.Email=Email;
  }
  public String getEmail(){
   return Email;
  }
  public void setUserName(String UserName){
   this.UserName=UserName;
  }
  public String getUserName(){
   return UserName;
  }
  public void setSavePassword(String SavePassword){
   this.SavePassword=SavePassword;
  }
  public String getSavePassword(){
   return SavePassword;
  }
  public void setInsertDate(String InsertDate){
   this.InsertDate=InsertDate;
  }
  public String getInsertDate(){
   return InsertDate;
  }
  public void setAddress2(String Address2){
   this.Address2=Address2;
  }
  public String getAddress2(){
   return Address2;
  }
  public void setMobileNumber1(String MobileNumber1){
   this.MobileNumber1=MobileNumber1;
  }
  public String getMobileNumber1(){
   return MobileNumber1;
  }
  public void setAddress1(String Address1){
   this.Address1=Address1;
  }
  public String getAddress1(){
   return Address1;
  }
  public void setMobileNumber2(String MobileNumber2){
   this.MobileNumber2=MobileNumber2;
  }
  public String getMobileNumber2(){
   return MobileNumber2;
  }
  public void setCity(String City){
   this.City=City;
  }
  public String getCity(){
   return City;
  }
  public void setUpdateDate(String UpdateDate){
   this.UpdateDate=UpdateDate;
  }
  public String getUpdateDate(){
   return UpdateDate;
  }
  public void setIMINumber2(String IMINumber2){
   this.IMINumber2=IMINumber2;
  }
  public String getIMINumber2(){
   return IMINumber2;
  }
  public void setIMINumber1(String IMINumber1){
   this.IMINumber1=IMINumber1;
  }
  public String getIMINumber1(){
   return IMINumber1;
  }
  public void setState(String State){
   this.State=State;
  }
  public String getState(){
   return State;
  }
  public void setId(String id){
   this.id=id;
  }
  public String getId(){
   return id;
  }
  public void setBranchName(String BranchName){
   this.BranchName=BranchName;
  }
  public String getBranchName(){
   return BranchName;
  }
  public void setBranchCode(String BranchCode){
   this.BranchCode=BranchCode;
  }
  public String getBranchCode(){
   return BranchCode;
  }
  public void setPassword(String Password){
   this.Password=Password;
  }
  public String getPassword(){
   return Password;
  }
}