package api.models.workreport1.request;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class TW{
  @SerializedName("TotalWorkers")
  @Expose
  private String TotalWorkers;
  public void setTotalWorkers(String TotalWorkers){
   this.TotalWorkers=TotalWorkers;
  }
  public String getTotalWorkers(){
   return TotalWorkers;
  }
}