package api.models.materialrequest.response;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class Data{
  @SerializedName("pc")
  @Expose
  private Pc pc;
  @SerializedName("mlist")
  @Expose
  private List<Mlist> mlist;
  public void setPc(Pc pc){
   this.pc=pc;
  }
  public Pc getPc(){
   return pc;
  }
  public void setMlist(List<Mlist> mlist){
   this.mlist=mlist;
  }
  public List<Mlist> getMlist(){
   return mlist;
  }
}