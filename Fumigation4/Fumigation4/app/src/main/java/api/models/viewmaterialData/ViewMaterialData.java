package api.models.viewmaterialData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class ViewMaterialData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<ViewMaterial> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<ViewMaterial> data){
   this.data=data;
  }
  public List<ViewMaterial> getData(){
   return data;
  }
}