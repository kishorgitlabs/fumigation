
package api.models.gps;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GpsData {

    @SerializedName("data")
    private Gps mData;
    @SerializedName("result")
    private String mResult;

    public Gps getData() {
        return mData;
    }

    public void setData(Gps data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
