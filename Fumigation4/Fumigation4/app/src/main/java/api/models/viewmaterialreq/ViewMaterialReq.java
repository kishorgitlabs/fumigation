package api.models.viewmaterialreq;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class ViewMaterialReq {
  @SerializedName("Description")
  @Expose
  private String Description;
  @SerializedName("ProductName")
  @Expose
  private String ProductName;
  @SerializedName("ReqQty")
  @Expose
  private Integer ReqQty;
  @SerializedName("ApprovedQty")
  @Expose
  private String ApprovedQty;
  public void setDescription(String Description){
   this.Description=Description;
  }
  public String getDescription(){
   return Description;
  }
  public void setProductName(String ProductName){
   this.ProductName=ProductName;
  }
  public String getProductName(){
   return ProductName;
  }
  public void setReqQty(Integer ReqQty){
   this.ReqQty=ReqQty;
  }
  public Integer getReqQty(){
   return ReqQty;
  }
  public void setApprovedQty(String ApprovedQty){
   this.ApprovedQty=ApprovedQty;
  }
  public String getApprovedQty(){
   return ApprovedQty;
  }
}