package api.models.alertnames;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class GetAllName {
    @SerializedName("Name")
    @Expose
    private String Name;
    public void setName(String Name){
        this.Name=Name;
    }
    public String getName(){
        return Name;
    }
}