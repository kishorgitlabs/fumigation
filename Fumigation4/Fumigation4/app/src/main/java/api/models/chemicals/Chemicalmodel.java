package api.models.chemicals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import api.models.upload1.response.Chemicals1;

/**
 * Awesome Pojo Generator
 * */
public class Chemicalmodel{
  @SerializedName("Status")
  @Expose
  private String Status;
  @SerializedName("Remarks")
  @Expose
  private String Remarks;
  @SerializedName("Image2")
  @Expose
  private String Image2;
  @SerializedName("Image1")
  @Expose
  private String Image1;
  @SerializedName("workreport")
  @Expose
  private String workreport;
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("ContainerNo")
  @Expose
  private String ContainerNo;
  @SerializedName("Audio")
  @Expose
  private String Audio;
  @SerializedName("chemicals1")
  @Expose
  private List<Chemicals> chemicals1;
  public void setStatus(String Status){
   this.Status=Status;
  }
  public String getStatus(){
   return Status;
  }
  public void setRemarks(String Remarks){
   this.Remarks=Remarks;
  }
  public String getRemarks(){
   return Remarks;
  }
  public void setImage2(String Image2){
   this.Image2=Image2;
  }
  public String getImage2(){
   return Image2;
  }
  public void setImage1(String Image1){
   this.Image1=Image1;
  }
  public String getImage1(){
   return Image1;
  }
  public void setWorkreport(String workreport){
   this.workreport=workreport;
  }
  public String getWorkreport(){
   return workreport;
  }
  public void setId(String id){
   this.id=id;
  }
  public String getId(){
   return id;
  }
  public void setContainerNo(String ContainerNo){
   this.ContainerNo=ContainerNo;
  }
  public String getContainerNo(){
   return ContainerNo;
  }
  public void setAudio(String Audio){
   this.Audio=Audio;
  }
  public String getAudio(){
   return Audio;
  }
  public void setChemicals(List<Chemicals> chemicals1){
   this.chemicals1=chemicals1;
  }
  public List<Chemicals> getChemicals1(){
   return chemicals1;
  }

    public void setChemicals1(List<Chemicals1> msg) {
    }
}