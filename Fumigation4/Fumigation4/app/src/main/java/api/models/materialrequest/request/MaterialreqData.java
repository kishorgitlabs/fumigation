package api.models.materialrequest.request;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class  MaterialreqData{
  @SerializedName("RequestTo")
  @Expose
  private String RequestTo;
  @SerializedName("data")
  @Expose
  private List<Materialrequest> data;
  @SerializedName("BranchId")
  @Expose
  private Integer BranchId;
  @SerializedName("ReqBranchName")
  @Expose
  private String ReqBranchName;
  @SerializedName("ReqPersonName")
  @Expose
  private String ReqPersonName;
  @SerializedName("ReqBranchCode")
  @Expose
  private String ReqBranchCode;
  public void setRequestTo(String RequestTo){
   this.RequestTo=RequestTo;
  }
  public String getRequestTo(){
   return RequestTo;
  }
  public void setData(List<Materialrequest> data){
   this.data=data;
  }
  public List<Materialrequest> getData(){
   return data;
  }
  public void setBranchId(Integer BranchId){
   this.BranchId=BranchId;
  }
  public Integer getBranchId(){
   return BranchId;
  }
  public void setReqBranchName(String ReqBranchName){
   this.ReqBranchName=ReqBranchName;
  }
  public String getReqBranchName(){
   return ReqBranchName;
  }
  public void setReqPersonName(String ReqPersonName){
   this.ReqPersonName=ReqPersonName;
  }
  public String getReqPersonName(){
   return ReqPersonName;
  }
  public void setReqBranchCode(String ReqBranchCode){
   this.ReqBranchCode=ReqBranchCode;
  }
  public String getReqBranchCode(){
   return ReqBranchCode;
  }
}