package api.models.chemicals;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class Chemicals {
  @SerializedName("Quatity")
  @Expose
  private String Quatity;
  @SerializedName("chemicals")
  @Expose
  private String chemicals;
  public void setQuatity(String Quatity){
   this.Quatity=Quatity;
  }
  public String getQuatity(){
   return Quatity;
  }
  public void setChemicals(String chemicals){
   this.chemicals=chemicals;
  }
  public String getChemicals(){
   return chemicals;
  }
}