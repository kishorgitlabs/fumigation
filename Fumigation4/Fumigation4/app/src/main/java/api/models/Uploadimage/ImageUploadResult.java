package api.models.Uploadimage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class ImageUploadResult{
  @SerializedName("Message")
  @Expose
  private String Message;
  public void setMessage(String Message){
   this.Message=Message;
  }
  public String getMessage(){
   return Message;
  }
}