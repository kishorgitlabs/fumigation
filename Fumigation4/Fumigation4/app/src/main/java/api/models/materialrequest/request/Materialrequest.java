package api.models.materialrequest.request;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Materialrequest {
  @SerializedName("ProductUnit")
  @Expose
  private String ProductUnit;
  @SerializedName("Description")
  @Expose
  private String Description;
  @SerializedName("ProductName")
  @Expose
  private String ProductName;
  @SerializedName("ReqQty")
  @Expose
  private String ReqQty;
  public void setProductUnit(String ProductUnit){
   this.ProductUnit=ProductUnit;
  }
  public String getProductUnit(){
   return ProductUnit;
  }
  public void setDescription(String Description){
   this.Description=Description;
  }
  public String getDescription(){
   return Description;
  }
  public void setProductName(String ProductName){
   this.ProductName=ProductName;
  }
  public String getProductName(){
   return ProductName;
  }
  public void setReqQty(String ReqQty){
   this.ReqQty=ReqQty;
  }
  public String getReqQty(){
   return ReqQty;
  }
}