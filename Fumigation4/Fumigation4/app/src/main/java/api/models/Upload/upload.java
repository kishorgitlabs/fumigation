package api.models.Upload;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class upload {
  @SerializedName("emp")
  @Expose
  private Emp emp;
  @SerializedName("chemicals1")
  @Expose
  private List<Chemicals1> chemicals1;
  public void setEmp(Emp emp){
   this.emp=emp;
  }
  public Emp getEmp(){
   return emp;
  }
  public void setChemicals1(List<Chemicals1> chemicals1){
   this.chemicals1=chemicals1;
  }
  public List<Chemicals1> getChemicals1(){
   return chemicals1;
  }
}