package api.models.viewmaterialreq;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class ViewMaterialDataReq{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<ViewMaterialReq> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<ViewMaterialReq> data){
   this.data=data;
  }
  public List<ViewMaterialReq> getData(){
   return data;
  }
}