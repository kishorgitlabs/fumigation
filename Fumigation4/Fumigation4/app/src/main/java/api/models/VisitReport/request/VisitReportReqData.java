package api.models.VisitReport.request;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class VisitReportReqData{
  @SerializedName("MobileNo")
  @Expose
  private String MobileNo;
  @SerializedName("PlaceOfVisit")
  @Expose
  private String PlaceOfVisit;
  @SerializedName("Remarks")
  @Expose
  private String Remarks;
  @SerializedName("Time")
  @Expose
  private String Time;
  @SerializedName("Image")
  @Expose
  private String Image;
  @SerializedName("Audio")
  @Expose
  private String Audio;
  @SerializedName("Date")
  @Expose
  private String Date;
  @SerializedName("Name")
  @Expose
  private String Name;
  public void setMobileNo(String MobileNo){
   this.MobileNo=MobileNo;
  }
  public String getMobileNo(){
   return MobileNo;
  }
  public void setPlaceOfVisit(String PlaceOfVisit){
   this.PlaceOfVisit=PlaceOfVisit;
  }
  public String getPlaceOfVisit(){
   return PlaceOfVisit;
  }
  public void setRemarks(String Remarks){
   this.Remarks=Remarks;
  }
  public String getRemarks(){
   return Remarks;
  }
  public void setTime(String Time){
   this.Time=Time;
  }
  public String getTime(){
   return Time;
  }
  public void setImage(String Image){
   this.Image=Image;
  }
  public String getImage(){
   return Image;
  }
  public void setAudio(String Audio){
   this.Audio=Audio;
  }
  public String getAudio(){
   return Audio;
  }
  public void setDate(String Date){
   this.Date=Date;
  }
  public String getDate(){
   return Date;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }
}