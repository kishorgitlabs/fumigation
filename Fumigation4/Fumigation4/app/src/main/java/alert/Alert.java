package alert;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.brainmagic.fumigation.R;


/**
 * Created by SYSTEM10 on 4/26/2018.
 */
//alertbox
public class Alert {
    private Context context;
    private AlertDialog alertDialog;

    public Alert(Context context) {
        this.context = context;
    }


    public void showAlert(String msg) {
        alertDialog = new AlertDialog.Builder(
                context).create();

        View dialogView = LayoutInflater.from(context)
                .inflate(R.layout.alertboxlinear, null, false);
        alertDialog.setView(dialogView);
        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button)  dialogView.findViewById(R.id.okay);
        log.setText(msg);
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }
    public void showErrorAlert(String msg) {
        alertDialog = new AlertDialog.Builder(
                context).create();

        View dialogView = LayoutInflater.from(context)
                .inflate(R.layout.erroralertbox







                        , null, false);

        alertDialog.setView(dialogView);
        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button)  dialogView.findViewById(R.id.okay);
        log.setText(msg);
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }
}
