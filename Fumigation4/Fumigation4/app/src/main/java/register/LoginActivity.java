package register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.fumigation.R;
import com.brainmagic.fumigation.SupervisorHomeActivity;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.ArrayList;

import alert.Alertbox;
import api.models.login.LoginData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class LoginActivity extends AppCompatActivity {
    //intialize the Details
    private EditText username, password;
    private Button login;
  //  private MaterialSpinner usertype;
    private TextView forget;
    private Alertbox box = new Alertbox(this);
    private Toasts toasts = new Toasts(this);
    //intialize the local Storage...
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ArrayList<String>user;
    private String iemi1,iemi2;
    private ArrayList<String>list;
    private  String selectbranch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //Declare the Database.
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();

        iemi1=myshare.getString("imei1","");
        iemi2=myshare.getString("Imei2","");
        username = (EditText) findViewById(R.id.edlogname);
        password = (EditText) findViewById(R.id.edpassword);
      //  usertype = (MaterialSpinner) findViewById(R.id.usertype);
        forget = (TextView) findViewById(R.id.txforgotpassword);
        login = (Button) findViewById(R.id.logbtn);
//        list=new ArrayList<String>();
//        list.add("Select User type");
//        list.add("Supervisor");
//        list.add("Field Operator");
//        list.add("Branch");
//        usertype.setItems(list);
//        usertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                selectbranch=item.toString();
//            }
//        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(username.getText().toString().equals("")){
                    username.setError("Enter username");
                    StyleableToast st = new StyleableToast(LoginActivity.this, "Enter username", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(LoginActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else if(password.getText().toString().equals("")){
                    password.setError("Enter password");
                    StyleableToast st = new StyleableToast(LoginActivity.this, "Enter password !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(LoginActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else {
                    //check the interernet.
                    checkinternet();
                }
            }
        });


    }
  //Check The Internet device.
    private void checkinternet() {
        NetworkConnection isnet = new NetworkConnection(LoginActivity.this);
        if (isnet.CheckInternet()) {
            logindata();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }
    //Check the Login Device...
    private void logindata() {
        try {
            final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, "Fumigation", "Login...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<LoginData> call = service.login(username.getText().toString(),
                        password.getText().toString(),iemi2,iemi1);
                call.enqueue(new Callback<LoginData>() {
                    @Override
                    public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                        try {
                            switch (response.body().getResult()) {
                                case "Success":
                                    switch (response.body().getData().getUserType()) {
                                        case "Supervisor":
                                            loading.dismiss();
                                            editor.putBoolean("isLogin", true);
                                            editor.putBoolean("isloginbranch", false);
                                            editor.putString("usertype", response.body().getData().getUserType());
                                            editor.putString("name", response.body().getData().getName());
                                            editor.putString("branchcode",response.body().getData().getBranchCode());
                                            editor.putString("branchname",response.body().getData().getBranchName());
                                       /* editor.putString("imei1", response.body().getData().getIMINumber1());
                                        editor.putString("Imei2", response.body().getData().getIMINumber2());*/
                                            editor.putInt("id", response.body().getData().getId());
                                         //  editor.putString("")
                                            editor.putString("email",response.body().getData().getEmailId());
                                            editor.putString("customercode",response.body().getData().getCustomerCode());
                                            editor.putString("phonenumber", String.valueOf(response.body().getData().getMobileNumber1()));
                                            editor.putString("type","Supervisor Name :");
                                            editor.commit();
                                            Intent i=new Intent(LoginActivity.this, SupervisorHomeActivity.class);
                                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(i);
                                            finish();
                                            toasts.ShowSuccessToast("Login Successfully");
                                            break;

                                        case "Field Operator":
                                            editor.putBoolean("isLogin", true);
                                            editor.putString("usertype", response.body().getData().getUserType());
                                            editor.putString("name", response.body().getData().getName());
                                            editor.putString("usertype", response.body().getData().getUserType());
                                            editor.putString("customercode",response.body().getData().getCustomerCode());
                                        /*editor.putString("imei1", response.body().getData().getIMINumber1());
                                        editor.putString("Imei2", response.body().getData().getIMINumber2());*/
                                            editor.putString("name", response.body().getData().getName());
                                            editor.putInt("id", response.body().getData().getId());
                                            editor.putString("type","Field Operator Name :");
                                            editor.commit();
                                            loading.dismiss();
                                            Intent i1=new Intent(LoginActivity.this, SupervisorHomeActivity.class);
                                            i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(i1);
                                            finish();
                                            toasts.ShowSuccessToast("Login Successfully");
                                            break;
                                    }

                                    break;
                                case "NotSuccess":
                                    box.showAlertbox(getResources().getString(R.string.not_success));
                                    loading.dismiss();
                                    break;
                                default:
                                    box.showAlertbox("Server error in connection");
                                    loading.dismiss();
                                    break;
                            }




                      /*  loading.dismiss();
                        if (response.body().getResult().equals("Success")) {

                            //OnSuccessRegistration(response.body().getData());
                        } else if (response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox(getResources().getString(R.string.not_success));
                        }*/

                        } catch (Exception e) {
                            e.printStackTrace();
                            loading.dismiss();
                            box.showAlertbox(getResources().getString(R.string.server_error));

                        }
                    }

                    @Override
                    public void onFailure(Call<LoginData> call, Throwable t) {
                        loading.dismiss();
                        t.printStackTrace();
                        box.showAlertbox(getResources().getString(R.string.server_error));
                    }
                });



        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //FORGOT PASSWORD
    public void Forget_Password(View view) {
        ForgetPasswordAlert forget = new ForgetPasswordAlert(LoginActivity.this);
        forget.showLoginbox();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
