package com.brainmagic.fumigation;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.crypto.KeyGenerator;

import alert.Alertbox;
import api.models.attendance.Attendancename;
import api.models.workreport.LocationData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import logout.Logout;
import network.NetworkConnection;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendanceOrioActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks,ResultCallback<LocationSettingsResult>,
        GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener,com.google.android.gms.location.LocationListener,LocationListener,FingerprintHandler.RunBackgroundService{
    private ImageView menu,back,attendancetime;
    private AlertDialog alertDialog;
    private Alertbox box = new Alertbox(this);
    private Connection connection;
    // private ArrayList<String> empidlist,namelist;

    private Statement statement;
    private ResultSet resultSet;
    private ProgressDialog loading;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private MaterialSpinner worklocate;
    private TextView designation,address2,ename;
    private String jobcard,jobcardid,selectname;
    private List<Attendancename>AttendanceData;
    private List<String>locationdata;
    List<Attendancename>namelist;
    private LocationManager mlocationManager;
    private String description;
    Double latitude;
    Double longitude;
    private int MIN_DISTANCE_CHANGE_FOR_UPDATES=500;
    LocationHelper locationHelper;
    String selectwork;
    private String name,selectid;
    private String City="No City found";
    private boolean ischeck=false;
    private List<String> getnameList,getIdlist,getCustomerList;
    private KeyStore keyStore;

    // location
    private Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    private static final String TAG = "";
    private int REQUEST_CHECK_SETTINGS = 100;
    private ProgressDialog mProgressDialog;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    String address, address1, city, state, country, postalCode,title,selectedempid = "";
    private Button attendancebtn;
    private String employeename,design;
    private String location;
    private Boolean ischeckin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_orio);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        mlocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        ename=(TextView)findViewById(R.id.ename);
        worklocate=(MaterialSpinner)findViewById(R.id.worklocate);
        designation=(TextView)findViewById(R.id.designation);
        address2=(TextView)findViewById(R.id.address);
        attendancebtn=(Button)findViewById(R.id.attendancebtn);
        back=(ImageView)findViewById(R.id.back);
        ischeck=Boolean.getBoolean(getIntent().getStringExtra(""));
        attendancetime=(ImageView)findViewById(R.id.attendancetime);
        employeename=myshare.getString("name","");
        design=myshare.getString("usertype","");

        ename.setText(employeename);
        designation.setText(design);
        getnameList=new ArrayList<>();
        getIdlist=new ArrayList<>();
        getCustomerList=new ArrayList<>();
        namelist=new ArrayList<>();
        menu=(ImageView)findViewById(R.id.menu);
        worklocate.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectwork=item.toString();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        attendancebtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if(ename.getText().toString().equals("Select Name")){
                    StyleableToast st = new StyleableToast(AttendanceOrioActivity.this, "Enter Employee name !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(AttendanceOrioActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                } else if(designation.getText().toString().equals("")){
                    StyleableToast st = new StyleableToast(AttendanceOrioActivity.this, "Enter your designation !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(AttendanceOrioActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                } else if(address2.getText().toString().equals("")){
                    StyleableToast st = new StyleableToast(AttendanceOrioActivity.this, "Enter your address!", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(AttendanceOrioActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                }  else if(worklocate.getText().equals("Select location")){
                    StyleableToast st = new StyleableToast(AttendanceOrioActivity.this, "Enter your location !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(AttendanceOrioActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        FingerprintManager fingerprintManager = (FingerprintManager) AttendanceOrioActivity.this.getSystemService(Context.FINGERPRINT_SERVICE);
                        if(fingerprintManager.isHardwareDetected()){
                            Intent i = new Intent(AttendanceOrioActivity.this, FingerprintActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            editor.putString("worklocation",selectwork);
                            editor.commit();
                            startActivity(i);
                        }
                    }else{
                        Intent i = new Intent(AttendanceOrioActivity.this, AttendanceViewActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        editor.putString("worklocation",selectwork);
                        editor.commit();
                        startActivity(i);
                    }



//                        ntent i = new Intent(AttendanceOrioActivity.this, AttendanceViewActivity.class).putExtra("worklocation", selectwork);
//                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(i);I

                    // Everything is ready for fingerprint authentication

//
//                    Intent i = new Intent(AttendanceOrioActivity.this, AttendanceViewActivity.class).putExtra("worklocation", selectwork);
//                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(i);
                }
            }
        });

        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(AttendanceOrioActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(AttendanceOrioActivity.this).log_out();
                                return true;
                            case R.id.changepassword:
                                Intent i2=new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;




                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menuattendance);
                popupMenu.show();
                ;
            }
        });
        attendancetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu=new PopupMenu(AttendanceOrioActivity.this,view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(AttendanceOrioActivity.this).log_out();
                                return true;
                            case R.id.view_Attendance:
                                Intent i2=new Intent(getApplicationContext(), ViewAttendance.class);
                                startActivity(i2);
                                return true;
                            case R.id.Travelhistory:
                                Intent i5=new Intent(getApplicationContext(), TravelHistoryActivity.class);
                                startActivity(i5);
                                return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.attendancemenu);
                popupMenu.show();

            }
        });

       /* ename.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectname=item.toString();
                if(!selectname.equals("Select Name")){
                    selectedempid= getCustomerList.get(getnameList.indexOf(selectname) - 1);

                }
                //selectid=getCustomerList.get(getIdlist.indexOf(selectname)-1);
                Getdescription();
            }

        });*/
        // location
        //Check If Google Services Is Available
        if (getServicesAvailable()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
            //  Toast.makeText(this, "Google Service Is Available!!", Toast.LENGTH_SHORT).show();

        }
        AskLocationPermission();
        NetworkConnection isnet = new NetworkConnection(AttendanceOrioActivity.this);
        if (isnet.CheckInternet()) {
            GetLocation();

        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }



    }

    private void GetLocation() {
        try {
            final ProgressDialog loading = ProgressDialog.show(AttendanceOrioActivity.this, "Fumigation", "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<LocationData> call;
            call = service.location();
            call.enqueue(new Callback<LocationData>() {
                @Override
                public void onResponse(Call<LocationData> call, Response<LocationData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            locationdata = response.body().getData();
                            locationdata.add(0,"Select location");
                            worklocate.setItems(locationdata);


                        } else if (response.body().getResult().equals(" Not Success")) {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<LocationData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AskLocationPermission() {
        if(EasyPermissions.hasPermissions(AttendanceOrioActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)){
            if(CheckLocationIsEnabled()){
                startLocationUpdates();
            } else{
                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(mLocationRequest);

                PendingResult<LocationSettingsResult> result =
                        LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @Override
                    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                        final Status status = locationSettingsResult.getStatus();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                startLocationUpdates();
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    status.startResolutionForResult(
                                            AttendanceOrioActivity.this,
                                            REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException e) {
                                    Log.e(TAG, "Exception : " + e.getMessage());
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e(TAG, "Location settings are not satisfied.");
                                break;
                        }
                    }
                });
            }


        }else{
            // Request one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location), MY_PERMISSIONS_REQUEST_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);

        }
    }

    private void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates: ");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if(mGoogleApiClient!=null) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                }
//                if(mLastLocation==null)
                {
                    mProgressDialog= ProgressDialog.show(this,"Location","Getting Your Location",false,false);
                    mlocationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            1000,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES,this);
                    mlocationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            1000,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES,this);
                }
//                else
//                    {
//                    mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
//
//                    CheckInternet();
//                }
//                CheckInternet();
//                mProgressDialog=ProgressDialog.show(this,"Location","Getting Your Location",false,false);
                Log.d(TAG, "startLocationUpdates: ");
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
                Log.d(TAG, "Permission Not Granted");
            }

        } else {
            Log.d(TAG, "startLocationUpdates: else");
            if(mGoogleApiClient!=null){
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            }
//            if(mLastLocation==null)
            {
                mProgressDialog=ProgressDialog.show(this,"Location","Getting Your Location",false,false);
                mlocationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        1000,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES,this);
                mlocationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        1000,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES,this);
            }
//            else
//            {
//                mProgressDialog=ProgressDialog.show(this,"Location","Getting Your Location",false,false);
//                CheckInternet();
//            }

        }

    }


    private boolean CheckLocationIsEnabled() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        if(mGoogleApiClient!=null)
            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);

        if (mLastLocation == null) {
            return false;
        } else {

            return true;
        }
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(AppConstants.UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(AppConstants.FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(AppConstants.DISPLACEMENT);
    }

    protected synchronized void buildGoogleApiClient() {
        try {
            Log.d(TAG, "buildGoogleApiClient: ");
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(this, this).build();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean getServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {

            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
            return false;
        } else {
            Toast.makeText(this, "Cannot Connect To Play Services", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void runBackgroundService(String start) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mProgressDialog.dismiss();
        if (location != null) {
            mLastLocation = location;
            latitude=mLastLocation.getLatitude();
            longitude=mLastLocation.getLongitude();
            editor.putString("latitude",latitude.toString());
            editor.putString("longitude",longitude.toString());
            Log.d(TAG, "onLocationChanged: "+location.getLatitude()+" long "+location.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(location.getLatitude(), location.getLongitude());
            //Or Do whatever you want with your location
        }
        else if(mLastLocation!=null) {
            latitude=mLastLocation.getLatitude();
            longitude=mLastLocation.getLongitude();
            editor.putString("latitude",latitude.toString());
            editor.putString("longitude",longitude.toString());
            Log.d(TAG, "onLocationChanged: "+mLastLocation.getLatitude()+" long "+mLastLocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";


        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(AttendanceOrioActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }

                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            } else {

            }

            return null;
        }

        protected void onPostExecute(Address addresss) {

            if (addresss == null) {
                Log.d(TAG, "onPostExecute: *****");
            } else {
//                progressBar.setVisibility(View.GONE);
                address = addresss.getAddressLine(0); //0 to obtain first possible address
                editor.putString("FromAddress",address);
                editor.putString("ToAddress",address);
                editor.commit();
                editor.apply();
                City = addresss.getLocality();
                Log.d(TAG, "onPostExecute: **************************"+City);
                String state = addresss.getAdminArea();
                address2.setText(address);
                //create your custom title
//                String title = city + "-" + state;
//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertboxwithback("Your Current location is "+city);


            }
        }
    }
}
