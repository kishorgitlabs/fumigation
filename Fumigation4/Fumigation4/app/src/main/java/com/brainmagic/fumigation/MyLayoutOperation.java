package com.brainmagic.fumigation;

import android.app.Activity;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import api.models.chemicals.Chemicalmodel;
import api.models.chemicals.Chemicals;

/**
 * Created by SYSTEM10 on 6/29/2018.
 */
//Dynamic processs...
class MyLayoutOperation {
    private Chemicalmodel chemicalmodel;

    public  void display(final Activity activity, Button Add, final SharedPreferences myshare,final SharedPreferences.Editor editor) {


        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText chemicalsedit,quantityedit;
                LinearLayout scrollViewlinerLayout = (LinearLayout) activity.findViewById(R.id.linearLayoutForm);
                List<Chemicals> msg = new ArrayList<>();
                for (int i = 0; i < scrollViewlinerLayout.getChildCount(); i++) {
                    LinearLayout innerLayout = (LinearLayout) scrollViewlinerLayout.getChildAt(i);
                    chemicalsedit=(EditText)scrollViewlinerLayout.findViewById(R.id.chemicalsedit);
                    quantityedit=(EditText)scrollViewlinerLayout.findViewById(R.id.quantityedit);
                    String chemicals1 = chemicalsedit.getText().toString();
                    String quantity1 = quantityedit.getText().toString();
                    msg.get(i).setChemicals(chemicals1);
                    msg.get(i).setQuatity(quantity1);
                    chemicalmodel.setChemicals(msg);
                    editor.putString("chemicals",chemicals1);
                    editor.putString("quantity",quantity1);
                    editor.commit();
                }
            }
        });
    }


    public  void add(final Activity activity, Button Addchemicals, final  ScrollView scrollView1) {



        final LinearLayout linearLayoutForm = (LinearLayout) activity.findViewById(R.id.linearLayoutForm);
        Addchemicals.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                scrollView1.setVisibility(View.VISIBLE);
                final LinearLayout newView = (LinearLayout) activity.getLayoutInflater().inflate(R.layout.chemicals, null);

                newView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                ImageButton btnRemove = (ImageButton) newView.findViewById(R.id.btnRemove);
                btnRemove.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        linearLayoutForm.removeView(newView);
                    }
                });
                linearLayoutForm.addView(newView);
            }
        });
    }

    public Chemicalmodel getChemicalmodel() {
        return chemicalmodel;
    }

    public void setChemicalmodel(Chemicalmodel chemicalmodel) {
        this.chemicalmodel = chemicalmodel;
    }
}
