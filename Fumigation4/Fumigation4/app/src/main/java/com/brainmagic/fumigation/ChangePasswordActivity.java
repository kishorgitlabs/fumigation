package com.brainmagic.fumigation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import alert.Alertbox;
import api.models.changepassword.ChangePassword;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import logout.Logout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {
    //intilize the details...
    private EditText oldpass,newpass;
    private Button submit;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String empcode;
    private ImageView back,menu;
    private Alertbox box;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        //get shared preference...
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        //find the variable name...
        back=(ImageView)findViewById(R.id.back);
        box=new Alertbox(ChangePasswordActivity.this);
        empcode=myshare.getString("customercode","");
        oldpass=(EditText)findViewById(R.id.oldpassword);
        menu=(ImageView)findViewById(R.id.menu);
        newpass=(EditText)findViewById(R.id.newpassword);
        submit=(Button)findViewById(R.id.submit);
        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ChangePasswordActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(ChangePasswordActivity.this).log_out();
                                return true;

                            case R.id.attendance:
                                Intent i3=new Intent(getApplicationContext(), AttendanceViewNewActivity.class);
                                startActivity(i3);
                                return true;
                            case R.id.jobcard:
                                Intent i4=new Intent(getApplicationContext(), Jobcarddetails.class);
                                startActivity(i4);
                                return true;
                            case R.id.viewvisit:
                                Intent i6=new Intent(getApplicationContext(), ViewReportActivity.class);
                                startActivity(i6);
                                return true;
                            case R.id.visitreport:
                                Intent i5=new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i5);
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menupassword);
                popupMenu.show();
                ;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(oldpass.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Enter Old Password",Toast.LENGTH_SHORT).show();
                }else if(newpass.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Enter New Password",Toast.LENGTH_SHORT).show();
                }else{
                    //method...
                    changepassword();
                }
            }
        });

    }
    //method...
    private void changepassword() {
        try {
            final ProgressDialog loading = ProgressDialog.show(ChangePasswordActivity.this, "Fumigation", "Login...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<ChangePassword> call = service.changepassword(empcode,oldpass.getText().toString(),
                    newpass.getText().toString());
            call.enqueue(new Callback<ChangePassword>() {
                @Override
                public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            //alertbox...
                            final AlertDialog.Builder alertDialog =new AlertDialog.Builder(ChangePasswordActivity.this);
                            alertDialog.setTitle("Change Password");
                            alertDialog.setMessage("Your Password Has Been Changed Successfully");
                            alertDialog.setCancelable(false);
                            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                 finish();
                                }
                            });
                            alertDialog.show();
                          /* Intent i =new Intent(context,AttendanceHomeActivity.class);
                           context.startActivity(i);*/


                        } else if (response.body().getResult().equals("InvalidUser")) {
                            //alert box...
                            final AlertDialog.Builder alertDialog =new AlertDialog.Builder(ChangePasswordActivity.this);
                            alertDialog.setTitle("Change Password");
                            alertDialog.setMessage("The Password InCorrect");
                            alertDialog.setCancelable(false);
                            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                            alertDialog.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox( ChangePasswordActivity.this.getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<ChangePassword> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(ChangePasswordActivity.this.getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
                }



    }

