package com.brainmagic.fumigation;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;

import adapter.JobCardAdapter;
import alert.Alertbox;
import logout.Logout;

public class fieldjobcarddetails extends AppCompatActivity {
    //intlize the detals...
    private ListView listView;
    private JobCardAdapter jobCardAdapter;
    private ImageView menu;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fieldjobcarddetails);
        //find the menu...
        menu=(ImageView)findViewById(R.id.menu) ;
        //Get Shared preference...
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
       /* listView=(ListView)findViewById(R.id.)*/
        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(fieldjobcarddetails.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(fieldjobcarddetails.this).log_out();
                                return true;
                            case R.id.jobcard:
                                Intent i=new Intent(getApplicationContext(), Jobcarddetails.class);
                                startActivity(i);
                                return true;


                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menu);
                popupMenu.show();
                ;
            }
        });
    }
}
