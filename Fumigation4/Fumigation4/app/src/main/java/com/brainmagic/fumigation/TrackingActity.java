package com.brainmagic.fumigation;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationSettingsResult;

import net.bohush.geometricprogressview.GeometricProgressView;

import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class TrackingActity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks,ResultCallback<LocationSettingsResult>
    ,GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener,com.google.android.gms.location.LocationListener,LocationListener {
    private Button checkbutton;
    private LocationManager mlocationManager;
    private int MIN_DISTANCE_CHANGE_FOR_UPDATES=500;
    private GoogleApiClient mGoogleApiClient;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private int REQUEST_CHECK_SETTINGS = 100;
    private static final int THREE_MINUTES = 3* 60 * 1000;
    private Intent servicbackground;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private GeometricProgressView progressView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_actity);
        checkbutton=(Button)findViewById(R.id.check_button);
         progressView = (GeometricProgressView) findViewById(R.id.progress);
         myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
         editor = myshare.edit();
         mlocationManager=(LocationManager)this.getSystemService(LOCATION_SERVICE);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

    }
}
