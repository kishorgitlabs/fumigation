package com.brainmagic.fumigation;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.TooltipCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import alert.Alertbox;
import logout.Logout;

public class SupervisorHomeActivity extends AppCompatActivity {
    //intialize the Details
    private LinearLayout jobcard,attendance,material,viewmaterial,tracking,visit,ViewVisit1;
    private ImageView menu;
    private TextView type;
    private Alertbox box = new Alertbox(this);
    //local Database intialize
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    TextView ASPName;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supervisor_home);
        //local Database Declaration
        jobcard=(LinearLayout)findViewById(R.id.jobcard);
        attendance=(LinearLayout)findViewById(R.id.attendance);
        visit=(LinearLayout)findViewById(R.id.visit);
        ViewVisit1=(LinearLayout)findViewById(R.id.ViewVisit1);
        type=(TextView)findViewById(R.id.type);
        viewmaterial=(LinearLayout)findViewById(R.id.viewmaterial);
         material=(LinearLayout)findViewById(R.id.material);
      // tracking=(LinearLayout)findViewById(R.id.tracking);
        menu=(ImageView)findViewById(R.id.menu);
        ASPName=(TextView)findViewById(R.id.aspname) ;
        //local Database declare
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        name=myshare.getString("branchname","");
        editor.commit();
        //asign the service engineer Name
        ASPName.setText(myshare.getString("name","" ));
        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(SupervisorHomeActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(SupervisorHomeActivity.this).log_out();
                                return true;
                            case R.id.changepassword:
                                Intent i2=new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;
                            case R.id.attendance:
                                 Intent i3=new Intent(getApplicationContext(), AttendanceViewNewActivity.class);
                                 startActivity(i3);
                                 return true;
                            case R.id.visitreport:
                                Intent i4=new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i4);
                                return true;
                            case R.id.jobcard:
                                Intent i5=new Intent(getApplicationContext(), Jobcarddetails.class);
                                startActivity(i5);
                                return true;
                            case R.id.viewvisit:
                                Intent i6=new Intent(getApplicationContext(), ViewReportActivity.class);
                                startActivity(i6);
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.menuhome);
                popupMenu.show();
                ;
            }
        });
        jobcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(), Jobcarddetails.class);
                startActivity(i);
            }
        });
        attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                    Intent i=new Intent(getApplicationContext(), AttendanceCrashActivity.class);
                    startActivity(i);
                }else{
                    Intent i=new Intent(getApplicationContext(), AttendanceCrashActivity.class);
                    startActivity(i);
                }

            }
        });

        visit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                    Intent i=new Intent(getApplicationContext(), VisitReportActivity.class);
//                    startActivity(i);
                Intent i=new Intent(getApplicationContext(), VisitReportFragment.class);
                startActivity(i);


            }
        });
        ViewVisit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(), ViewReportActivity.class);
                startActivity(i);
            }
        });
       material.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), MaterialRequestActivity.class);
                startActivity(i);
              //  AlertMaterial();

            }
        });

        viewmaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               AlertViewMaterial();

//                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class);
//                startActivity(i);
            }
        });
        attendance.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Intent i=new Intent(getApplicationContext(), AttendanceActivity.class);
                startActivity(i);
                TooltipCompat.setTooltipText(view, getApplicationContext().getText(R.string.myString));

                return true;
            }
        });
    }

    private void AlertViewMaterial() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                SupervisorHomeActivity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxviewmaterial, null);


        alertDialogbox.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(true);
        alertDialogbox.setView(dialogView);
        Button pendingmaterial = (Button) dialogView.findViewById(R.id.textView1);
        Button approvedmaterial = (Button) dialogView.findViewById(R.id.textView2);
       // Button receivedmaterial = (Button) dialogView.findViewById(R.id.textView3);
        pendingmaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("Tiltle","Pending");
                startActivity(i);
                alertDialogbox.dismiss();

            }
        });

        approvedmaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("Tiltle","Approved");
                startActivity(i);
                alertDialogbox.dismiss();

            }
        });
        alertDialogbox.show();
//        receivedmaterial.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("Tiltle","Received");
//                startActivity(i);
//                alertDialogbox.dismiss();
//
//            }
//        });
        alertDialogbox.show();
    }

    private void alertstorevalues() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                SupervisorHomeActivity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxpendingbranch, null);
        alertDialogbox.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(true);
        alertDialogbox.setView(dialogView);

        Button requestforbranch = (Button) dialogView.findViewById(R.id.textView1);
        Button requestforstore = (Button) dialogView.findViewById(R.id.textView2);
        requestforbranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("status","pending store");
                startActivity(i);
                alertDialogbox.dismiss();
            }
        });

        requestforstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("status","Approved store");
                startActivity(i);
                alertDialogbox.dismiss();
            }
        });
        alertDialogbox.show();
    }

    private void alertPendingvalues() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                SupervisorHomeActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxpendingbranch, null);
        alertDialogbox.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(true);
        alertDialogbox.setView(dialogView);
        Button requestforbranch = (Button) dialogView.findViewById(R.id.textView1);
        Button requestforstore = (Button) dialogView.findViewById(R.id.textView2);
        requestforbranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("status","pending branch");
                startActivity(i);
                alertDialogbox.dismiss();
            }
        });

        requestforstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("status","Approved branch");
                startActivity(i);
                alertDialogbox.dismiss();
            }
        });
        alertDialogbox.show();

    }

    private void AlertMaterial() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                SupervisorHomeActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxmaterial, null);
        alertDialogbox.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(true);
        alertDialogbox.setView(dialogView);
        Button requestforbranch = (Button) dialogView.findViewById(R.id.textView1);
        Button requestforstore = (Button) dialogView.findViewById(R.id.textView2);
       requestforbranch.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent i=new Intent(getApplicationContext(), MaterialRequestActivity.class).putExtra("Tiltle","Request for branch");
               startActivity(i);
               alertDialogbox.dismiss();
           }
       });

       requestforstore.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent i=new Intent(getApplicationContext(), MaterialRequestActivity.class).putExtra("Tiltle","Request for Store");
               startActivity(i);
               alertDialogbox.dismiss();
           }
       });
        alertDialogbox.show();
    }


    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
