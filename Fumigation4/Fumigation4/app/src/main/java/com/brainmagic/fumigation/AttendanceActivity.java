package com.brainmagic.fumigation;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.halilibo.bettervideoplayer.subtitle.CaptionsView;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.TransformerUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import alert.Alertbox;
import api.models.attendance.AttendanceData;
import api.models.attendance.Attendancename;
import api.models.description.DescriptionData;
import api.models.workreport.LocationData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import logout.Logout;
import network.NetworkConnection;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AttendanceActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks, ResultCallback<LocationSettingsResult>,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    //intilize the details...
    private ListView listView;
    private static final int PERMISSION_REQUEST = 100;
    private ImageView menu,back,attendancetime;
    private AlertDialog alertDialog;
    private Alertbox box = new Alertbox(this);
    private Connection connection;
   // private ArrayList<String> empidlist,namelist;

    private Statement statement;
    private ResultSet resultSet;
    private ProgressDialog loading;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private MaterialSpinner worklocate;
    private TextView designation,address2,ename;
    private String jobcard,jobcardid,selectname;
    private List<Attendancename>AttendanceData;
    private List<String>locationdata;
    List<Attendancename>namelist;
    private String description;
    double latitude;
    double longitude;
    LocationHelper locationHelper;
    String selectwork;
    private String name,selectid;
    private boolean ischeck=false;
    private List<String> getnameList,getIdlist,getCustomerList;

    // location
    private Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    private static final String TAG = "";
    private int REQUEST_CHECK_SETTINGS = 100;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    String address, address1, city, state, country, postalCode,title,selectedempid = "";
    private Button attendancebtn;
    private String employeename,design;
    private String location;
    private Boolean ischeckin;
    private Boolean timing =true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        //get shared preference..
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        //declare the location
        mLastLocation=new Location(location);
        //find the variable name...
      //  listView=(ListView)findViewById(R.id.listatt);
        ename=(TextView)findViewById(R.id.ename);
        worklocate=(MaterialSpinner)findViewById(R.id.worklocate);
        designation=(TextView)findViewById(R.id.designation);
        address2=(TextView)findViewById(R.id.address);
        attendancebtn=(Button)findViewById(R.id.attendancebtn);
        back=(ImageView)findViewById(R.id.back);
        ischeck=Boolean.getBoolean(getIntent().getStringExtra(""));
        attendancetime=(ImageView)findViewById(R.id.attendancetime);
        employeename=myshare.getString("name","");
        design=myshare.getString("usertype","");
        timing=myshare.getBoolean("istiming",timing);
        ename.setText(employeename);
        designation.setText(design);
        getnameList=new ArrayList<>();
        getIdlist=new ArrayList<>();
        getCustomerList=new ArrayList<>();
        namelist=new ArrayList<>();
        menu=(ImageView)findViewById(R.id.menu);
       worklocate.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
           @Override
           public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
               //select work...
               selectwork=item.toString();
           }
       });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if(timing==false){
            attendancebtn.setText("Out Time");
        }else {
            //attendancebtn.setText("Already Punched");
        }

        attendancebtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if(ename.getText().toString().equals("Select Name")){
                    StyleableToast st = new StyleableToast(AttendanceActivity.this, "Enter Employee name !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(AttendanceActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                } else if(designation.getText().toString().equals("")){
                    StyleableToast st = new StyleableToast(AttendanceActivity.this, "Enter your designation !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(AttendanceActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                } else if(address2.getText().toString().equals("")){
                    StyleableToast st = new StyleableToast(AttendanceActivity.this, "Enter your address!", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(AttendanceActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                }  else if(worklocate.getText().equals("Select location")){
                    StyleableToast st = new StyleableToast(AttendanceActivity.this, "Enter your location !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(AttendanceActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                } else {
                       //fingerprint....
                       FingerprintManager fingerprintManager = (FingerprintManager) AttendanceActivity.this.getSystemService(Context.FINGERPRINT_SERVICE);
                        if(fingerprintManager.isHardwareDetected()){
                            //activity...
                            Intent i = new Intent(AttendanceActivity.this, FingerprintActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            editor.putString("worklocation",selectwork);
                            editor.putBoolean("istiming",true);
                            editor.commit();
                            startActivity(i);

                    }else {
                            //activity...
                        Intent i = new Intent(AttendanceActivity.this, AttendanceViewActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        editor.putString("worklocation",selectwork);
                        editor.putBoolean("istiming",true);
                        editor.commit();
                        startActivity(i);
                    }


                 /* Intent i = new Intent(AttendanceActivity.this, AttendanceViewActivity.class).putExtra("worklocation", selectwork);
                     i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                     startActivity(i);*/

                        // Everything is ready for fingerprint authentication

//
//                    Intent i = new Intent(AttendanceActivity.this, AttendanceViewActivity.class).putExtra("worklocation", selectwork);
//                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(i);
                }
            }
        });

        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(AttendanceActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(AttendanceActivity.this).log_out();
                                return true;
                            case R.id.changepassword:
                                Intent i2=new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;
                            case R.id.visitreport:
                                Intent i4=new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i4);
                                return true;
                            case R.id.jobcard:
                                Intent i3=new Intent(getApplicationContext(), Jobcarddetails.class);
                                startActivity(i3);
                                return true;

                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menuattendance);
                popupMenu.show();
                ;
            }
        });
       attendancetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu=new PopupMenu(AttendanceActivity.this,view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(AttendanceActivity.this).log_out();
                                return true;
                            case R.id.view_Attendance:
                                Intent i2=new Intent(getApplicationContext(), ViewAttendance.class);
                                startActivity(i2);
                                return true;
                            case R.id.Travelhistory:
                                Intent i5=new Intent(getApplicationContext(), TravelHistoryActivity.class);
                                startActivity(i5);
                                return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.attendancemenu);
                popupMenu.show();

            }
        });

       /* ename.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectname=item.toString();
                if(!selectname.equals("Select Name")){
                    selectedempid= getCustomerList.get(getnameList.indexOf(selectname) - 1);

                }
                //selectid=getCustomerList.get(getIdlist.indexOf(selectname)-1);
                Getdescription();
            }

        });*/
        // location
        //Check If Google Services Is Available
        if (getServicesAvailable()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
            //  Toast.makeText(this, "Google Service Is Available!!", Toast.LENGTH_SHORT).show();
        }
        isGpsOn();
        NetworkConnection isnet = new NetworkConnection(AttendanceActivity.this);
        if (isnet.CheckInternet()) {
            //location...
            GetLocation();

        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }

        //CheckInternet();
        //checkinternet();
    }
//description..
    private void Getdescription() {
        try {
            final ProgressDialog loading = ProgressDialog.show(AttendanceActivity.this, "Fumigation", "Loading...", false, false);
            final APIService service = RetrofitClient.getApiService();
            Call<DescriptionData> call = service.description(selectedempid);
            call.enqueue(new Callback<DescriptionData>() {
                @Override
                public void onResponse(Call<DescriptionData> call, Response<DescriptionData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            description = response.body().getData();
                            if(selectname.equals("Select Name")){
                                Toast.makeText(getApplicationContext(),"Select Name",Toast.LENGTH_SHORT);
                            }else{
                                //set the desination...
                                designation.setText(description);

                            }


                        } else if (response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<DescriptionData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*private void GetemployeeName() {
        try {
            final ProgressDialog loading = ProgressDialog.show(AttendanceActivity.this, "Fumigation", "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<AttendanceData> call;
            call = service.attendance();
            call.enqueue(new Callback<AttendanceData>() {
                @Override
                public void onResponse(Call<AttendanceData> call, Response<AttendanceData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                           // namelist = response.body().getData();
                             setspinnerdata(response.body().getData());
                          //  name.add(0,"Select Name ");
                            //ename.setItems(name);


                        } else if (response.body().getResult().equals(" Not Success")) {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<AttendanceData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setspinnerdata(List<Attendancename> data) {

        List<Object> SiteIDObject = (List<Object>) CollectionUtils.collect(data, TransformerUtils.invokerTransformer("getId"));
        List<Object> SiteNameObject = (List<Object>) CollectionUtils.collect(data, TransformerUtils.invokerTransformer("getName"));
        List<Object> SiteCodeObject = (List<Object>) CollectionUtils.collect(data, TransformerUtils.invokerTransformer("getCustomerCode"));
       // List<String> sitename = new ArrayList<>();
//Thennavan1-0000000001
        getnameList = (List<String>) (Object) SiteNameObject;
        getIdlist = (List<String>) (Object) SiteIDObject;
        getCustomerList = (List<String>) (Object) SiteCodeObject;




        getnameList = new ArrayList<String>(new LinkedHashSet<String>(getnameList));
       // sitename = new ArrayList<String>(new LinkedHashSet<String>(sitename));
        getIdlist = new ArrayList<String>(new LinkedHashSet<String>(getIdlist));
        getCustomerList = new ArrayList<String>(new LinkedHashSet<String>(getCustomerList));


        getnameList.add(0, "Select Name");
        ename.setItems(getnameList);
    }*/
     //Get location...
    private void GetLocation() {
        try {
            final ProgressDialog loading = ProgressDialog.show(AttendanceActivity.this, "Fumigation", "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<LocationData> call;
            call = service.location();
            call.enqueue(new Callback<LocationData>() {
                @Override
                public void onResponse(Call<LocationData> call, Response<LocationData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            //location..
                            locationdata = response.body().getData();
                            locationdata.add(0,"Select location");
                            worklocate.setItems(locationdata);


                        } else if (response.body().getResult().equals(" Not Success")) {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<LocationData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


//gps...
    private void isGpsOn() {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startLocationUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(AttendanceActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.e(TAG, "Exception : " + e);
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e(TAG, "Location1 settings are not satisfied.");
                        break;
                }
            }
        });

    }

    public boolean getServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {

            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
            return false;
        } else {
            Toast.makeText(this, "Cannot Connect To Play Services", Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getServicesAvailable()) {
            buildGoogleApiClient();
            if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
                // mGoogleApiClient.stopAutoManage(this);
                startLocationUpdates();
            }
        }


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
/*
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //  stopService(new Intent(PlacePickerActivity.this, GPSTracker.class));
        stopLocationUpdates();


    }*/

    // Creating google api client object
    protected synchronized void buildGoogleApiClient() {
        try {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .enableAutoManage(this, this).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Creating location request object
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(AppConstants.UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(AppConstants.FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(AppConstants.DISPLACEMENT);
    }


    //Stopping location updates
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }


    //Ask permissions
    public void AskLocationPermission() {
        if (EasyPermissions.hasPermissions(AttendanceActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            if (CheckLocationIsEnabled()) {
                // if location is enabled show place picker activity to use
                startLocationUpdates();

            } else {

                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(mLocationRequest);

                PendingResult<LocationSettingsResult> result =
                        LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @Override
                    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                        final Status status = locationSettingsResult.getStatus();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                startLocationUpdates();
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    status.startResolutionForResult(
                                            AttendanceActivity.this,
                                            REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException e) {
                                    Log.e(TAG, "Exception : " + e);
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e(TAG, "Location1 settings are not satisfied.");
                                break;
                        }
                    }
                });


            }
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_ask_again), MY_PERMISSIONS_REQUEST_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);


        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {

        if (CheckLocationIsEnabled()) {
            // if location is enabled show place picker activity to user
            startLocationUpdates();
        } else {
            // if location is not enabled show request to enable location to user
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);
            builder.setAlwaysShow(true);
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(
                            mGoogleApiClient,
                            builder.build()

                    );
            result.setResultCallback(this);
        }

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Some permissions have been denied
        // if location is enabled show place picker activity to user
        // AskLocationPermission();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }


    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {


        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // NO need to show the dialog;
                ConvertToAddress();
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location1 settings are not satisfied. Show the user a dialog
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(AttendanceActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    //failed to show
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location1 settings are unavailable so not possible to show any dialog now
                break;
        }
    }
    //convert address...
    private void ConvertToAddress() {
        try {
            new GeocodeAsyncTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        }catch(Exception e){
            e.printStackTrace();
        }



    }

    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";


        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(AttendanceActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }

                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            } else {
                new GetGeoCodeAPIAsynchTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            }


            return null;
        }

        protected void onPostExecute(Address addresss) {

            if (addresss == null) {
                new GetGeoCodeAPIAsynchTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            } else {
                String address = addresss.getAddressLine(0); //0 to obtain first possible address
                String city = addresss.getLocality();
                String state = addresss.getAdminArea();
                //create your custom title
                String title = city + "-" + state;
//                address2.setText(address +
//                        "\n"
//                        + title);
                editor.putString("address",address +
                        "\n"
                        + title);
             editor.commit();
             Geocoder geocoder=new Geocoder(AttendanceActivity.this);
                try {
                    ArrayList<Address>addresses=(ArrayList<Address>)geocoder.getFromLocationName("karur",50);
                    for(Address address3 :addresses){
                       double lat =address3.getLatitude();
                       double lon=address3.getLongitude();
                        address2.setText(lat +
                                "\n"
                                + lon);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class GetGeoCodeAPIAsynchTask extends AsyncTask<Double, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String[] doInBackground(Double... latlang) {
            String response;
            try {
                String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang[0] + "," + latlang[1];
                Log.v("URL", URL);
                response = getLatLongByURL(URL);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                String address = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(0).getString("long_name");

                String city = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(2).getString("long_name");

                String state = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(4).getString("long_name");

                String title = city + "-" + state;


                address2.setText(address + "\n" + title);

                Log.d("Address", "" + address);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    //Starting the location updates
    protected void startLocationUpdates() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                ConvertToAddress();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
                Log.d(TAG, "Permission Not Granted");

            }

        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            ConvertToAddress();

        }
    }

    private boolean CheckLocationIsEnabled() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        if (mGoogleApiClient != null)
            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
        if (mLastLocation == null) {
            return false;
        } else {

            return false;
        }
    }


    private void getAddress() {
        Address locationAddress;

        locationAddress = locationHelper.getAddress(latitude, longitude);

        if (locationAddress != null) {

            String address = locationAddress.getAddressLine(0);


            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1;

                if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += " - " + postalCode;
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state;

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country;


            }

        } else
            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
        //showToast("Something went wrong");

    }




    public void onReceiveResult(@NonNull LocationSettingsResult locationSettingsResult) throws RemoteException {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }



    /*private void GetEmployeename() {
        try {
            final ProgressDialog loading = ProgressDialog.show(AttendanceActivity.this, "Fumigation", "Loading...", false, false);
            final APIService service = RetrofitClient.getApiService();
            Call<AttendanceData> call = service.attendance();
            call.enqueue(new Callback<AttendanceData>() {
                @Override
                public void onResponse(Call<AttendanceData> call, Response<AttendanceData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            Attendancename = response.body().getData();




                        } else if (response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<AttendanceData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }
}
