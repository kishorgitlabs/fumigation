package com.brainmagic.fumigation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import persistance.ServerConnection;
import adapter.ViewAttendanceAdapter;
import alert.Alertbox;
import logout.Logout;
import network.NetworkConnection;

public class ViewAttendance extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{
    //intlize the details...
    private ListView listattendance;
    private ImageView menu, back;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    private Button filter;

    private boolean mAutoHighlight;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance);
        //Get Shared preference...
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        //intiize the variable name...
        listattendance = (ListView) findViewById(R.id.listattendance);
        filter=(Button)findViewById(R.id.filter);
        menu = (ImageView) findViewById(R.id.menu);
        back = (ImageView) findViewById(R.id.back);
        id=myshare.getInt("id",0);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //calender...
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        (DatePickerDialog.OnDateSetListener) ViewAttendance.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );

                dpd.setAutoHighlight(mAutoHighlight);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ViewAttendance.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.home:
                                Intent i2=new Intent(getApplicationContext(), SupervisorHomeActivity.class);
                                startActivity(i2);
                                return true;
                            case R.id.menuLogout:
                                new Logout(ViewAttendance.this).log_out();
                                return true;
                            case R.id.jobcard:
                                Intent i=new Intent(getApplicationContext(), Jobcarddetails.class);
                                startActivity(i);
                                return true;
                            case R.id.attendance:
                                Intent i3=new Intent(getApplicationContext(), AttendanceViewNewActivity.class);
                                startActivity(i3);
                                return true;
                            case R.id.viewvisit:
                                Intent i5=new Intent(getApplicationContext(), ViewReportActivity.class);
                                startActivity(i5);
                                return true;
                            case R.id.visitreport:
                                Intent i4=new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i4);
                                return true;
                            case R.id.changepassword:
                                Intent i6=new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i6);
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menu);
                popupMenu.show();
                ;
            }
        });
        //check the internet...
        checkInternet();
    }
     //Date set...
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String date = "You picked the following date: From- " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + " To " + dayOfMonthEnd + "/" + (monthOfYearEnd+1) + "/" + yearEnd;
        String FromDate = "";
        String Todate = "";

        Toast.makeText(ViewAttendance.this, date, Toast.LENGTH_LONG).show();

        if (monthOfYear < 10)
            FromDate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;

        if (monthOfYearEnd < 10)
            Todate = yearEnd + "-0" + (monthOfYearEnd + 1) + "-" + dayOfMonthEnd;

//        new GetTravelHistory().execute("select datetime from Travelcoordinates where S_id = '" + salesID + "' and convert(nvarchar,datetime) between '" + FromDate + "' and '" + Todate + "' and IsCancelled ='0' order by datetime desc");
        new CheckAttendance().execute("select id,date,Name,InTime,OutTime from AttendanceTable where EmpId = '" + id + "' and date between '" + FromDate + "' and '" + Todate + "' order by date desc");

    }
    //check intenet...
    private void checkInternet() {
        NetworkConnection networkConnection = new NetworkConnection(ViewAttendance.this);
        if (networkConnection.CheckInternet()) {
            //method...
            new CheckAttendance().execute("Select * from AttendanceTable where EmpId = '"+ id +"'");
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }

    //method...
    private class CheckAttendance extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private SQLiteDatabase db;
        private List<String> namelist, DateList, Intimelist, outtimelist;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ViewAttendance.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            namelist = new ArrayList<>();
            DateList = new ArrayList<>();
            Intimelist = new ArrayList<>();
            outtimelist = new ArrayList<>();


//            descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//            quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//            productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);


        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = strings[0];
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    namelist.add(resultSet.getString("Name"));
                    DateList.add(resultSet.getString("Date"));
                    Intimelist.add(resultSet.getString("InTime"));
                    outtimelist.add(resultSet.getString("OutTime"));


                }
                if (!namelist.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                //adapter..
             ViewAttendanceAdapter viewAttendanceAdapter=new ViewAttendanceAdapter(ViewAttendance.this,namelist,DateList,Intimelist,outtimelist);
             listattendance.setAdapter(viewAttendanceAdapter);

            } else if (s.equals("empty")) {
                box.showAlertbox("No data found!");
            } else {
                box.showAlertbox("Kindly check your Internet Connection");
            }
        }
    }
}
