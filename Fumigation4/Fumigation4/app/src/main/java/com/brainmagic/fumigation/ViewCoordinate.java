package com.brainmagic.fumigation;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import persistance.ServerConnection;
import adapter.ViewCordinateAdapter;
import alert.Alertbox;
import network.NetworkConnection;

public class ViewCoordinate extends AppCompatActivity {
    //intilize the details...
    private ListView listView;
    private ImageView menu, back;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    private List<String> Timelist, addresslist;
    private Integer travelid, sid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_coordinate);
        //find the listview...
        listView = (ListView) findViewById(R.id.travelhistory1);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        travelid = myshare.getInt("trackid", 0);
        sid = myshare.getInt("sid", 0);
        Timelist = new ArrayList<>();
        addresslist = new ArrayList<>();
        back=(ImageView)findViewById(R.id.back) ;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        //check the internet...
        checkInternet();

    }
    //check the internet...
    private void checkInternet() {
        NetworkConnection networkConnection = new NetworkConnection(ViewCoordinate.this);
        if (networkConnection.CheckInternet()) {
            //method...
            new Viewcordinate().execute();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }
    //method...
    private class Viewcordinate extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private Integer id;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ViewCoordinate.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            id = myshare.getInt("id", 0);


//            descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//            quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//            productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);


        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = "select distinct * from coordinate where s_id ='" + sid + "'and TrackId = '" + travelid + "'";
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    Timelist.add(resultSet.getString("timedate"));
                    addresslist.add(resultSet.getString("address"));


                }
                if (!Timelist.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                //adapter...
                ViewCordinateAdapter viewAttendanceAdapter = new ViewCordinateAdapter(ViewCoordinate.this, Timelist, addresslist);
                listView.setAdapter(viewAttendanceAdapter);

            } else if (s.equals("empty")) {
                box.showNegativebox("No data found!");
            } else {
                box.showNegativebox("Kindly check your Internet Connection");
            }
        }
    }
}
