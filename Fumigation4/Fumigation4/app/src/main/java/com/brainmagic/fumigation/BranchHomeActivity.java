package com.brainmagic.fumigation;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import logout.Logout;

public class BranchHomeActivity extends AppCompatActivity {
    private LinearLayout material,viewmaterial;
    TextView BranchName;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ImageView menu;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_home);
        BranchName=(TextView) findViewById(R.id.aspname);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        menu=(ImageView)findViewById(R.id.menu);
        viewmaterial=(LinearLayout)findViewById(R.id.viewmaterial);
        material=(LinearLayout)findViewById(R.id.material);
        type=myshare.getString("branchname","");
        BranchName.setText(type);
        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(BranchHomeActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(BranchHomeActivity.this).log_out();
                                return true;
                            case R.id.changepassword:
                                Intent i2=new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;
                            case R.id.Materialrequest:
                                AlertMaterial();
                                return true;

                            case R.id.Viewaterialrequest:
                                AlertViewMaterial();
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menubranchhome);
                popupMenu.show();
                ;
            }
        });
        material.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertMaterial();

            }
        });
        viewmaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertViewMaterial();
//                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class);
//                startActivity(i);
            }
        });
    }
    private void AlertMaterial() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                BranchHomeActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxmaterial, null);
        alertDialogbox.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(true);
        alertDialogbox.setView(dialogView);
        Button requestforbranch = (Button) dialogView.findViewById(R.id.textView1);
        Button requestforstore = (Button) dialogView.findViewById(R.id.textView2);
        requestforbranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), MaterialRequestActivity.class).putExtra("Tiltle","Branch");

                startActivity(i);
                alertDialogbox.dismiss();
            }
        });

        requestforstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), MaterialRequestActivity.class).putExtra("Tiltle","Store");
                startActivity(i);
                alertDialogbox.dismiss();
            }
        });
        alertDialogbox.show();
    }
    private void AlertViewMaterial() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                BranchHomeActivity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxviewmaterial, null);


        alertDialogbox.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(true);
        alertDialogbox.setView(dialogView);
        Button requestforbranch = (Button) dialogView.findViewById(R.id.textView1);
        Button requestforstore = (Button) dialogView.findViewById(R.id.textView2);
        requestforbranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("Tiltle","Request for branch");
//                startActivity(i);
                alertPendingvalues();
                alertDialogbox.dismiss();
            }
        });

        requestforstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("Tiltle","Request for Store");
//                startActivity(i);
                alertstorevalues();
                alertDialogbox.dismiss();
            }
        });
        alertDialogbox.show();
    }

    private void alertstorevalues() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                BranchHomeActivity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxpendingbranch, null);
        alertDialogbox.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(true);
        alertDialogbox.setView(dialogView);

        Button requestforbranch = (Button) dialogView.findViewById(R.id.textView1);
        Button requestforstore = (Button) dialogView.findViewById(R.id.textView2);
        requestforbranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("status","pending").putExtra("tittlecard","Store");
                startActivity(i);
                alertDialogbox.dismiss();
            }
        });

        requestforstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("status","Approved").putExtra("tittlecard","Store");
                startActivity(i);
                alertDialogbox.dismiss();
            }
        });
        alertDialogbox.show();
    }

    private void alertPendingvalues() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                BranchHomeActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxpendingbranch, null);
        alertDialogbox.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(true);
        alertDialogbox.setView(dialogView);
        Button requestforbranch = (Button) dialogView.findViewById(R.id.textView1);
        Button requestforstore = (Button) dialogView.findViewById(R.id.textView2);
        requestforbranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("status","pending").putExtra("tittlecard","Branch");;
                startActivity(i);
                alertDialogbox.dismiss();
            }
        });

        requestforstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("status","Approved").putExtra("tittlecard","Branch");
                startActivity(i);
                alertDialogbox.dismiss();
            }
        });
        alertDialogbox.show();

    }
}
