package com.brainmagic.fumigation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.List;

import adapter.ViewMaterialAdapter;
import alert.Alertbox;
import api.models.viewmaterialData.ViewMaterial;
import api.models.viewmaterialData.ViewMaterialData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import logout.Logout;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewMaterialRequestActivity extends AppCompatActivity {
    //intilize the details...
    private ListView listView;
    private NetworkConnection networkConnection;
    private Alertbox box1 = new Alertbox(this);
    private ImageView menu, back;
    private String status,Tiltle;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private List<ViewMaterial> data;
    private ViewMaterialAdapter viewMaterialAdapter;
    private MaterialSearchBar searchBar;
    private TextView tittlenew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_material_request);
        //ffind the variable name...
        listView=(ListView) findViewById(R.id.list_material);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        menu = (ImageView) findViewById(R.id.menu);
        back = (ImageView) findViewById(R.id.back);
        tittlenew = (TextView) findViewById(R.id.tittlenew);
        searchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
        searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("LOG_TAG", getClass().getSimpleName() + " text changed " + searchBar.getText());
                if(viewMaterialAdapter!=null) {
                    viewMaterialAdapter.filter(searchBar.getText());
                    viewMaterialAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

        });
        Tiltle=getIntent().getStringExtra("Tiltle");
        if(Tiltle.equals("Pending")){
            tittlenew.setText("Pending Materials Request");
        }else if(Tiltle.equals("Approved")){
            tittlenew.setText("Approved Materials Request");
        }else{
            tittlenew.setText("Received Materials");
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ViewMaterialRequestActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(ViewMaterialRequestActivity.this).log_out();
                                return true;
                            case R.id.changepassword:
                                Intent i2 = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;

                            case R.id.Materialrequest:
                                Intent i=new Intent(getApplicationContext(), MaterialRequestActivity.class);
                                startActivity(i);
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menubranchviewmaterial);
                popupMenu.show();
                ;
            }
        });
        //check the internet...
        checkInterent();

    }


    //check the internet...
    private void checkInterent() {
        networkConnection=new NetworkConnection(ViewMaterialRequestActivity.this);
        if(networkConnection.CheckInternet()){
           //method...
            viewmaterials();

        }else{

            box.showAlertbox(getResources().getString(R.string.no_internet));
        }

    }
    //method...
    private void viewmaterials() {
        try {
            final ProgressDialog loading = ProgressDialog.show(ViewMaterialRequestActivity.this, "Fumigation", "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<ViewMaterialData> call = service.viewmaterial(myshare.getString("branchcode",""),Tiltle);
            call.enqueue(new Callback<ViewMaterialData>() {
                @Override
                public void onResponse(Call<ViewMaterialData> call, Response<ViewMaterialData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data = (List<ViewMaterial>) response.body().getData();
                             if(data.size() == 0){
                                 final AlertDialog alertDialogbox = new AlertDialog.Builder(
                                         ViewMaterialRequestActivity.this).create();

                                 LayoutInflater inflater = getLayoutInflater();
                                 View dialogView = inflater.inflate(R.layout.alertboxlinear, null);
                                 alertDialogbox.setView(dialogView);

                                 TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                                 Button okay = (Button) dialogView.findViewById(R.id.okay);
                                 log.setText("No Material");
                                 okay.setOnClickListener(new View.OnClickListener() {

                                     @Override
                                     public void onClick(View arg0) {
                                         // TODO Auto-generated method stub
                                         Intent go = new Intent(ViewMaterialRequestActivity.this, SupervisorHomeActivity.class);
                                         go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                         startActivity(go);
                                         alertDialogbox.dismiss();
                                         finish();
                                     }
                                 });
                                 alertDialogbox.show();
                             }else {
                                 //aDAPTER...
                                 viewMaterialAdapter = new ViewMaterialAdapter(ViewMaterialRequestActivity.this, data);
                                 listView.setAdapter(viewMaterialAdapter);
                             }



                        } else if (response.body().getResult().equals("NotSuccess")) {
                            final AlertDialog alertDialogbox = new AlertDialog.Builder(
                                    ViewMaterialRequestActivity.this).create();

                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.alertboxlinear, null);
                            alertDialogbox.setView(dialogView);

                            TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                            Button okay = (Button) dialogView.findViewById(R.id.okay);
                            log.setText("No Material");
                            okay.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View arg0) {
                                    // TODO Auto-generated method stub
                                    Intent go = new Intent(ViewMaterialRequestActivity.this, SupervisorHomeActivity.class);
                                    go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(go);
                                    alertDialogbox.dismiss();
                                    finish();
                                }
                            });
                            alertDialogbox.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<ViewMaterialData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    }

