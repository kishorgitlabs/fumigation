package com.brainmagic.fumigation;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import alert.Alertbox;
import logout.Logout;

public class FieldOperatorHomeActivity extends AppCompatActivity {
    private LinearLayout jobcard;
    private ImageView menu;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    TextView ASPName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_field_operator_home);
        jobcard=(LinearLayout)findViewById(R.id.jobcard);
        menu=(ImageView)findViewById(R.id.menu);
        ASPName=(TextView)findViewById(R.id.aspname) ;
        ASPName.setText(myshare.getString("name","" ));
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();

        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(FieldOperatorHomeActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(FieldOperatorHomeActivity.this).log_out();
                                return true;
                            case R.id.jobcard:
                                Intent i=new Intent(getApplicationContext(), fieldjobcarddetails.class);
                                startActivity(i);
                                return true;


                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menu);
                popupMenu.show();
                ;
            }
        });
        jobcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(), fieldjobcarddetails.class);
                startActivity(i);
            }
        });
    }
    }

