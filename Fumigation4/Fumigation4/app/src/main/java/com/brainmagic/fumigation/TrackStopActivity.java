package com.brainmagic.fumigation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import service.BGService;


public class TrackStopActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_stop);

    }

    public void StopTrack(View view) {
        stopService(new Intent(TrackStopActivity.this, BGService.class));
        finish();
    }

    public void CancelStoping(View view) {
        startService(new Intent(TrackStopActivity.this, BGService.class));
//        new Toasts(this).ShowSuccessToast("Tracking not stopped");
        finish();
    }
}
