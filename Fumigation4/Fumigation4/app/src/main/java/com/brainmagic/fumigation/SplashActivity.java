package com.brainmagic.fumigation;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.lang.reflect.Method;
import java.util.List;

import alert.Alertbox;
import api.models.ImeiData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import pub.devrel.easypermissions.EasyPermissions;
import register.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks{
    //intialize the Details
    private ImageView splash;
    private Alertbox box = new Alertbox(this);
    private TelephonyManager telephonyManager;
    String Imei2, imei1;
    private ProgressBar progressBar;
    private final int MY_PERMISSIONS_READ_PHONE_STATE = 1001;
    private final int MY_PERMISSIONS_SMS_PHONE_STATE = 1002;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    //intialize the local Storage...
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private boolean islogin =false;
   /* //private static final long SPLASH_DISPLAY_LENGTH = 2000;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private boolean islogin;
    String UserType;*/

    @Override
    @TargetApi(Build.VERSION_CODES.M)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //local Database Declaration
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
       islogin = myshare.getBoolean("isLogin", false);
        //Assign Methods
        AskLocationPermission();
    }

    public void AskLocationPermission() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_PHONE_STATE) && EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            openCamera1();
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(this, getString(R.string.sms), MY_PERMISSIONS_READ_PHONE_STATE, Manifest.permission.READ_PHONE_STATE,Manifest.permission.ACCESS_COARSE_LOCATION);
//            EasyPermissions.requestPermissions(this, getString(R.string.deny), MY_PERMISSIONS_SMS_PHONE_STATE, Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE);
//            EasyPermissions.requestPermissions(this, getString(R.string.rationale_ask_again), MY_PERMISSIONS_REQUEST_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);

        }
    }

//    public void AskLocationPermission() {
//        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_PHONE_STATE)  && EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
//            // Have permission, do the thing!
//            openCamera1();
//        } else {
//            // Request one permission
//            EasyPermissions.requestPermissions(this, getString(R.string.sms), MY_PERMISSIONS_READ_PHONE_STATE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_COARSE_LOCATION);
////            EasyPermissions.requestPermissions(this, getString(R.string.deny), MY_PERMISSIONS_SMS_PHONE_STATE, Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE);
////            EasyPermissions.requestPermissions(this, getString(R.string.rationale_ask_again), MY_PERMISSIONS_REQUEST_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);
//
//        }
//    }

    private void openCamera1() {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            //Get The Imei Numbers(Above Marshmellow)
            imei1 = tm.getDeviceId(2);
            Imei2 = tm.getDeviceId(1);


        }else
        {
            try {
                //Get The Imei Numbers(below Marshmellow)
                Class<?> telephonyClass = Class.forName(tm.getClass().getName());
                Class<?>[] parameter = new Class[1];
                parameter[0] = int.class;
                Method getFirstMethod = telephonyClass.getMethod("getDeviceId", parameter);
                Log.d("SimData", getFirstMethod.toString());
                Object[] obParameter = new Object[1];
                obParameter[0] = 0;
                TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                imei1 = (String) getFirstMethod.invoke(tm, obParameter);
                Log.d("SimData", "first :" + imei1);
                obParameter[0] = 1;
                Imei2 = (String) getFirstMethod.invoke(tm, obParameter);
                Log.d("SimData", "Second :" + Imei2);
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        //get the method Check the internet connection
        checkinternet();
    }

    private void checkinternet() {
        //Check the Interent
        NetworkConnection isnet = new NetworkConnection(SplashActivity.this);
        if (isnet.CheckInternet()) {
            saveimei();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }

    //Assign the Imei Numbers.
    @TargetApi(Build.VERSION_CODES.M)
    private void saveimei() {
        try {
            final ProgressDialog loading = ProgressDialog.show(SplashActivity.this, "Fumigation", "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<ImeiData> call = service.imei(imei1,Imei2);
            call.enqueue(new Callback<ImeiData>() {
                @Override
                public void onResponse(Call<ImeiData> call, Response<ImeiData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {

                            if(islogin)
                            {
                                    Intent i=new Intent(SplashActivity.this, SupervisorHomeActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                    finish();
                            }
                            else {
                                Intent i = new Intent(SplashActivity.this, LoginActivity.class).putExtra("imei1", Imei2).putExtra("Imei2", imei1);
                                startActivity(i);
                            }


                            editor.putString("imei1", Imei2);
                            editor.putString("Imei2", imei1);
                            editor.commit();
                        } else if (response.body().getResult().equals("InvalidUser")) {
                            dismatchImeinumbers();
                            //box.showAlertbox("Imei Numbers Does Not Match");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<ImeiData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dismatchImeinumbers() {
        MaterialDialog dialog = new MaterialDialog.Builder(SplashActivity.this)
                .title(R.string.fumigation)
                .content("Imei Numbers Does Not Match")
                .positiveText(R.string.ok)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        onBackPressed();
                    }
                })
                .show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

        if(requestCode==MY_PERMISSIONS_SMS_PHONE_STATE && requestCode==MY_PERMISSIONS_READ_PHONE_STATE)
            openCamera1();
        else {
//            finish();
        }
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_PHONE_STATE)  && EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            openCamera1();
        }
        else if(requestCode==MY_PERMISSIONS_READ_PHONE_STATE)
        {
            EasyPermissions.requestPermissions(this, getString(R.string.sms), MY_PERMISSIONS_READ_PHONE_STATE, Manifest.permission.READ_PHONE_STATE,Manifest.permission.ACCESS_FINE_LOCATION);
        }
        else {
            EasyPermissions.requestPermissions(this, getString(R.string.sms), MY_PERMISSIONS_REQUEST_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);
        }

//        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_PHONE_STATE)  && EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
//            // Have permission, do the thing!
//            openCamera1();
//        }
//        else if(requestCode==MY_PERMISSIONS_READ_PHONE_STATE)
//        {
//            EasyPermissions.requestPermissions(this, getString(R.string.sms), MY_PERMISSIONS_READ_PHONE_STATE, Manifest.permission.READ_PHONE_STATE,Manifest.permission.ACCESS_FINE_LOCATION);
//        }
//        else {
//            EasyPermissions.requestPermissions(this, getString(R.string.sms), MY_PERMISSIONS_REQUEST_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);
//        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        /*box.showAlertbox(getResources().getString(R.string.msg));*/
        AskLocationPermission();
    }
}


