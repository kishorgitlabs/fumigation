package service;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

;
import com.brainmagic.fumigation.AttendanceCrashActivity;
import com.brainmagic.fumigation.ConnectivityReceiver;
import com.brainmagic.fumigation.FingerprintActivity;
import com.brainmagic.fumigation.R;
import com.brainmagic.fumigation.TrackStopActivity;
import com.google.android.gms.location.LocationRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import persistance.ServerConnection;
import api.models.gps.GpsData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.location.GpsStatus.GPS_EVENT_STARTED;
import static android.location.GpsStatus.GPS_EVENT_STOPPED;

public class BGService extends Service {
    //Background service...fingtprint
    public static final String APIKEY =  "AIzaSyBRmYkOOy9QhrI53Fp3h_Tt8t7amWNa4Q0";
    private static final String TAG = "BGService";
    private static final int FIFTEEN_MINUTES = 15* 60 * 1000;
    static final int NOTIFICATION_ID = 543;
    private boolean isServiceRunning=false;
    public LocationManager locationManager;
    public MyLocationListener listener;
    private Location mLastLocation, mCurrentLocation;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String address,city,state,description,locate,emaildi,phonenumber,formatdate,formatedtime;
    int  empid;
    private double mLatitude;
    private double mLongtitude;
    int id,s_Id;
    Integer trackId;
    String name="noName",phoneType="";
    private int hours,minutes1,seconds1,currentmiiliseconds,Totalmiliseconds,exampleseconds;
    private CountDownTimer countDownTimer;
    private AttendanceCrashActivity attendanceCrashActivity;
    long hours1;
    String hour1,minutes,seconds;
    private ConnectivityReceiver mConnectivityReceiver;
    private static  final int MY_PERMISSIONS_REQUEST_SEND_SMS=0;
    private String s="Karthik";
    private boolean servuce=false;
    private Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        try {
            Log.d(TAG, "onCreate: " + isServiceRunning);
            startServiceWithNotification();
            getLocationDetails();
            myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
            editor = myshare.edit();
            name = myshare.getString("name", "");
            description = myshare.getString("description", "");
            address = myshare.getString("address", "");
            locate = myshare.getString("worklocation", "");
            emaildi = myshare.getString("email", "");
            phonenumber=myshare.getString("phonenumber","");
            formatdate = myshare.getString("formateddate", "");
            formatedtime = myshare.getString("formatedtime", "");
            //  empid=myshare.getString("sid","");
            empid = myshare.getInt("sid", 0);
            mLatitude = Double.parseDouble(myshare.getString("latitude", ""));
            mLongtitude = Double.parseDouble(myshare.getString("longitude", ""));
            trackId = myshare.getInt("trackid", 0);
//         //   mConnectivityReceiver = new ConnectivityReceiver(this);
//            phoneType = myshare.getString("fingerType", "");
//            Log.d(TAG,"FormatedDate: " +formatdate);
//            Log.d(TAG,"FormatedTime: " +formatedtime);
//            String[] hour=formatedtime.split(":");
//            hour1=hour[0];
//            minutes=hour[1];
//            seconds=hour[2];
//            Log.d(TAG,"hourssplit: " +hour1);
//            Log.d(TAG,"minutessplit: " +minutes);
//            Log.d(TAG,"secondssplit: " +seconds);
//            hours=Integer.parseInt(hour1) * 3600;
//            minutes1=Integer.parseInt(minutes) * 60;
//            seconds1=Integer.parseInt(seconds);
//            currentmiiliseconds=hours+minutes1+seconds1;
//            exampleseconds=(currentmiiliseconds * 1000)+120000;
//            //Totalmiliseconds = exampleseconds - currentmiiliseconds * 1000;
//            Totalmiliseconds=  86400000-currentmiiliseconds * 1000;
//            Log.d(TAG,"hoursint: " +hours);
//            Log.d(TAG,"minutesint: " +minutes1);
//            Log.d(TAG,"secondsint: " +seconds1);
//            Log.d(TAG,"currentint: " +currentmiiliseconds);
//            Log.d(TAG,"totalint: " +Totalmiliseconds);
//            countDownTimer=new CountDownTimer(Totalmiliseconds,1000) {
//                @Override
//                public void onTick(long millisUntilFinished) {
//                    hours1 = millisUntilFinished/1000;
//                    Log.d(TAG, "hours: "+hours1);
//                }
//
//                @Override
//                public  void onFinish() {
//                    Intent broadCastIntent = new Intent();
//                    broadCastIntent.setAction(AttendanceCrashActivity.BROADCAST_ACTION);
//                    broadCastIntent.putExtra("data", "In Time");
//                    sendBroadcast(broadCastIntent);
////                    Intent intent = new Intent("changethebutton");
////                    Bundle bundle = new Bundle();
////                    bundle.putString("SetStatus","In Time");
////                    intent.putExtras(bundle);
////                    LocalBroadcastManager.getInstance(BGServicenormal.this).sendBroadcast(intent);
//
//                    //attendanceCrashActivity.showworld();
//                    Log.d("Finish", String.valueOf(+hours1));
//                    servuce=true;
//                    editor.putBoolean("service", servuce);
//                    editor.commit();
////                countDownTimer.onFinish();
//                }
//            };
//            Log.d(TAG, "onCreate: name " + name + " id " + id + " trackId " + trackId + " s_id " + s_Id);
        }catch (Exception e)
        {

        }

    }
    void startServiceWithNotification() {
        try {
            if (isServiceRunning) {
//                Log.d(TAG, "startServiceWithNotification: returning true");
                return;
            }
            isServiceRunning = true;
//            Log.d(TAG, "startServiceWithNotification: returning false");
            Intent notificationIntent = new Intent(getApplicationContext(), FingerprintActivity.class);
            // notificationIntent.setAction(C.ACTION_MAIN);  // A string containing the action name
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent contentPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

            Notification notification = new NotificationCompat.Builder(this)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setTicker(getResources().getString(R.string.app_name))
                    .setContentText("Travel in Progress")
                    .setSmallIcon(R.drawable.route)
                    .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                    .setContentIntent(contentPendingIntent)
                    .setOngoing(true)
//                .setDeleteIntent(contentPendingIntent)  // if needed
                    .build();
            notification.flags = notification.flags | Notification.FLAG_NO_CLEAR;     // NO_CLEAR makes the notification stay when the user performs a "delete all" command
            startForeground(NOTIFICATION_ID, notification);
//            Log.d(TAG, "startServiceWithNotification: ");
        }catch (Exception r)
        {
            r.printStackTrace();
        }
    }

    public void getLocationDetails() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener = new MyLocationListener();
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, FIFTEEN_MINUTES, 500, listener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, FIFTEEN_MINUTES, 500, listener);
            locationManager.addGpsStatusListener(new android.location.GpsStatus.Listener()
            {
                public void onGpsStatusChanged(int event)
                {
                    switch(event)
                    {
                        case GPS_EVENT_STARTED:
                            // do your tasks
                            break;
                        case GPS_EVENT_STOPPED:
                         //   gpsstopprd();
                           /* String message = "Sorry! Not connected to gps";
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();*/
                            break;
                    }
                }
            });


//            myLocationData = getSharedPreferences("Registration", MODE_PRIVATE);
//            String id = myLocationData.getString("LastTravelId", null);
            Log.d(TAG, "" );
//            myLocationData = getSharedPreferonStartCommand: id*********ences("Registration", MODE_PRIVATE);
//            if(id!=null)
//            {
//                LastTravelId=myLocationData.getString("LastTravelId",null);
//                editor.putString("LastLat", myLocationData.getString("FromLat",null));
//                editor.putString("LastLang", myLocationData.getString("FromLang",null));
//                mLastLocation = new Location("");
//                editor.apply();
//                editor.commit();
//                mLastLocation.setLatitude(Float.parseFloat( myLocationData.getString("FromLat",null)));
//                mLastLocation.setLongitude(Float.parseFloat(myLocationData.getString("FromLang",null)));
//            } else {
//                Log.d(TAG, "onStartCommands: id=null ");
//                stopMyService();
//            }
        }
        catch (Exception e)
        {

        }

    }
    private void gpsstopprd() {
        try {
            APIService service = RetrofitClient.getApiService();
            Call<GpsData> call;
            call = service.gps(emaildi);
            call.enqueue(new Callback<GpsData>() {
                @Override
                public void onResponse(Call<GpsData> call, Response<GpsData> response) {
                    if (response.body().getResult().equals("Success")) {
                        String message = "Sorry! Not connected to gps";

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } else if (response.body().getResult().equals("NotSuccess")) {

                    } else {

                    }
                }

                @Override
                public void onFailure(Call<GpsData> call, Throwable t) {
                    t.printStackTrace();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onTaskRemoved(final Intent rootIntent) {
//        startActivity(getPopupIntent());

    }
    public Intent getPopupIntent() {
        Intent popup = new Intent(getApplicationContext(), TrackStopActivity.class);
        popup.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        return popup;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        isServiceRunning = false;
        unregisterReceiver(mConnectivityReceiver);
//        Log.d(TAG, "onDestroy: ");
    }
    void stopMyService() {
        stopForeground(true);
        stopSelf();
        isServiceRunning = false;
    }
    private boolean CheckDeviceIsMoved(Location current, Location last) {
        try {
            double dist = current.distanceTo(last) / 1000;
//            Log.v("distance Calculate ", Double.toString(dist));

            NumberFormat df = DecimalFormat.getInstance();
            df.setMaximumFractionDigits(3);
            df.setGroupingUsed(false);

            String distanc = df.format(dist).toString().replaceAll(",", ".");
            dist = Double.parseDouble(distanc);

            //  dist = Double.parseDouble(df.format(dist));

//            Log.v("dist Calculate Decimal ", Double.toString(dist));
            if (dist <= 0.900) { //(in km, you can use 0.1 for metres etc.)
                //If it's within 1km, we assume we're not moving
//                Log.v("Device is = ", "Not  Moved");
                return false;
            } else {
//                Log.v("Device is = ", "Moved");
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));


        Log.d(TAG, "onStartCommand: ");
        try
        {
            startServiceWithNotification();
           // startTimer();
            //countDownTimer.start();
            Log.d(TAG, "onStartCommand: ");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return START_NOT_STICKY;
    }

//    private void startTimer() {
//        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
//        editor = myshare.edit();
//        context = BGService.this;
//        attendanceCrashActivity = new AttendanceCrashActivity();
//        name = myshare.getString("name", "");
//        description = myshare.getString("description", "");
//        address = myshare.getString("address", "");
//        locate = myshare.getString("worklocation", "");
//        formatdate = myshare.getString("formateddate", "");
//        formatedtime = myshare.getString("formatedtime", "");
//        emaildi = myshare.getString("email", "");
//        phonenumber = myshare.getString("phonenumber", "");
//        //  empid=myshare.getString("sid","");
//        empid = myshare.getInt("sid", 0);
//        mLatitude = Double.parseDouble(myshare.getString("latitude", ""));
//        mLongtitude = Double.parseDouble(myshare.getString("longitude", ""));
//        trackId = myshare.getInt("trackid", 0);
//        // mConnectivityReceiver = new ConnectivityReceiver(this);
//        phoneType = myshare.getString("fingerType", "");
//        Log.d(TAG, "FormatedDate: " + formatdate);
//        Log.d(TAG, "formatedtime: " + formatedtime);
//        String[] hour = formatedtime.split(":");
//        hour1 = hour[0];
//        minutes = hour[1];
//        seconds = hour[2];
//        Log.d(TAG, "hourssplit: " + hour1);
//        Log.d(TAG, "minutessplit: " + minutes);
//        Log.d(TAG, "secondssplit: " + seconds);
//        hours = Integer.parseInt(hour1) * 3600;
//        minutes1 = Integer.parseInt(minutes) * 60;
//        seconds1 = Integer.parseInt(seconds);
//        currentmiiliseconds = hours + minutes1 + seconds1;
//        exampleseconds = (currentmiiliseconds * 1000) + 120000;
//        Totalmiliseconds=  86400000-currentmiiliseconds * 1000;
//       // Totalmiliseconds = exampleseconds - currentmiiliseconds * 1000;
//        Log.d(TAG, "hoursint: " + hours);
//        Log.d(TAG, "minutesint: " + minutes1);
//        Log.d(TAG, "secondsint: " + seconds1);
//        Log.d(TAG, "currentint: " + currentmiiliseconds);
//        Log.d(TAG, "totalint: " + Totalmiliseconds);
//        countDownTimer = new CountDownTimer(Totalmiliseconds, 1000) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//                hours1 = millisUntilFinished / 1000;
//                Log.d(TAG, "hours1: " + hours1);
//            }
//
//            @Override
//            public void onFinish() {
//                Intent broadCastIntent = new Intent();
//                broadCastIntent.setAction(AttendanceCrashActivity.BROADCAST_ACTION);
//                broadCastIntent.putExtra("data", "In Time");
//                sendBroadcast(broadCastIntent);
////                    Intent intent = new Intent("changethebutton");
////                    Bundle bundle = new Bundle();
////                    bundle.putString("SetStatus","In Time");
////                    intent.putExtras(bundle);
////                    LocalBroadcastManager.getInstance(BGServicenormal.this).sendBroadcast(intent);
//
//                //attendanceCrashActivity.showworld();
//                Log.d("Finish1", String.valueOf(+hours1));
//                servuce=true;
//                editor.putBoolean("service", servuce);
//                editor.commit();
//                Log.d("service", String.valueOf(servuce));
//
////                countDownTimer.onFinish();
//            }
//        };
//        Log.d(TAG, "onCreate: name " + name + " id " + id + " trackId " + trackId + " s_id " + s_Id);
//    }
//    @Override
//    public void onNetworkConnectionChanged(boolean isConnected) {
//        Log.d(TAG, "onNetworkConnectionChanged: isConnected"+isConnected);
//        nonetwork();
//        String message = isConnected ? "Good Network Connection" : nonetwork();
//        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
//  }

//    private String nonetwork() {
//////
//        SmsManager smsManager = SmsManager.getDefault();
//        smsManager.sendTextMessage(phonenumber +"9940020178",null, "Mr."+name+" Turned Off the Network.", null, null);
//        return "Your Travel cannot be tracked. So Please turn on your Network connection";
//
//    }
    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";
        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
//                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
//                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
//                    Log.e(TAG, errorMessage + ". " +
//                            "Latitude = " + latlang[0] + ", Longitude = " +
//                            latlang[1], illegalArgumentException);
                }

                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            } else {
                new GetGeoCodeAPIAsynchTask().execute(latlang[0], latlang[1]);
            }


            return null;
        }

        protected void onPostExecute(Address addresss) {
            double lat=0,lon=0;
            if (addresss == null) {
                new GetGeoCodeAPIAsynchTask().execute(lat, lon);
            } else {
                address = addresss.getAddressLine(0); //0 to obtain first possible address
                city = addresss.getLocality();
                Log.d(TAG, "onPostExecute: **************************"+city);
                state = addresss.getAdminArea();
                //create your custom title
//                String title = city + "-" + state;
//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertboxwithback("Your Current location is "+city);


            }
        }
    }

    private class GetGeoCodeAPIAsynchTask extends AsyncTask<Double, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String[] doInBackground(Double... latlang) {
            String response;
            try {
                String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang[0] + "," + latlang[1] + "&key=" + APIKEY;
//                Log.v("URL", URL);
                response = getLatLongByURL(URL);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                address = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(0).getString("long_name");

                 city = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(2).getString("long_name");

                 state = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(4).getString("long_name");

                Log.d(TAG, "onPostExecute: "+city);
//                String title = city + "-" + state;


//                if (fetchType == FROMADDRESS) {
//
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.requestFocus();
//                    fromaddress = address;
//                    mPickupText.setText(address);
//
//                    editor.putString("FromAddress", fromaddress);
//                    editor.apply();
//                    editor.commit();
//
//                    Log.i("FromAddress1", address);
//                    Log.i("FromAddress2", title);
//                } else {
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.setText(address);
//                    toaddress = address;
//                    editor.putString("ToAddress", toaddress).commit();
//
//                    Log.i("ToAddress1", address);
//                    Log.i("ToAddress2", title);
//
//                }


//                Log.d("Address", "" + address);
            } catch (JSONException e) {
                e.printStackTrace();
                if(TextUtils.isEmpty(address)||address.equals(null))
                {
                    address="Address Not found";
                }
            }
//            progressBar.setVisibility(View.GONE);

        }

        public String getLatLongByURL(String requestURL) {
            URL url;
            String response = "";
            try {
                url = new URL(requestURL);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                conn.setDoOutput(true);
                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }
                } else {
                    response = "";
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }


//    private boolean isSameProvider(String provider1, String provider2) {
//        if (provider1 == null) {
//            return provider2 == null;
//        }
//        return provider1.equals(provider2);
//    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

public class MyLocationListener implements android.location.LocationListener {

    public void onLocationChanged(final Location loc) {
        Log.i("********************", "Location changed");
//        mCurrentLocation.set(loc);
//        editor.putString("LastLat",String.valueOf(loc.getLatitude()));
//        editor.putString("LastLang",String.valueOf(loc.getLongitude()));
        Log.d(TAG, "onLocationChanged: lat " + loc.getLatitude() + " n lon " + loc.getLongitude());
        mLastLocation=new Location("");
        mLastLocation.setLatitude(mLatitude);
        mLastLocation.setLongitude(mLongtitude);

        if (CheckDeviceIsMoved(loc, mLastLocation))
       {
            mLatitude=loc.getLatitude();
            mLongtitude=loc.getLongitude();
            Log.d(TAG, "onLocationChanged: lat " + loc.getLatitude() + " n lon " + loc.getLongitude());
////            getDistance(Double.parseDouble(myLocationData.getString("LastLat", "")), Double.parseDouble(myLocationData.getString("LastLang", "")), loc.getLatitude(), loc.getLongitude());
    //To get Address
     new GeocodeAsyncTask().execute(loc.getLatitude(),loc.getLongitude());
     new AddItems().execute(loc.getLatitude(),loc.getLongitude());
        }
    }

    public void onProviderDisabled(String provider) {
        // Toast.makeText(getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT).show();
    }

    public void onProviderEnabled(String provider) {
        // Toast.makeText(getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

}

public class AddItems extends AsyncTask<Double,Void,String>
{
    private String datetime,updateddate,timedate,latitude,longitude,distance;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d(TAG, "onPreExecute: Additems");
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        datetime = df.format(c.getTime());
        SimpleDateFormat mdformat = new SimpleDateFormat("h:mm a");
        timedate = mdformat.format(c.getTime());
        distance="0";
        updateddate="";

    }
    @Override
    protected String doInBackground(Double... doubles) {
        ServerConnection serverConnection = new ServerConnection();
        try
        {
            Log.d(TAG, "doInBackground: ");
            Connection connection = serverConnection.getConnection();
            Statement statement = connection.createStatement();
            latitude= String.valueOf(doubles[0]);
            longitude= String.valueOf(doubles[1]);
            String query="insert into coordinate(TrackId,S_id,saname,latitude,langtitude,address,datetime,timedate,updateddate,distance) " +
                    "values ('"+trackId+"' , '"+empid+"' ,'"+name+"', '"+latitude+"' ,'"+longitude+"' , '"+address+"','"+datetime+"','"+timedate+"','"+updateddate+"','"+distance+"' )";
            Log.d(TAG, "doInBackground: "+doubles[0]+ "long "+doubles[1]);
            int i= statement.executeUpdate(query);
            if(i!=0)
            {
                statement.close();
                connection.close();
                return "success";
            } else {
                statement.close();
                connection.close();
                return "notsucess";
            }

        }
        catch (Exception e)
        {
            Log.d(TAG, "doInBackground: exception"+e);
            address="Address not found";
            return "Exception";
        }

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s.equals("success"))
        {
            Log.d(TAG, "onPostExecute: success");
        }
        else {
            Log.d(TAG, "onPostExecute: not success");
        }
    }
}

}
