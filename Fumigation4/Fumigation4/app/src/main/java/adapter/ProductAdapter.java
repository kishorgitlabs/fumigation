package adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.brainmagic.fumigation.ContainerDetailsActivity;
import com.brainmagic.fumigation.R;
import java.util.List;
import api.models.jobcard.JobCard;
import api.models.materialrequest.request.Materialrequest;

/**
 * Created by SYSTEM10 on 3/7/2019.
 */

public class ProductAdapter extends ArrayAdapter {
    //intilize the deails...
    private Context context;
    private List<Materialrequest> data;


    public ProductAdapter(@NonNull Context context, List<Materialrequest> data) {
        super(context, R.layout.productname, data);
        this.context = context;
        this.data = data;
    }

    @SuppressLint("WrongConstant")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Productaction holder;

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.productname, parent, false);

            holder = new Productaction();
            //find the variable name
            holder.productname = (TextView) convertView.findViewById(R.id.productname1);
            holder.productcode = (TextView) convertView.findViewById(R.id.productunit);
            holder.quantity = (TextView) convertView.findViewById(R.id.quantity);
            holder.sno = (TextView) convertView.findViewById(R.id.sno);
            //set the variable name...
            holder.productname.setText(data.get(position).getProductName());
            holder.productcode.setText(data.get(position).getProductUnit());
            holder.quantity.setText(data.get(position).getReqQty());
            holder.sno.setText(Integer.toString(position + 1) + ".");
            convertView.setTag(holder);
        } else {
            holder = (Productaction) convertView.getTag();
        }

        return convertView;
    }

    private class Productaction {
        public TextView sno;
        public TextView productname;
        public TextView productcode;
        public TextView quantity;

    }
}
