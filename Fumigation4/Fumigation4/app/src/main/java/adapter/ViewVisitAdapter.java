package adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.fumigation.ContainerDetailsActivity;
import com.brainmagic.fumigation.R;
import com.brainmagic.fumigation.ViewFilesActivity;

import java.util.List;

import api.models.jobcard.JobCard;
import api.models.viewReport.ViewData;

/**
 * Created by SYSTEM10 on 12/24/2018.
 */

public class ViewVisitAdapter extends ArrayAdapter {
    //intial the details...
    private Context context;
    private List<ViewData> data;



    public ViewVisitAdapter(@NonNull Context context, List<ViewData> data) {
        super(context, R.layout.viewreportdetails,data);
        this.context = context;
        this.data = data ;
    }
    @SuppressLint("WrongConstant")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewDetails holder;

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.viewreportdetails, parent, false);

            holder=new ViewDetails();
            //find the variable name....
            holder.customername = (TextView) convertView.findViewById(R.id.customername);
            holder.mobilenumber = (TextView) convertView.findViewById(R.id.mobilenumber);
            holder.address1=(TextView)convertView.findViewById(R.id.address1);

            //set the variable name...
            holder.customername.setText(data.get(position).getName());
            holder.mobilenumber.setText(data.get(position).getMobileNo());
            holder.address1.setText(data.get(position).getPlaceOfVisit());
            holder.customername.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //activity...
                    Intent i =new Intent(context,ViewFilesActivity.class).putExtra("id",data.get(position).getId());
                    context.startActivity(i);
                }
            });





            convertView.setTag(holder);
        } else {
            holder = (ViewDetails) convertView.getTag();
        }

        return convertView;
    }

    private class ViewDetails {

        public TextView customername;
        public TextView mobilenumber;
        public TextView address1;
        public TextView dateform;
        public TextView timeform;




    }
}
