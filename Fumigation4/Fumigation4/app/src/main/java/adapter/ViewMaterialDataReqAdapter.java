package adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.fumigation.R;

import java.util.List;

import api.models.viewmaterialreq.ViewMaterialReq;

/**
 * Created by SYSTEM10 on 2/8/2019.
 */

public class ViewMaterialDataReqAdapter extends ArrayAdapter {
    //intilize tthe class...
    private Context context;
    private List<ViewMaterialReq> data;


    public ViewMaterialDataReqAdapter(@NonNull Context context, List<ViewMaterialReq> data) {
        super(context, R.layout.jobcard_deatils, data);
        this.context = context;
        this.data = data;
    }

    @SuppressLint("WrongConstant")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
      ViewMaterialReq1 holder;

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.viewmaterialreq, parent, false);

            holder = new ViewMaterialReq1();
           //find the variable name...
            holder.productname = (TextView) convertView.findViewById(R.id.productname);
            holder.approved = (TextView) convertView.findViewById(R.id.approved);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);


            //set the variable name...
            holder.productname.setText(data.get(position).getProductName());
            holder.approved.setText(data.get(position).getApprovedQty());
            holder.qty.setText(String.valueOf(data.get(position).getReqQty()));




            convertView.setTag(holder);
        } else {
            holder = (ViewMaterialReq1) convertView.getTag();
        }

        return convertView;
    }

    private class ViewMaterialReq1 {

        public TextView productname;
        public TextView approved;
        public TextView qty;
    }


    }
