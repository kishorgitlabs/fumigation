package adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.fumigation.JobCardFeedbackActivity;
import com.brainmagic.fumigation.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import jobcard.JobcardFeedbackFragment;
import persistance.ServerConnection;
import api.models.jobcardcontainer.Container;

/**
 * Created by SYSTEM10 on 6/12/2018.
 */

public class ContainerDetailsAdapter  extends ArrayAdapter{
    private Context context;
    //assign the container in list...
    private List<Container> data;
    private Containerdetails holder;
    private AlertDialog alertDialog;
    View convertView;
    public ContainerDetailsAdapter(@NonNull Context context, List<Container> data) {
        super(context, R.layout.container_details,data);
        this.context = context;
        this.data = data;


    }

    @SuppressLint("WrongConstant")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.container_details, parent, false);

            holder = new Containerdetails();
            this.convertView=convertView;
            //find the data in adapter....
            holder.sno = (TextView) convertView.findViewById(R.id.sno);
            holder.lorrydrivename=(TextView)convertView.findViewById(R.id.lorrydrivename);
            holder.lorrydrivenumber=(TextView)convertView.findViewById(R.id.lorrydrivenumber);
            holder.containerno = (TextView) convertView.findViewById(R.id.containerno);
            holder.Feedback = (ImageView) convertView.findViewById(R.id.feedback);
            holder.update=(ImageView)convertView.findViewById(R.id.update);
            holder.status=(TextView) convertView.findViewById(R.id.status);

            //set the data in adapter........
            holder.sno.setText(Integer.toString(position + 1) + ".");
            holder.containerno.setText(data.get(position).getContainerNo());
            holder.lorrydrivename.setText(data.get(position).getLorryDriverName());
            holder.lorrydrivenumber.setText(data.get(position).getLorryDriverMobNo());
            holder.status.setText(data.get(position).getStatus());


            holder.update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //show the alert box.......
                    ShowAlertbox(data,position);
                    notifyDataSetChanged();
                }
            });
            holder.Feedback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //going to the nextactivity.......
              context.startActivity(new Intent(context, JobCardFeedbackActivity.class).putExtra("containernumber",data.get(position).getContainerNo()).putExtra("jobcardod",data.get(position).getJobcardid()));
                    //context.startActivity(new Intent(context, JobcardFeedbackFragment.class).putExtra("containernumber",data.get(position).getContainerNo()).putExtra("jobcardod",data.get(position).getJobcardid()));


                }
            });


            convertView.setTag(holder);
        } else {
            holder = (Containerdetails) convertView.getTag();
        }

        return convertView;
    }

    private void ShowAlertbox(List<Container> data,int position) {
        alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.update_order, null);
        alertDialog.setView(dialogView);

        final EditText lname = (EditText) dialogView.findViewById(R.id.lorrydrivernameupdate);
        final EditText ldrivername = (EditText) dialogView.findViewById(R.id.lorrydrivermobupdate);

        Button Save = (Button)  dialogView.findViewById(R.id.save);
        Button Cancel = (Button)  dialogView.findViewById(R.id.cancel);
        lname.setText(data.get(position).getLorryDriverName());
        ldrivername.setText(data.get(position).getLorryDriverMobNo());
        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                String driverName=lname.getText().toString();
                String driverNumber=ldrivername.getText().toString();
                //update the data.........
                new Update(position,data).execute(data.get(position).getId(),driverName,driverNumber);
                notifyDataSetChanged();
                 /* Connection connection;
                  Statement statement;
                  ResultSet resultSet;
                 ServerConnection serverConnection = new ServerConnection();
                try {
                    connection =serverConnection.getConnection();
                    statement =connection.createStatement();
                    String query = "update ContainerTable set LorryDriverName = '"+lorrydrivename.getText().toString()+"', LorryDriverMobNo = '"+lorrydrivenumber.getText().toString()+"' where id = '"+id+"'";
                    Log.v("query", query);
                    resultSet = statement.executeQuery(query);
                    int i = statement.executeUpdate(query);
                    if (i == 0) {
                        connection.close();
                        statement.close();

                    } else {
                        connection.close();
                        statement.close();

                    }

                    while (resultSet.next()) {
                        lorrydrivename.setText(lorrydrivename.getText().toString());
                        lorrydrivenumber.setText(lorrydrivenumber.getText().toString());
                    }

                    notifyDataSetChanged();
                    alertDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
*/

            }
        });
        alertDialog.show();
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    private class Containerdetails {

        public TextView sno;
        public TextView containerno;
        public TextView lorrydrivename;
        public TextView lorrydrivenumber;
        public TextView status;
        private ImageView Feedback;
        private ImageView update;


    }

    //update the methods..
    private class Update extends AsyncTask<String,Void,String> {
         String lorrydrivename ;
         String lorrydrivenumber;
         Connection connection;
         Statement statement;
         String id;
         ResultSet resultSet;
         int position;
//        Containerdetails holders=null;
        View convertView;
        List<Container> data;

        public Update(int convertView,List<Container> data) {
            this.position = convertView;
            this.data=data;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();


        }
        @Override
        protected String doInBackground(String... params) {
          try {
              id=params[0];
              lorrydrivename=params[1];
              lorrydrivenumber=params[2];
              ServerConnection serverConnection = new ServerConnection();
              connection =serverConnection.getConnection();
              statement =connection.createStatement();
              //update query..
              String query = "update ContainerTable set LorryDriverName = '"+lorrydrivename+"', LorryDriverMobNo = '"+lorrydrivenumber+"' where id = '"+id+"'";
              Log.v("update query", query);
              int i = statement.executeUpdate(query);

              if (i == 0) {
                  connection.close();
                  statement.close();
                  Toast.makeText(getContext(),"Error Updating ",Toast.LENGTH_SHORT).show();
                  return "notinseted";
              } else {
                  connection.close();
                  statement.close();
                  return "inserted";
              }

          } catch (Exception e){
              e.printStackTrace();
              return "notsuccess";
          }


        }
        @Override
        protected void onPostExecute(String result
//                                     ,Containerdetails holder , AlertDialog alertDialog
        ) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (result.equals("inserted")) {
                alertDialog.dismiss();
//                Intent i= new Intent(context,ContainerDetailsActivity.class);
//                context.startActivity(i);
                //set the lorrydrivername and lorrydrivernumber.....
                data.get(position).setLorryDriverName(lorrydrivename);
                data.get(position).setLorryDriverMobNo(lorrydrivenumber);
                    notifyDataSetChanged();

            }
            else
            {
                Toast.makeText(getContext(),"Error Updating ",Toast.LENGTH_SHORT).show();
            }

        }

    }


}
