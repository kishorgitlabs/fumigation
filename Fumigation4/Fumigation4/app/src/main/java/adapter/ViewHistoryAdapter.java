package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brainmagic.fumigation.MapsActivity;
import com.brainmagic.fumigation.R;
import com.brainmagic.fumigation.ViewCoordinate;

import java.util.List;

/**
 * Created by SYSTEM10 on 10/23/2018.
 */

public class ViewHistoryAdapter extends ArrayAdapter {
    //intilize the variable name
    private Context context;
    private List<String> namelist,datelist, intimelist,outtime, fromlocation, tolocation,fromlat,fromlong,tolat,tolong;



    public ViewHistoryAdapter(@NonNull Context context, List<String> namelist, List<String>datelist, List<String>intimelist, List<String>outtime, List<String>fromlocation,List<String>tolocation, List<String>fromlat,List<String>fromlong,List<String>tolat,List<String>tolong) {
        super(context, R.layout.viewhistory,namelist);
        this.context = context;
        this.namelist = namelist ;
        this.datelist = datelist ;
        this.intimelist = intimelist ;
        this.outtime = outtime ;
        this.fromlocation = fromlocation ;
        this.tolocation = tolocation ;
        this.fromlat = fromlat ;
        this.fromlong = fromlong ;
        this.tolat = tolat ;
        this.tolong = tolong ;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.viewhistory, parent, false);
            try {

                //find the vriable name...
                //  TextView sno=(TextView)convertView.findViewById(R.id.sno1);
             //   TextView name = (TextView) convertView.findViewById(R.id.namehis);
                TextView date = (TextView) convertView.findViewById(R.id.datehis);
                TextView intime = (TextView) convertView.findViewById(R.id.inhis);
                TextView outtime1 = (TextView) convertView.findViewById(R.id.outhis);
                TextView fromlocation1 = (TextView) convertView.findViewById(R.id.fromloc);
                TextView outlocation = (TextView) convertView.findViewById(R.id.tohis);
                Button map = (Button) convertView.findViewById(R.id.map);
                LinearLayout linearyout = (LinearLayout) convertView.findViewById(R.id.linearyout);
             //set the variable name...
          /*   name.setText(namelist.get(position));*/
                         if(datelist.get(position) != null){
                  String[] date1 = datelist.get(position).split(" ");
                  date.setText(date1[0]);
             }

                if (intimelist.get(position) != null) {
                    String[] intime1 = intimelist.get(position).split("\\.");
                    intime.setText(intime1[0]);

                } else {
                    intime.setText("");
                }
                if (outtime.get(position)!= null) {
                    String[] outtime2 = outtime.get(position).split("\\.");
                    outtime1.setText(outtime2[0]);

                } else {
                    outtime1.setText("");
                }


                if(fromlocation.get(position) != null)
                fromlocation1.setText(fromlocation.get(position));
                else
                {
                    fromlocation1.setText("No Address Found");
                }
                if(tolocation.get(position)!=null)
                outlocation.setText(tolocation.get(position));
                 else
                {
                    outlocation.setText("No Address Found");
                }
                map.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //activity...
                        Intent i=new Intent(context, MapsActivity.class).putExtra("fromadd",fromlocation.get(position)).putExtra("toadd",tolocation.get(position))
                                .putExtra("fromlatitude",fromlat.get(position)).putExtra("fromlongitude",fromlong.get(position))
                                .putExtra("tolatitude",tolat.get(position)).putExtra("tolongitude",tolong.get(position));
                        context.startActivity(i);
                    }
                });

                linearyout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //activity..
                        Intent i=new Intent(context, ViewCoordinate.class);
                        context.startActivity(i);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }



        }


        return convertView;
    }
    private class Jobcarddetailsfumi {

        public TextView sno;
        public TextView name;
        public TextView date;
        public TextView intime;
        public TextView outtime;


    }

    @Override
    public int getCount() {
        return datelist.size();
    }
}
