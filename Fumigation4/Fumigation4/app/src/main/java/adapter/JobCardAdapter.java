package adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.fumigation.ContainerDetailsActivity;
import com.brainmagic.fumigation.R;

import java.util.List;

import api.models.jobcard.JobCard;

/**
 * Created by SYSTEM10 on 6/11/2018.
 */

public class JobCardAdapter extends ArrayAdapter {
    private Context context;
    //assign the jobcard model.......
    private List<JobCard> data;



    public JobCardAdapter(@NonNull Context context, List<JobCard> data) {
        super(context, R.layout.jobcard_deatils,data);
        this.context = context;
        this.data = data ;
    }
    @SuppressLint("WrongConstant")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Jobcarddetailsfumi holder;

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.jobcard_deatils, parent, false);
            holder=new Jobcarddetailsfumi();

            //find the data in adapter..........
            holder.sno = (TextView) convertView.findViewById(R.id.sno);
            holder.jobcardno = (TextView) convertView.findViewById(R.id.jobcardno);
            holder.noofcontainer=(TextView)convertView.findViewById(R.id.noofcontroler);
            holder.date=(TextView)convertView.findViewById(R.id.date);
            holder.sno.setText(Integer.toString(position + 1) + ".");
            holder.jobcardno.setText(data.get(position).getJobcardNo());
            holder.noofcontainer.setText(data.get(position).getNOofContainer());
            holder.date.setText(data.get(position).getNDate());
            holder.jobcardno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //click jobcardno going to next activity.....
                    context.startActivity(new Intent(context, ContainerDetailsActivity.class).putExtra("jobcardno",data.get(position).getJobcardNo()).putExtra("id",data.get(position).getId()));
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (Jobcarddetailsfumi) convertView.getTag();
        }
        return convertView;
    }

    private class Jobcarddetailsfumi {

        public TextView sno;
        public TextView jobcardno;
        public TextView noofcontainer;
        public TextView date;




    }
}
