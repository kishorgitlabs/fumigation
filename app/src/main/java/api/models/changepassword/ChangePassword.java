
package api.models.changepassword;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ChangePassword {

    @SerializedName("data")
    private ChangePAsswordData mData;
    @SerializedName("result")
    private String mResult;

    public ChangePAsswordData getData() {
        return mData;
    }

    public void setData(ChangePAsswordData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
