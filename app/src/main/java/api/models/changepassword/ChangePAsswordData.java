
package api.models.changepassword;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ChangePAsswordData {

    @SerializedName("AadharNo")
    private Object mAadharNo;
    @SerializedName("Address")
    private String mAddress;
    @SerializedName("City")
    private Object mCity;
    @SerializedName("ClientCode")
    private Object mClientCode;
    @SerializedName("Count")
    private Object mCount;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("ERPid")
    private Object mERPid;
    @SerializedName("EmailId")
    private String mEmailId;
    @SerializedName("FieldOperatorName")
    private Object mFieldOperatorName;
    @SerializedName("IMINumber1")
    private String mIMINumber1;
    @SerializedName("IMINumber2")
    private String mIMINumber2;
    @SerializedName("id")
    private Long mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("MobileNo")
    private Object mMobileNo;
    @SerializedName("MobileNumber1")
    private String mMobileNumber1;
    @SerializedName("MobileNumber2")
    private String mMobileNumber2;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("SecurityKey")
    private Object mSecurityKey;
    @SerializedName("State")
    private Object mState;
    @SerializedName("StuffingPoint")
    private Object mStuffingPoint;
    @SerializedName("SupervisorName")
    private Object mSupervisorName;
    @SerializedName("UpdateDate")
    private String mUpdateDate;
    @SerializedName("UserName")
    private String mUserName;
    @SerializedName("UserType")
    private String mUserType;
    @SerializedName("Verified")
    private String mVerified;
    @SerializedName("Zip")
    private Object mZip;

    public Object getAadharNo() {
        return mAadharNo;
    }

    public void setAadharNo(Object aadharNo) {
        mAadharNo = aadharNo;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public Object getCity() {
        return mCity;
    }

    public void setCity(Object city) {
        mCity = city;
    }

    public Object getClientCode() {
        return mClientCode;
    }

    public void setClientCode(Object clientCode) {
        mClientCode = clientCode;
    }

    public Object getCount() {
        return mCount;
    }

    public void setCount(Object count) {
        mCount = count;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(String customerCode) {
        mCustomerCode = customerCode;
    }

    public Object getERPid() {
        return mERPid;
    }

    public void setERPid(Object eRPid) {
        mERPid = eRPid;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String emailId) {
        mEmailId = emailId;
    }

    public Object getFieldOperatorName() {
        return mFieldOperatorName;
    }

    public void setFieldOperatorName(Object fieldOperatorName) {
        mFieldOperatorName = fieldOperatorName;
    }

    public String getIMINumber1() {
        return mIMINumber1;
    }

    public void setIMINumber1(String iMINumber1) {
        mIMINumber1 = iMINumber1;
    }

    public String getIMINumber2() {
        return mIMINumber2;
    }

    public void setIMINumber2(String iMINumber2) {
        mIMINumber2 = iMINumber2;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public Object getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(Object mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getMobileNumber1() {
        return mMobileNumber1;
    }

    public void setMobileNumber1(String mobileNumber1) {
        mMobileNumber1 = mobileNumber1;
    }

    public String getMobileNumber2() {
        return mMobileNumber2;
    }

    public void setMobileNumber2(String mobileNumber2) {
        mMobileNumber2 = mobileNumber2;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public Object getSecurityKey() {
        return mSecurityKey;
    }

    public void setSecurityKey(Object securityKey) {
        mSecurityKey = securityKey;
    }

    public Object getState() {
        return mState;
    }

    public void setState(Object state) {
        mState = state;
    }

    public Object getStuffingPoint() {
        return mStuffingPoint;
    }

    public void setStuffingPoint(Object stuffingPoint) {
        mStuffingPoint = stuffingPoint;
    }

    public Object getSupervisorName() {
        return mSupervisorName;
    }

    public void setSupervisorName(Object supervisorName) {
        mSupervisorName = supervisorName;
    }

    public String getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        mUpdateDate = updateDate;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

    public String getVerified() {
        return mVerified;
    }

    public void setVerified(String verified) {
        mVerified = verified;
    }

    public Object getZip() {
        return mZip;
    }

    public void setZip(Object zip) {
        mZip = zip;
    }

}
