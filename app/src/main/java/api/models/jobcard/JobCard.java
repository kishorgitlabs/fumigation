package api.models.jobcard;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class JobCard {
  @SerializedName("NameOftheCFS")
  @Expose
  private String NameOftheCFS;
  @SerializedName("Cargo")
  @Expose
  private String Cargo;
  @SerializedName("Updatedate")
  @Expose
  private String Updatedate;
  @SerializedName("DosageofFumigant")
  @Expose
  private String DosageofFumigant;
  @SerializedName("JobcardNo")
  @Expose
  private String JobcardNo;
  @SerializedName("Port")
  @Expose
  private String Port;
  @SerializedName("ContainerType")
  @Expose
  private String ContainerType;
  @SerializedName("Insertdate")
  @Expose
  private String Insertdate;
  @SerializedName("SupervisorName")
  @Expose
  private String SupervisorName;
  @SerializedName("Time")
  @Expose
  private String Time;
  @SerializedName("Method")
  @Expose
  private String Method;
  @SerializedName("StuffingPoint")
  @Expose
  private String StuffingPoint;
  @SerializedName("FieldOperatorName")
  @Expose
  private String FieldOperatorName;
  @SerializedName("VesselName")
  @Expose
  private String VesselName;
  @SerializedName("Fseal")
  @Expose
  private String Fseal;
  @SerializedName("NDate")
  @Expose
  private String NDate;
  @SerializedName("NOofContainer")
  @Expose
  private String NOofContainer;
  @SerializedName("CountryandPort")
  @Expose
  private String CountryandPort;
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("NameofFumigant")
  @Expose
  private String NameofFumigant;
  @SerializedName("Cusid")
  @Expose
  private String Cusid;
  @SerializedName("BranchCode")
  @Expose
  private String BranchCode;
  @SerializedName("Createdate")
  @Expose
  private String Createdate;
  public void setNameOftheCFS(String NameOftheCFS){
   this.NameOftheCFS=NameOftheCFS;
  }
  public String getNameOftheCFS(){
   return NameOftheCFS;
  }
  public void setCargo(String Cargo){
   this.Cargo=Cargo;
  }
  public String getCargo(){
   return Cargo;
  }
  public void setUpdatedate(String Updatedate){
   this.Updatedate=Updatedate;
  }
  public String getUpdatedate(){
   return Updatedate;
  }
  public void setDosageofFumigant(String DosageofFumigant){
   this.DosageofFumigant=DosageofFumigant;
  }
  public String getDosageofFumigant(){
   return DosageofFumigant;
  }
  public void setJobcardNo(String JobcardNo){
   this.JobcardNo=JobcardNo;
  }
  public String getJobcardNo(){
   return JobcardNo;
  }
  public void setPort(String Port){
   this.Port=Port;
  }
  public String getPort(){
   return Port;
  }
  public void setContainerType(String ContainerType){
   this.ContainerType=ContainerType;
  }
  public String getContainerType(){
   return ContainerType;
  }
  public void setInsertdate(String Insertdate){
   this.Insertdate=Insertdate;
  }
  public String getInsertdate(){
   return Insertdate;
  }
  public void setSupervisorName(String SupervisorName){
   this.SupervisorName=SupervisorName;
  }
  public String getSupervisorName(){
   return SupervisorName;
  }
  public void setTime(String Time){
   this.Time=Time;
  }
  public String getTime(){
   return Time;
  }
  public void setMethod(String Method){
   this.Method=Method;
  }
  public String getMethod(){
   return Method;
  }
  public void setStuffingPoint(String StuffingPoint){
   this.StuffingPoint=StuffingPoint;
  }
  public String getStuffingPoint(){
   return StuffingPoint;
  }
  public void setFieldOperatorName(String FieldOperatorName){
   this.FieldOperatorName=FieldOperatorName;
  }
  public String getFieldOperatorName(){
   return FieldOperatorName;
  }
  public void setVesselName(String VesselName){
   this.VesselName=VesselName;
  }
  public String getVesselName(){
   return VesselName;
  }
  public void setFseal(String Fseal){
   this.Fseal=Fseal;
  }
  public String getFseal(){
   return Fseal;
  }
  public void setNDate(String NDate){
   this.NDate=NDate;
  }
  public String getNDate(){
   return NDate;
  }
  public void setNOofContainer(String NOofContainer){
   this.NOofContainer=NOofContainer;
  }
  public String getNOofContainer(){
   return NOofContainer;
  }
  public void setCountryandPort(String CountryandPort){
   this.CountryandPort=CountryandPort;
  }
  public String getCountryandPort(){
   return CountryandPort;
  }
  public void setId(String id){
   this.id=id;
  }
  public String getId(){
   return id;
  }
  public void setNameofFumigant(String NameofFumigant){
   this.NameofFumigant=NameofFumigant;
  }
  public String getNameofFumigant(){
   return NameofFumigant;
  }
  public void setCusid(String Cusid){
   this.Cusid=Cusid;
  }
  public String getCusid(){
   return Cusid;
  }
  public void setBranchCode(String BranchCode){
   this.BranchCode=BranchCode;
  }
  public String getBranchCode(){
   return BranchCode;
  }
  public void setCreatedate(String Createdate){
   this.Createdate=Createdate;
  }
  public String getCreatedate(){
   return Createdate;
  }
}