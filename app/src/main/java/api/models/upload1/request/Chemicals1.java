
package api.models.upload1.request;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Chemicals1 {

    @SerializedName("chemicals")
    private String mChemicals;
    @SerializedName("Quatity")
    private String mQuatity;

    public String getChemicals() {
        return mChemicals;
    }

    public void setChemicals(String chemicals) {
        mChemicals = chemicals;
    }

    public String getQuatity() {
        return mQuatity;
    }

    public void setQuatity(String quatity) {
        mQuatity = quatity;
    }

}
