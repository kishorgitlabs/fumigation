
package api.models.upload1.request;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Chemicalreq {

    @SerializedName("Audio")
    private String mAudio;
    @SerializedName("chemicals1")
    private List<Chemicals1> mChemicals1;
    @SerializedName("ContainerNo")
    private String mContainerNo;
    @SerializedName("id")
    private String mId;
    @SerializedName("Image1")
    private String mImage1;
    @SerializedName("Image2")
    private String mImage2;
    @SerializedName("Remarks")
    private String mRemarks;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("workreport")
    private String mWorkreport;

    public String getAudio() {
        return mAudio;
    }

    public void setAudio(String audio) {
        mAudio = audio;
    }

    public List<Chemicals1> getChemicals1() {
        return mChemicals1;
    }

    public void setChemicals1(List<Chemicals1> chemicals1) {
        mChemicals1 = chemicals1;
    }

    public String getContainerNo() {
        return mContainerNo;
    }

    public void setContainerNo(String containerNo) {
        mContainerNo = containerNo;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getImage1() {
        return mImage1;
    }

    public void setImage1(String image1) {
        mImage1 = image1;
    }

    public String getImage2() {
        return mImage2;
    }

    public void setImage2(String image2) {
        mImage2 = image2;
    }

    public String getRemarks() {
        return mRemarks;
    }

    public void setRemarks(String remarks) {
        mRemarks = remarks;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getWorkreport() {
        return mWorkreport;
    }

    public void setWorkreport(String workreport) {
        mWorkreport = workreport;
    }

}
