
package api.models.upload1.response;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Chemicals1 {

    @SerializedName("chemicals")
    private String mChemicals;
    @SerializedName("container_number")
    private Object mContainerNumber;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Quatity")
    private String mQuatity;

    public String getChemicals() {
        return mChemicals;
    }

    public void setChemicals(String chemicals) {
        mChemicals = chemicals;
    }

    public Object getContainerNumber() {
        return mContainerNumber;
    }

    public void setContainerNumber(Object containerNumber) {
        mContainerNumber = containerNumber;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getQuatity() {
        return mQuatity;
    }

    public void setQuatity(String quatity) {
        mQuatity = quatity;
    }

}
