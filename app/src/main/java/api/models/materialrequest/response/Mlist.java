package api.models.materialrequest.response;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Mlist{
  @SerializedName("Description")
  @Expose
  private String Description;
  @SerializedName("data")
  @Expose
  private Object data;
  @SerializedName("ProductName")
  @Expose
  private String ProductName;
  @SerializedName("RequestTo")
  @Expose
  private Object RequestTo;
  @SerializedName("data1")
  @Expose
  private Object data1;
  @SerializedName("ReqQty")
  @Expose
  private Integer ReqQty;
  @SerializedName("ApprovedQty")
  @Expose
  private Object ApprovedQty;
  @SerializedName("Rid")
  @Expose
  private Integer Rid;
  @SerializedName("Remark")
  @Expose
  private Object Remark;
  @SerializedName("RequestedNo")
  @Expose
  private Integer RequestedNo;
  @SerializedName("ProductUnit")
  @Expose
  private String ProductUnit;
  @SerializedName("CreatedDate")
  @Expose
  private String CreatedDate;
  @SerializedName("id")
  @Expose
  private Integer id;
  public void setDescription(String Description){
   this.Description=Description;
  }
  public String getDescription(){
   return Description;
  }
  public void setData(Object data){
   this.data=data;
  }
  public Object getData(){
   return data;
  }
  public void setProductName(String ProductName){
   this.ProductName=ProductName;
  }
  public String getProductName(){
   return ProductName;
  }
  public void setRequestTo(Object RequestTo){
   this.RequestTo=RequestTo;
  }
  public Object getRequestTo(){
   return RequestTo;
  }
  public void setData1(Object data1){
   this.data1=data1;
  }
  public Object getData1(){
   return data1;
  }
  public void setReqQty(Integer ReqQty){
   this.ReqQty=ReqQty;
  }
  public Integer getReqQty(){
   return ReqQty;
  }
  public void setApprovedQty(Object ApprovedQty){
   this.ApprovedQty=ApprovedQty;
  }
  public Object getApprovedQty(){
   return ApprovedQty;
  }
  public void setRid(Integer Rid){
   this.Rid=Rid;
  }
  public Integer getRid(){
   return Rid;
  }
  public void setRemark(Object Remark){
   this.Remark=Remark;
  }
  public Object getRemark(){
   return Remark;
  }
  public void setRequestedNo(Integer RequestedNo){
   this.RequestedNo=RequestedNo;
  }
  public Integer getRequestedNo(){
   return RequestedNo;
  }
  public void setProductUnit(String ProductUnit){
   this.ProductUnit=ProductUnit;
  }
  public String getProductUnit(){
   return ProductUnit;
  }
  public void setCreatedDate(String CreatedDate){
   this.CreatedDate=CreatedDate;
  }
  public String getCreatedDate(){
   return CreatedDate;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
}