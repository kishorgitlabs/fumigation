package api.models.materialrequest.response;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Pc{
  @SerializedName("Status")
  @Expose
  private String Status;
  @SerializedName("Description")
  @Expose
  private Object Description;
  @SerializedName("data")
  @Expose
  private Object data;
  @SerializedName("ProductName")
  @Expose
  private Object ProductName;
  @SerializedName("RequestTo")
  @Expose
  private String RequestTo;
  @SerializedName("ApprovedDate")
  @Expose
  private Object ApprovedDate;
  @SerializedName("ApprovedPerson")
  @Expose
  private Object ApprovedPerson;
  @SerializedName("ReqQty")
  @Expose
  private Object ReqQty;
  @SerializedName("BranchId")
  @Expose
  private Integer BranchId;
  @SerializedName("CreateDate")
  @Expose
  private String CreateDate;
  @SerializedName("Flag")
  @Expose
  private Object Flag;
  @SerializedName("Remark")
  @Expose
  private Object Remark;
  @SerializedName("RejectReason")
  @Expose
  private Object RejectReason;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("ReqBranchName")
  @Expose
  private String ReqBranchName;
  @SerializedName("ReqPersonName")
  @Expose
  private String ReqPersonName;
  @SerializedName("RequestNo")
  @Expose
  private Integer RequestNo;
  @SerializedName("ReqBranchCode")
  @Expose
  private String ReqBranchCode;
  public void setStatus(String Status){
   this.Status=Status;
  }
  public String getStatus(){
   return Status;
  }
  public void setDescription(Object Description){
   this.Description=Description;
  }
  public Object getDescription(){
   return Description;
  }
  public void setData(Object data){
   this.data=data;
  }
  public Object getData(){
   return data;
  }
  public void setProductName(Object ProductName){
   this.ProductName=ProductName;
  }
  public Object getProductName(){
   return ProductName;
  }
  public void setRequestTo(String RequestTo){
   this.RequestTo=RequestTo;
  }
  public String getRequestTo(){
   return RequestTo;
  }
  public void setApprovedDate(Object ApprovedDate){
   this.ApprovedDate=ApprovedDate;
  }
  public Object getApprovedDate(){
   return ApprovedDate;
  }
  public void setApprovedPerson(Object ApprovedPerson){
   this.ApprovedPerson=ApprovedPerson;
  }
  public Object getApprovedPerson(){
   return ApprovedPerson;
  }
  public void setReqQty(Object ReqQty){
   this.ReqQty=ReqQty;
  }
  public Object getReqQty(){
   return ReqQty;
  }
  public void setBranchId(Integer BranchId){
   this.BranchId=BranchId;
  }
  public Integer getBranchId(){
   return BranchId;
  }
  public void setCreateDate(String CreateDate){
   this.CreateDate=CreateDate;
  }
  public String getCreateDate(){
   return CreateDate;
  }
  public void setFlag(Object Flag){
   this.Flag=Flag;
  }
  public Object getFlag(){
   return Flag;
  }
  public void setRemark(Object Remark){
   this.Remark=Remark;
  }
  public Object getRemark(){
   return Remark;
  }
  public void setRejectReason(Object RejectReason){
   this.RejectReason=RejectReason;
  }
  public Object getRejectReason(){
   return RejectReason;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setReqBranchName(String ReqBranchName){
   this.ReqBranchName=ReqBranchName;
  }
  public String getReqBranchName(){
   return ReqBranchName;
  }
  public void setReqPersonName(String ReqPersonName){
   this.ReqPersonName=ReqPersonName;
  }
  public String getReqPersonName(){
   return ReqPersonName;
  }
  public void setRequestNo(Integer RequestNo){
   this.RequestNo=RequestNo;
  }
  public Integer getRequestNo(){
   return RequestNo;
  }
  public void setReqBranchCode(String ReqBranchCode){
   this.ReqBranchCode=ReqBranchCode;
  }
  public String getReqBranchCode(){
   return ReqBranchCode;
  }
}