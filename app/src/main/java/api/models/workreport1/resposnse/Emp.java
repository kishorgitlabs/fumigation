package api.models.workreport1.resposnse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Emp{
  @SerializedName("Jobcardid")
  @Expose
  private Integer Jobcardid;
  @SerializedName("TW")
  @Expose
  private Object TW;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("ContainerNo")
  @Expose
  private String ContainerNo;
  @SerializedName("TotalWorker")
  @Expose
  private String TotalWorker;
  @SerializedName("CreateDate")
  @Expose
  private String CreateDate;
  public void setJobcardid(Integer Jobcardid){
   this.Jobcardid=Jobcardid;
  }
  public Integer getJobcardid(){
   return Jobcardid;
  }
  public void setTW(Object TW){
   this.TW=TW;
  }
  public Object getTW(){
   return TW;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setContainerNo(String ContainerNo){
   this.ContainerNo=ContainerNo;
  }
  public String getContainerNo(){
   return ContainerNo;
  }
  public void setTotalWorker(String TotalWorker){
   this.TotalWorker=TotalWorker;
  }
  public String getTotalWorker(){
   return TotalWorker;
  }
  public void setCreateDate(String CreateDate){
   this.CreateDate=CreateDate;
  }
  public String getCreateDate(){
   return CreateDate;
  }
}