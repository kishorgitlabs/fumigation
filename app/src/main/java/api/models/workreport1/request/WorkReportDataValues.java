package api.models.workreport1.request;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class WorkReportDataValues{
  @SerializedName("Jobcardid")
  @Expose
  private String Jobcardid;
  @SerializedName("TW")
  @Expose
  private List<TW> TW;
  @SerializedName("ContainerNo")
  @Expose
  private String ContainerNo;
  public void setJobcardid(String Jobcardid){
   this.Jobcardid=Jobcardid;
  }
  public String getJobcardid(){
   return Jobcardid;
  }
  public void setTW(List<TW> TW){
   this.TW=TW;
  }
  public List<TW> getTW(){
   return TW;
  }
  public void setContainerNo(String ContainerNo){
   this.ContainerNo=ContainerNo;
  }
  public String getContainerNo(){
   return ContainerNo;
  }
}