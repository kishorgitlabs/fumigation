package api.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class ImeiData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private Imei data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(Imei data){
   this.data=data;
  }
  public Imei getData(){
   return data;
  }
}