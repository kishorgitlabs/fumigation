
package api.models.attendance;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AttendanceData {

    @SerializedName("data")
    private List<Attendancename> mData;
    @SerializedName("result")
    private String mResult;

    public List<Attendancename> getData() {
        return mData;
    }

    public void setData(List<Attendancename> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
