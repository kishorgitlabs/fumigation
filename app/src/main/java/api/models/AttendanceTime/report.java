
package api.models.AttendanceTime;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class report {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("attendanceTime")
    private String mAttendanceTime;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("Designation")
    private Object mDesignation;
    @SerializedName("EmpId")
    private Integer mEmpId;
    @SerializedName("id")
    private Integer mId;
    @SerializedName("InTime")
    private Object mInTime;
    @SerializedName("Latitude")
    private Object mLatitude;
    @SerializedName("Longitude")
    private Object mLongitude;
    @SerializedName("Name")
    private String mName;
    @SerializedName("OutTime")
    private String mOutTime;
    @SerializedName("WorkLocation")
    private String mWorkLocation;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAttendanceTime() {
        return mAttendanceTime;
    }

    public void setAttendanceTime(String attendanceTime) {
        mAttendanceTime = attendanceTime;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Object getDesignation() {
        return mDesignation;
    }

    public void setDesignation(Object designation) {
        mDesignation = designation;
    }

    public Integer getEmpId() {
        return mEmpId;
    }

    public void setEmpId(Integer empId) {
        mEmpId = empId;
    }

    public Integer getId() {
        return mId;
    }

    public void setId(Integer id) {
        mId = id;
    }

    public Object getInTime() {
        return mInTime;
    }

    public void setInTime(Object inTime) {
        mInTime = inTime;
    }

    public Object getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Object latitude) {
        mLatitude = latitude;
    }

    public Object getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Object longitude) {
        mLongitude = longitude;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOutTime() {
        return mOutTime;
    }

    public void setOutTime(String outTime) {
        mOutTime = outTime;
    }

    public String getWorkLocation() {
        return mWorkLocation;
    }

    public void setWorkLocation(String workLocation) {
        mWorkLocation = workLocation;
    }

}
