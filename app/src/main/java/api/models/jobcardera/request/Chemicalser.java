
package api.models.jobcardera.request;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Chemicalser {

    @SerializedName("chemicals")
    private String mChemicals;
    @SerializedName("Quatity")
    private String mQuatity;
    @SerializedName("SerialNumber")
    private String mSerialNumber;

    public String getChemicals() {
        return mChemicals;
    }

    public void setChemicals(String chemicals) {
        mChemicals = chemicals;
    }

    public String getQuatity() {
        return mQuatity;
    }

    public void setQuatity(String quatity) {
        mQuatity = quatity;
    }

    public String getSerialNumber() {
        return mSerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        mSerialNumber = serialNumber;
    }

}
