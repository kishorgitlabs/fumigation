
package api.models.jobcardera.response;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("chemicals1")
    private List<Chemicals1> mChemicals1;
    @SerializedName("emp")
    private Emp mEmp;

    public List<Chemicals1> getChemicals1() {
        return mChemicals1;
    }

    public void setChemicals1(List<Chemicals1> chemicals1) {
        mChemicals1 = chemicals1;
    }

    public Emp getEmp() {
        return mEmp;
    }

    public void setEmp(Emp emp) {
        mEmp = emp;
    }

}
