package api.models.Upload;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class UploadActivity{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private upload data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(upload data){
   this.data=data;
  }
  public upload getData(){
   return data;
  }
}