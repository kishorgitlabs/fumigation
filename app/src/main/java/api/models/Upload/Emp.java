package api.models.Upload;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Emp{
  @SerializedName("LorryDriverMobNo")
  @Expose
  private Long LorryDriverMobNo;
  @SerializedName("Status")
  @Expose
  private String Status;
  @SerializedName("AudioData")
  @Expose
  private Object AudioData;
  @SerializedName("Updatedate")
  @Expose
  private String Updatedate;
  @SerializedName("VehicleNo")
  @Expose
  private String VehicleNo;
  @SerializedName("Images")
  @Expose
  private String Images;
  @SerializedName("Image2")
  @Expose
  private String Image2;
  @SerializedName("Insertdate")
  @Expose
  private Object Insertdate;
  @SerializedName("Image1")
  @Expose
  private String Image1;
  @SerializedName("ContainerNo")
  @Expose
  private String ContainerNo;
  @SerializedName("Image")
  @Expose
  private String Image;
  @SerializedName("AudioContentType")
  @Expose
  private Object AudioContentType;
  @SerializedName("chemicals1")
  @Expose
  private Object chemicals1;
  @SerializedName("Remarks")
  @Expose
  private String Remarks;
  @SerializedName("workreport")
  @Expose
  private String workreport;
  @SerializedName("warid")
  @Expose
  private Integer warid;
  @SerializedName("vhid")
  @Expose
  private Integer vhid;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("Quatity")
  @Expose
  private Object Quatity;
  @SerializedName("Audio")
  @Expose
  private String Audio;
  @SerializedName("jobcardid")
  @Expose
  private String jobcardid;
  @SerializedName("LorryDriverName")
  @Expose
  private String LorryDriverName;
  public void setLorryDriverMobNo(Long LorryDriverMobNo){
   this.LorryDriverMobNo=LorryDriverMobNo;
  }
  public Long getLorryDriverMobNo(){
   return LorryDriverMobNo;
  }
  public void setStatus(String Status){
   this.Status=Status;
  }
  public String getStatus(){
   return Status;
  }
  public void setAudioData(Object AudioData){
   this.AudioData=AudioData;
  }
  public Object getAudioData(){
   return AudioData;
  }
  public void setUpdatedate(String Updatedate){
   this.Updatedate=Updatedate;
  }
  public String getUpdatedate(){
   return Updatedate;
  }
  public void setVehicleNo(String VehicleNo){
   this.VehicleNo=VehicleNo;
  }
  public String getVehicleNo(){
   return VehicleNo;
  }
  public void setImages(String Images){
   this.Images=Images;
  }
  public String getImages(){
   return Images;
  }
  public void setImage2(String Image2){
   this.Image2=Image2;
  }
  public String getImage2(){
   return Image2;
  }
  public void setInsertdate(Object Insertdate){
   this.Insertdate=Insertdate;
  }
  public Object getInsertdate(){
   return Insertdate;
  }
  public void setImage1(String Image1){
   this.Image1=Image1;
  }
  public String getImage1(){
   return Image1;
  }
  public void setContainerNo(String ContainerNo){
   this.ContainerNo=ContainerNo;
  }
  public String getContainerNo(){
   return ContainerNo;
  }
  public void setImage(String Image){
   this.Image=Image;
  }
  public String getImage(){
   return Image;
  }
  public void setAudioContentType(Object AudioContentType){
   this.AudioContentType=AudioContentType;
  }
  public Object getAudioContentType(){
   return AudioContentType;
  }
  public void setChemicals1(Object chemicals1){
   this.chemicals1=chemicals1;
  }
  public Object getChemicals1(){
   return chemicals1;
  }
  public void setRemarks(String Remarks){
   this.Remarks=Remarks;
  }
  public String getRemarks(){
   return Remarks;
  }
  public void setWorkreport(String workreport){
   this.workreport=workreport;
  }
  public String getWorkreport(){
   return workreport;
  }
  public void setWarid(Integer warid){
   this.warid=warid;
  }
  public Integer getWarid(){
   return warid;
  }
  public void setVhid(Integer vhid){
   this.vhid=vhid;
  }
  public Integer getVhid(){
   return vhid;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setQuatity(Object Quatity){
   this.Quatity=Quatity;
  }
  public Object getQuatity(){
   return Quatity;
  }
  public void setAudio(String Audio){
   this.Audio=Audio;
  }
  public String getAudio(){
   return Audio;
  }
  public void setJobcardid(String jobcardid){
   this.jobcardid=jobcardid;
  }
  public String getJobcardid(){
   return jobcardid;
  }
  public void setLorryDriverName(String LorryDriverName){
   this.LorryDriverName=LorryDriverName;
  }
  public String getLorryDriverName(){
   return LorryDriverName;
  }
}