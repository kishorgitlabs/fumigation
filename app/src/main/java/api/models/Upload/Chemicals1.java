package api.models.Upload;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Chemicals1{
  @SerializedName("container_number")
  @Expose
  private Object container_number;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("Quatity")
  @Expose
  private Integer Quatity;
  @SerializedName("chemicals")
  @Expose
  private String chemicals;
  public void setContainer_number(Object container_number){
   this.container_number=container_number;
  }
  public Object getContainer_number(){
   return container_number;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setQuatity(Integer Quatity){
   this.Quatity=Quatity;
  }
  public Integer getQuatity(){
   return Quatity;
  }
  public void setChemicals(String chemicals){
   this.chemicals=chemicals;
  }
  public String getChemicals(){
   return chemicals;
  }
}