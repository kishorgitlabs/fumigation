package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.brainmagic.fumigation.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import api.models.alertnames.GetAllName;
import api.models.workreport1.request.TW;

/**
 * Created by SYSTEM10 on 2/14/2019.
 */

public class ViewAllNameAdapter  extends ArrayAdapter<String> {
    private Context context;
    private List<GetAllName> allnames;
  //  private List<GetAllName> checklistbalaji;
    private List<TW> checklist;
    private List<Integer> checklistid;
    private HashMap<String,TW> hashSet;
    private Map<Integer,Boolean>ischeckedmap;
 //   private ClickInterface clickInterface;
    TW tw;
    public ViewAllNameAdapter(@NonNull Context context, List<GetAllName> allnames) {
        super(context, R.layout.vieallnames);
        this.context = context;
        this.allnames = allnames;
        checklist=new ArrayList<TW>();
        checklistid=new ArrayList<>();
        hashSet=new HashMap<>();
        ischeckedmap=new HashMap<>();
     //   checklistbalaji=new ArrayList<>();
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.vieallnames, parent, false);
            try {
                //clickInterface= (ClickInterface) context;
                CheckBox checkbox=(CheckBox) convertView.findViewById(R.id.checkbox);
                TextView textcheckbox=(TextView) convertView.findViewById(R.id.textcheckbox);
                textcheckbox.setText(allnames.get(position).getName());
                if(ischeckedmap.containsKey(position)){
                    checkbox.setChecked(ischeckedmap.get(position));
                }
                checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                     @Override
                     public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

                         if(checked){
                             TW tw=new TW();
                             tw.setTotalWorkers(allnames.get(position).getName());
                             hashSet.put(allnames.get(position).getName(),tw);
                             checklist.add(tw);
                             ischeckedmap.put(position,checked);
                             checkbox.setChecked(true);
                         }else{
                             checklist.remove(hashSet.get(allnames.get(position).getName()));
                             ischeckedmap.remove(position);
                             checkbox.setChecked(false);
                         }
                         //clickInterface.onCheckedListener(checklist,checked);
                     }

                 });


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return convertView;
    }

    private class Jobcarddetailsfumi {

        public TextView allname;
        public CheckBox checkbox;



    }
    public List<TW> getWork() {

        return checklist;
    }


    @Override
    public int getCount() {
        return allnames.size();
    }

    public interface ClickInterface{
        void onCheckedListener(List<TW> list,boolean isChecked);
    }
}
