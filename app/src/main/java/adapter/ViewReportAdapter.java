package adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.brainmagic.fumigation.ViewReportFilesFragment;
import com.brainmagic.fumigation.ViewReportFragment;

/**
 * Created by SYSTEM05 on 12/20/2018.
 */

public class ViewReportAdapter extends FragmentPagerAdapter {
    public ViewReportAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new ViewReportFragment();
            case 1 :
                return new ViewReportFilesFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
