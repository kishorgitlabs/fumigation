package adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.fumigation.R;
import com.brainmagic.fumigation.ViewMaterialObjectActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import api.models.viewmaterialData.ViewMaterial;

/**
 * Created by SYSTEM10 on 2/8/2019.
 */

public class ViewMaterialAdapter extends ArrayAdapter {
    private Context context;
    private List<ViewMaterial> data;
    private List<ViewMaterial>data1;

    public ViewMaterialAdapter(@NonNull Context context, List<ViewMaterial> data) {
        super(context, R.layout.viewmaterials, data);
        this.context = context;
        this.data = data;
        data1=new ArrayList<>();
        this.data1.addAll(data);
    }

    @SuppressLint("WrongConstant")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewMaterialData holder;
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.viewmaterials, parent, false);

            holder = new ViewMaterialData();

            holder.sno = (TextView) convertView.findViewById(R.id.serialno1);
            holder.requestno = (TextView) convertView.findViewById(R.id.requestno);
         //   holder.branchname = (TextView) convertView.findViewById(R.id.branchname);
            holder.date = (TextView) convertView.findViewById(R.id.date);


            holder.sno.setText(Integer.toString(position + 1) + ".");
            holder.requestno.setText(String.valueOf(data.get(position).getRequestNo()));
          //  holder.branchname.setText(String.valueOf(data.get(position).getReqBranchName()));
            if(data.get(position).getCreateDate() !=null){
                String [] date =data.get(position).getCreateDate().split("T");
                holder.date.setText(date[0]);
            }else {
                holder.date.setText("");
            }
            holder.requestno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, ViewMaterialObjectActivity.class).putExtra("requestno", String.valueOf(data.get(position).getRequestNo())));
                }
            });


            convertView.setTag(holder);
        } else {
            holder = (ViewMaterialData) convertView.getTag();
        }

        return convertView;
    }

    public void filter(String text) {
        text = text.toLowerCase(Locale.getDefault());
        data.clear();

        if (text.length() == 0) {
            data.addAll(data1);

        } else {
            for (int i=0;i<data1.size();i++) {
                String wp = data1.get(i).getCreateDate().toLowerCase();
                String wp1=data1.get(i).getRequestNo().toLowerCase();
                if (wp.toLowerCase(Locale.getDefault()).contains(text.toLowerCase()) ||wp1.toLowerCase(Locale.getDefault()).contains(text.toLowerCase())) {
                    data.add(data1.get(i));
                    notifyDataSetChanged();
                }
            }
        }
    }

    private class ViewMaterialData {

        public TextView sno;
        public TextView requestno;
        public TextView branchname;
        public TextView date;


    }
}
