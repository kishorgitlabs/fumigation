package adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Outline;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.fumigation.ContainerDetailsActivity;
import com.brainmagic.fumigation.R;

import java.util.List;

import api.models.jobcard.JobCard;

/**
 * Created by SYSTEM10 on 10/23/2018.
 */

public class ViewAttendanceAdapter extends ArrayAdapter<String> {
    private Context context;
    private  List<String> namelist, DateList, Intimelist, outtimelist;

    public ViewAttendanceAdapter(@NonNull Context context, List<String> namelist, List<String>DateList, List<String>Intimelist,List<String>outtimelist) {
        super(context, R.layout.viewattendance,namelist);
        this.context = context;
        this.namelist = namelist ;
        this.DateList = DateList ;
        this.Intimelist = Intimelist ;
        this.outtimelist = outtimelist ;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null)
        {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.viewattendance, parent, false);
            try{

                //  TextView sno=(TextView)convertView.findViewById(R.id.sno1);
              //  TextView name=(TextView)convertView.findViewById(R.id.name1);
                TextView date=(TextView)convertView.findViewById(R.id.date1);
                TextView intime=(TextView)convertView.findViewById(R.id.intime);
                TextView outtime=(TextView)convertView.findViewById(R.id.outtime);
              // sno.setText(Integer.toString(position +1));
               // name.setText(namelist.get(position));
                if(DateList.get(position) != null){
                    String[] date1 =DateList.get(position).split(" ");
                    date.setText(date1[0]);

                }

                if(Intimelist.get(position).toString() != null){
                    String[] intime1=Intimelist.get(position).split("\\.");
                    intime.setText(intime1[0]);

                }else{
                    intime.setText("");
                }
               if(outtimelist.get(position).toString() != null){
                String[] outtime1=outtimelist.get(position).split("\\.");
                outtime.setText(outtime1[0]);

              }else{
                  outtime.setText("");
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        return convertView;
    }
    private class Jobcarddetailsfumi {

        public TextView sno;
        public TextView name;
        public TextView date;
        public TextView intime;
        public TextView outtime;


    }

    }
