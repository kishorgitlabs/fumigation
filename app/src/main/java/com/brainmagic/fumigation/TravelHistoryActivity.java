package com.brainmagic.fumigation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import persistance.ServerConnection;
import adapter.ViewHistoryAdapter;
import alert.Alertbox;
import logout.Logout;
import network.NetworkConnection;

public class TravelHistoryActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{
    private ListView listhistory;
    private ImageView menu,back;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Connection connection;
    private Button filterhistory;
    private Statement statement;
    private ResultSet resultSet;
    private int id;
    private ArrayList<String> DateList;
    private TreeMap<Integer,String> hashlist=new TreeMap<>(Collections.<Integer>reverseOrder());
    private boolean mAutoHighlight;
    private static final String TAG = "TravelHistoryActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel_history);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        listhistory=(ListView)findViewById(R.id.listhistory);
        filterhistory=(Button)findViewById(R.id.filterhistory);
        id=myshare.getInt("id",0);
        hashlist.clear();
        menu=(ImageView)findViewById(R.id.menu) ;
        back=(ImageView)findViewById(R.id.back) ;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        filterhistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        (DatePickerDialog.OnDateSetListener) TravelHistoryActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );

                dpd.setAutoHighlight(mAutoHighlight);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(TravelHistoryActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.home:
                                Intent i2=new Intent(getApplicationContext(), SupervisorHomeActivity.class);
                                startActivity(i2);
                                return true;
                            case R.id.menuLogout:
                                new Logout(TravelHistoryActivity.this).log_out();
                                return true;
                            case R.id.jobcard:
                                Intent i=new Intent(getApplicationContext(), Jobcarddetails.class);
                                startActivity(i);
                                return true;
                            case R.id.attendance:
                                Intent i3=new Intent(getApplicationContext(), AttendanceViewNewActivity.class);
                                startActivity(i3);
                                return true;
                            case R.id.viewvisit:
                                Intent i5=new Intent(getApplicationContext(), ViewReportActivity.class);
                                startActivity(i5);
                                return true;
                            case R.id.visitreport:
                                Intent i4=new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i4);
                                return true;
                            case R.id.changepassword:
                                Intent i6=new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i6);
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menu);
                popupMenu.show();
                ;
            }
        });
        checkInternet();
    }

    private void checkInternet() {
        NetworkConnection networkConnection=new NetworkConnection(TravelHistoryActivity.this);
        if(networkConnection.CheckInternet()){
           new Travelhistory().execute("Select * from AttendanceTable where EmpId = '"+ id +"'");
        }else{
            box.showAlertbox(getResources().getString(R.string.app_name));
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String date = "" + (monthOfYear + 1) + "You picked the following date: From- \" + dayOfMonth + \"//" + year + " To " + dayOfMonthEnd + "/" + (monthOfYearEnd+1) + "/" + yearEnd;
        String FromDate = "";
        String Todate = "";

        Toast.makeText(TravelHistoryActivity.this, date, Toast.LENGTH_LONG).show();

        if (monthOfYear+1 < 10)
            FromDate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;
        else {
            FromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
        }

        if (monthOfYearEnd+1 < 10)
            Todate = yearEnd + "-0" + (monthOfYearEnd + 1) + "-" + dayOfMonthEnd;
        else {
            Todate = yearEnd + "-" + (monthOfYearEnd + 1) + "-" + dayOfMonthEnd;
        }

//        new GetTravelHistory().execute("select datetime from Travelcoordinates where S_id = '" + salesID + "' and convert(nvarchar,datetime) between '" + FromDate + "' and '" + Todate + "' and IsCancelled ='0' order by datetime desc");
//        new Travelhistory().execute("select  * from AttendanceTable where EmpId = '" + id + "' and date between '" + FromDate + "' and '" + Todate + "' order by date desc");
        new Travelhistory().execute("select id,date from AttendanceTable where EmpId = '" + id + "' and date between '" + FromDate + "' and '" + Todate + "' order by date desc");
    }


    private class Travelhistory extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
//        private Integer id;
        private SQLiteDatabase db;
        private List<String>namelist,datelist,intimelist,fromlocation,outtime,tolocation,fromlat,fromlong,tolat,tolong;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(TravelHistoryActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            DateList = new ArrayList<String>();
            datelist=new ArrayList<>();
            namelist=new ArrayList<>();
            intimelist=new ArrayList<>();
            fromlocation=new ArrayList<>();
            outtime=new ArrayList<>();
            tolocation=new ArrayList<>();
            fromlat=new ArrayList<>();
            fromlong=new ArrayList<>();
            tolat=new ArrayList<>();
            tolong=new ArrayList<>();
            hashlist.clear();



//            descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//            quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//            productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);


        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery =  strings[0];
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    DateList.add(resultSet.getString("Date"));
                    hashlist.put(resultSet.getInt("id"),resultSet.getString("Date"));
                    Log.d(TAG, "doInBackground: datetime"+resultSet.getString("Date"));
                }
                for(Integer i:hashlist.keySet()) {
                    Log.v("query", selectquery);
                    String query2 = "select * from AttendanceTable where EmpId = '" + id + "' and date = '" + hashlist.get(i) + "'and id ='" + i + "' order by Date desc";
                    resultSet = statement.executeQuery(query2);
                    while (resultSet.next()) {
                        Log.d(TAG, "doInBackground: fromlatitude "+resultSet.getString("OutLatitude"));
                        Log.d(TAG, "doInBackground: fromongitude "+resultSet.getString("OutLongitude"));
//                        namelist.add(resultSet.getString("Name"));
                        datelist.add(resultSet.getString("Date"));
                        intimelist.add(resultSet.getString("InTime"));
                        outtime.add(resultSet.getString("OutTime"));
                        fromlocation.add(resultSet.getString("Address"));
                        tolocation.add(resultSet.getString("OutAddress"));
                        fromlat.add(resultSet.getString("InLatitude"));
                        fromlong.add(resultSet.getString("InLongitude"));
                        tolat.add(resultSet.getString("OutLatitude"));
                        tolong.add(resultSet.getString("OutLongitude"));


                    }
                }
                if (!datelist.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                ViewHistoryAdapter viewAttendanceAdapter=new ViewHistoryAdapter(TravelHistoryActivity.this,namelist,datelist,intimelist,outtime,fromlocation,tolocation,fromlat,fromlong,tolat,tolong);
                listhistory.setAdapter(viewAttendanceAdapter);

            } else if (s.equals("empty")) {
                box.showNegativebox("No data found!");
            } else {
                box.showNegativebox("Kindly check your Internet Connection");
            }
        }
    }
}
