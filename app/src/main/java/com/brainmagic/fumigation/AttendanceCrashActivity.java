package com.brainmagic.fumigation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.TrafficStats;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.net.ssl.HttpsURLConnection;

import alert.Alertbox;
import api.models.AttendanceTime.AttendanceReport;
import api.models.workreport.LocationData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import logout.Logout;
import network.NetworkConnection;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import service.BGServicenormal;
import toaster.Toasts;

public class AttendanceCrashActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, android.location.LocationListener, com.google.android.gms.location.LocationListener
        ,EasyPermissions.PermissionCallbacks,ResultCallback<LocationSettingsResult> {
    private Button check_button;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private int REQUEST_CHECK_SETTINGS = 100;
    private static final int TWENTY_MINUTES = 1* 60 * 1000;
    private Intent service;
    private GoogleApiClient mGoogleApiClient;
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    private Double FromLatitude, FromLongitude;
    private String toaddress, fromaddress;
    private boolean alreadyCalled=false;
    private int fromYDelta=0;
    private int fromXDelta=325;
    private ProgressDialog mProgressDialog;
    private String City="";
    private Alertbox box1 = new Alertbox(AttendanceCrashActivity.this);
    private LocationManager mlocationManager;
    private int MIN_DISTANCE_CHANGE_FOR_UPDATES=500;
    private Location mLastLocation;
    private KeyStore keyStore;
    private static final String TAG = "AttendanceCrashActivity";
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "androidHive";
    private Cipher cipher;
    private String empname,empid;
    private Double latitude,longitude;
    private String inText="InTime";
    private GoogleApiClient googleApiClient;
    private ViewGroup transitionsContainer;
    private ProgressDialog loading;
    private TextView name;
    private LocationRequest locationRequest;
    private Location mylocation;
    private LocationManager locationManager;
    public static final String APIKEY =  "AIzaSyBRmYkOOy9QhrI53Fp3h_Tt8t7amWNa4Q0";
    private final static int REQUEST_CHECK_SETTINGS_GPS=100;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;
    private boolean loc=false;
    private String address3;
    LocationHelper locationHelper;
    private static  final int MY_PERMISSIONS_REQUEST_SEND_SMS=101;
    private String  code;
    private Alertbox box = new Alertbox(this);
    private MaterialSpinner worklocate;
    private TextView designation,address2,ename;
    private ImageView menu,back,attendancetime;
    private  Button attendancebtn;
    String address, address1, city, state, country, postalCode;
    private String employeename,design;
    private List<String> locationdata;
    private String selectwork;
    private String currentDate,formattedtime,formatedtime2;
    private String currentdate,currentdating,formatdating,formatdate1;
    private boolean checktime =false;
    private boolean isCheckedIn=false;
    private boolean istiming=false;
    private boolean islinit =false;
    private boolean servuce=false;
    private LinearLayout locatemat,locatetext;
    private TextView worklocatetext;
    private Toasts toasts = new Toasts(this);
   // public static final String BROADCAST_ACTION = "com.brainmagic.fumigation";
   // MyBroadCastReceiver myBroadCastReceiver;
    private Object timerCount;
    private int timer,timer1,timer2,timer3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_crash);
        mlocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
      //  myBroadCastReceiver = new MyBroadCastReceiver();
        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction(BROADCAST_ACTION);
//        registerReceiver(myBroadCastReceiver, intentFilter);
        attendancetime=(ImageView)findViewById(R.id.attendancetime);
        locationHelper = new LocationHelper(this);
        locationHelper.checkpermission();;
        ename=(TextView)findViewById(R.id.ename);
        isCheckedIn=myshare.getBoolean("isCheckedIn",isCheckedIn);
        istiming=myshare.getBoolean("istiming",istiming);
        servuce=myshare.getBoolean("service",servuce);
        islinit=myshare.getBoolean("islinit",islinit);
        worklocate=(MaterialSpinner)findViewById(R.id.worklocate);
        designation=(TextView)findViewById(R.id.designation);
        address2=(TextView)findViewById(R.id.address);
        attendancebtn=(Button)findViewById(R.id.attendancebtn);
        back=(ImageView)findViewById(R.id.back);
        menu=(ImageView)findViewById(R.id.menu) ;
        name=(TextView)findViewById(R.id.nameedit);
        locatemat=(LinearLayout)findViewById(R.id.locatemat);
        locatetext=(LinearLayout)findViewById(R.id.locatetext);
        worklocatetext=(TextView) findViewById(R.id.worklocatetext);
        employeename=myshare.getString("name","");
        design=myshare.getString("usertype","");
        ename.setText(employeename);
        designation.setText(design);
        //date
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        currentdating = df.format(c.getTime());
        //currentdating = "2018-12-20";
        //editor.putString("currentdate",currentDate);
        editor.commit();
        // currentdating=myshare.getString("currentdate","");
        formatdating=myshare.getString("formateddate","");
        formatdate1= formatdating;
       // formatdate1="2019-04-04";
        //Time
        Calendar cal = Calendar.getInstance();
        Date currentTime = cal.getTime();
        SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
        formattedtime= dftime.format(currentTime);

        if(formatdate1.equals(currentdating)){
            if(istiming == false){
                attendancebtn.setText("In Time");
            }else if(servuce==true){
                attendancebtn.setText("In Time");
            }else{
                attendancebtn.setText("Out Time");
                locatemat.setVisibility(View.GONE);
                locatetext.setVisibility(View.VISIBLE);
                worklocatetext.setText(myshare.getString("worklocation",""));
            }
        }else{
            attendancebtn.setText("In Time");
            locatemat.setVisibility(View.VISIBLE);
            locatetext.setVisibility(View.GONE);
            isCheckedIn=false;
            editor.putBoolean("isCheckedIn", isCheckedIn);
            editor.commit();
        }



//              if( currentdating.equals(formatdating) && istiming == false){
//                  if(currentdating.equals(formatdating) && isCheckedIn == false)
//                  {
//                      attendancebtn.setText("In Time");
//                  }
//                  else if(currentdating.equals(formatdating)&& isCheckedIn == true){
//                      attendancebtn.setText("Out Time");
//                  }else if(!currentdating.equals(formatdating) && isCheckedIn == true){
//                          attendancebtn.setText("In Time");
//                  }
//              }else if(!currentdating.equals(formatdating) && istiming == true){
//                  attendancebtn.setText("In Time");
//
//              }else if(currentdating.equals(formatdating) && istiming == true){
//                  if(islinit == true){
//                      attendancebtn.setText("In Time");
//                  }
//
//
//              }

        attendancebtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if(ename.getText().toString().equals("Select Name")){
                    StyleableToast st = new StyleableToast(AttendanceCrashActivity.this, "Enter Employee name !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(AttendanceCrashActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                } else if(designation.getText().toString().equals("")){
                    StyleableToast st = new StyleableToast(AttendanceCrashActivity.this, "Enter your designation !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(AttendanceCrashActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                } else if(address2.getText().toString().equals("")){
                    StyleableToast st = new StyleableToast(AttendanceCrashActivity.this, "Enter your address!", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(AttendanceCrashActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                }else if(worklocate.getText().equals("Select location") && istiming == false) {
                    toasts.ShowErrorToast("Please Enter Location");
                }else
                    if(istiming == false){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            FingerprintManager fingerprintManager = (FingerprintManager) AttendanceCrashActivity.this.getSystemService(Context.FINGERPRINT_SERVICE);
                            if (fingerprintManager.isHardwareDetected()) {
                                Intent i = new Intent(AttendanceCrashActivity.this, FingerprintActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                editor.putString("worklocation", selectwork);
                                //editor.putBoolean("istiming", true);
                                editor.commit();
                                startActivity(i);
                            } else {

                                NetworkConnection network = new NetworkConnection(AttendanceCrashActivity.this);
                                if (network.CheckInternet()) {
                                    if (isCheckedIn) {
                                        new startAttendance().execute("stop", "");

                                        //inflateAlertBox(formattedtime);
                                    } else {
                                        sendSMS();

                                    }

                                }

                            }
                        }else {
                            NetworkConnection network = new NetworkConnection(AttendanceCrashActivity.this);
                            if (network.CheckInternet()) {
                                if (isCheckedIn) {
                                    new startAttendance().execute("stop", "");

                                    //inflateAlertBox(formattedtime);


                                } else {
                                    sendSMS();


                                }

                            }
                        }


                } else if(istiming == true){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        FingerprintManager fingerprintManager = (FingerprintManager) AttendanceCrashActivity.this.getSystemService(Context.FINGERPRINT_SERVICE);
                        if (fingerprintManager.isHardwareDetected()) {
                            Intent i = new Intent(AttendanceCrashActivity.this, FingerprintActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            editor.putString("worklocation", selectwork);
                            //editor.putBoolean("istiming", true);
                            editor.commit();
                            startActivity(i);
                        } else {

                            NetworkConnection network = new NetworkConnection(AttendanceCrashActivity.this);
                            if (network.CheckInternet()) {
                                if (isCheckedIn) {
                                    new startAttendance().execute("stop", "");

                                    //inflateAlertBox(formattedtime);


                                } else {
                                    sendSMS();


                                }

                            }

                        }
                    }else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            FingerprintManager fingerprintManager = (FingerprintManager) AttendanceCrashActivity.this.getSystemService(Context.FINGERPRINT_SERVICE);
                            if (fingerprintManager.isHardwareDetected()) {
                                Intent i = new Intent(AttendanceCrashActivity.this, FingerprintActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                editor.putString("worklocation", selectwork);
                                //editor.putBoolean("istiming", true);
                                editor.commit();
                                startActivity(i);
                            } else {

                                NetworkConnection network = new NetworkConnection(AttendanceCrashActivity.this);
                                if (network.CheckInternet()) {
                                    if (isCheckedIn) {
                                        new startAttendance().execute("stop", "");

                                        //inflateAlertBox(formattedtime);


                                    } else {
                                        sendSMS();


                                    }

                                }

                            }
                        }else{
                            NetworkConnection network = new NetworkConnection(AttendanceCrashActivity.this);
                            if (network.CheckInternet()) {
                                if (isCheckedIn) {
                                    new startAttendance().execute("stop", "");

                                    //inflateAlertBox(formattedtime);


                                } else {
                                    sendSMS();


                                }

                            }
                        }
                    }

                }

//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    FingerprintManager fingerprintManager = (FingerprintManager) AttendanceCrashActivity.this.getSystemService(Context.FINGERPRINT_SERVICE);
//                    if (fingerprintManager.isHardwareDetected()) {
//                        Intent i = new Intent(AttendanceCrashActivity.this, FingerprintActivity.class);
//                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        editor.putString("worklocation", selectwork);
//                        //editor.putBoolean("istiming", true);
//                        editor.commit();
//                        startActivity(i);
//                    } else {
//
//                        NetworkConnection network = new NetworkConnection(AttendanceCrashActivity.this);
//                        if (network.CheckInternet()) {
//                            if (isCheckedIn) {
//                                new startAttendance().execute("stop", "");
//
//                                //inflateAlertBox(formattedtime);
//
//
//                            } else {
//                                sendSMS();
//
//
//                            }
//
//                        }
//
//                    }
//                }else {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        FingerprintManager fingerprintManager = (FingerprintManager) AttendanceCrashActivity.this.getSystemService(Context.FINGERPRINT_SERVICE);
//                        if (fingerprintManager.isHardwareDetected()) {
//                            Intent i = new Intent(AttendanceCrashActivity.this, FingerprintActivity.class);
//                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            editor.putString("worklocation", selectwork);
//                            //editor.putBoolean("istiming", true);
//                            editor.commit();
//                            startActivity(i);
//                        } else {
//
//                            NetworkConnection network = new NetworkConnection(AttendanceCrashActivity.this);
//                            if (network.CheckInternet()) {
//                                if (isCheckedIn) {
//                                    new startAttendance().execute("stop", "");
//
//                                    //inflateAlertBox(formattedtime);
//
//
//                                } else {
//                                    sendSMS();
//
//
//                                }
//
//                            }
//
//                        }
//                    }else{
//                        NetworkConnection network = new NetworkConnection(AttendanceCrashActivity.this);
//                        if (network.CheckInternet()) {
//                            if (isCheckedIn) {
//                                new startAttendance().execute("stop", "");
//
//                                //inflateAlertBox(formattedtime);
//
//
//                            } else {
//                                sendSMS();
//
//
//                            }
//
//                        }
//                    }
//                }
            }

        });


        worklocate.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectwork=item.toString();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(AttendanceCrashActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(AttendanceCrashActivity.this).log_out();
                                return true;
                            case R.id.changepassword:
                                Intent i2=new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;

                            case R.id.visitreport:
                                Intent i4=new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i4);
                                return true;
                            case  R.id.home:
                                Intent i5=new Intent(getApplicationContext(), SupervisorHomeActivity.class);
                                startActivity(i5);
                                return true;
                            case R.id.viewvisit:
                                Intent i6=new Intent(getApplicationContext(), ViewReportActivity.class);
                                startActivity(i6);
                                return true;
                            case R.id.jobcard:
                                Intent i3=new Intent(getApplicationContext(), Jobcarddetails.class);
                                startActivity(i3);
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menuattendance);
                popupMenu.show();
                ;
            }
        });
        attendancetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu=new PopupMenu(AttendanceCrashActivity.this,view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(AttendanceCrashActivity.this).log_out();
                                return true;
                            case R.id.view_Attendance:
                                Intent i2=new Intent(getApplicationContext(), ViewAttendance.class);
                                startActivity(i2);
                                return true;
                            case R.id.Travelhistory:
                                Intent i5=new Intent(getApplicationContext(), TravelHistoryActivity.class);
                                startActivity(i5);
                                return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.attendancemenu);
                popupMenu.show();

            }
        });

        getNetworkStatus();
//        if(!isCheckedIn)
//        {
//            check_button.setText("In Time");
//        }
//        else
//        {
//            check_button.setText("Out Time");
//        }

        NetworkConnection network = new NetworkConnection(AttendanceCrashActivity.this);
        if (network.CheckInternet())
        {
            AskLocationPermission();
//            mProgressDialog= ProgressDialog.show(this,"Location","Getting Your Location",false,false);
        }
        else {
            Alertbox alert = new Alertbox(AttendanceCrashActivity.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }
        NetworkConnection network1 = new NetworkConnection(AttendanceCrashActivity.this);
        if (network1.CheckInternet())
        {
            GetLocation();
//            mProgressDialog= ProgressDialog.show(this,"Location","Getting Your Location",false,false);
        }
        else {
            Alertbox alert = new Alertbox(AttendanceCrashActivity.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }

    }

    public void inflateAlertBox(String formattedtime) {
//        Calendar cal = Calendar.getInstance();
//        Date currentTime = cal.getTime();
//        SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
//        String formattedtime = dftime.format(currentTime);
        new FancyAlertDialog.Builder((Activity) AttendanceCrashActivity.this)
                .setTitle("Fumigation Services Field Force")
                .setBackgroundColor(Color.parseColor("#303F9F"))  //Don't pass R.color.colorvalue
                .setMessage("Your Current Time " + formattedtime + " is marked as your  Out Time Attendance")
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground(Color.parseColor("#FF4081"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("Ok")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.SIDE)
                .isCancellable(false)
                .setIcon(R.drawable.checked, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {
                        // new startAttendance().execute("stop", "");
                        Intent i = new Intent(AttendanceCrashActivity.this, SupervisorHomeActivity.class);
                        AttendanceCrashActivity.this.startActivity(i);
                        ((Activity) AttendanceCrashActivity.this).finish();
                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }

    public void sendSMS()
    {
//        if (ContextCompat.checkSelfPermission(this,
//                Manifest.permission.SEND_SMS)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                    Manifest.permission.SEND_SMS)) {
//
//            } else {
//                ActivityCompat.requestPermissions(this,
//                        new String[]{Manifest.permission.SEND_SMS},
//                        MY_PERMISSIONS_REQUEST_SEND_SMS);
//            }
//        }
//        else {
        new startAttendance().execute("start","");
    }

    //}

    private void GetLocation() {
        try {
            final ProgressDialog loading = ProgressDialog.show(AttendanceCrashActivity.this, "Fumigation", "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<LocationData> call;
            call = service.location();
            call.enqueue(new Callback<LocationData>() {
                @Override
                public void onResponse(Call<LocationData> call, Response<LocationData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            locationdata = response.body().getData();
                            locationdata.add(0,"Select location");
                            worklocate.setItems(locationdata);


                        } else if (response.body().getResult().equals(" Not Success")) {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<LocationData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(mProgressDialog!=null)
            mProgressDialog.dismiss();
    }

    private void AskLocationPermission() {
        if (EasyPermissions.hasPermissions(AttendanceCrashActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            if (CheckLocationIsEnabled()) {
                // if location is enabled show place picker activity to use
                startLocationUpdates();

            } else {

                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(locationRequest);

                PendingResult<LocationSettingsResult> result =
                        LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                        final Status status = locationSettingsResult.getStatus();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                startLocationUpdates();
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    status.startResolutionForResult(
                                            AttendanceCrashActivity.this,
                                            REQUEST_CHECK_SETTINGS_GPS);
                                } catch (IntentSender.SendIntentException e) {
                                    Log.e(TAG, "Exception : " + e.getMessage());
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e(TAG, "Location settings are not satisfied.");
                                break;
                        }
                    }
                });


            }
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location), REQUEST_ID_MULTIPLE_PERMISSIONS, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);

        }
    }
    @SuppressLint("LongLogTag")
    protected void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates: ");
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if(googleApiClient.isConnected())
                    {
                        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                        mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    }
//                    if (mylocation == null)
                    {
                        mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                1000,
                                500, this);
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                1000,
                                500, this);
                    }
//                    else {
//                        mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
//                        locationManager.requestLocationUpdates(
//                                LocationManager.NETWORK_PROVIDER,
//                                1000,
//                                500, this);
//                    }
//                CheckInternet();
//                mProgressDialog=ProgressDialog.show(this,"Location","Getting Your Location",false,false);
                    Log.d(TAG, "startLocationUpdates: ");
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_ID_MULTIPLE_PERMISSIONS);
                    Log.d(TAG, "Permission Not Granted");
                }

            } else {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (googleApiClient.isConnected()) {
                    Log.d(TAG, "startLocationUpdates: else");

                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                }
//                if (mylocation == null)
                {
                    mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            1000,
                            500, this);
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            1000,
                            500, this);
                }
//                else {
//                    mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
//                    AskLocationPermission();
//                }

            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private boolean CheckLocationIsEnabled() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        if(googleApiClient!=null)
            mylocation= LocationServices.FusedLocationApi
                    .getLastLocation(googleApiClient);

        if (mylocation == null) {
            return false;
        } else {
            return true;
        }
    }

    public void getNetworkStatus()
    {
        NetworkConnection network = new NetworkConnection(AttendanceCrashActivity.this);
        if (network.CheckInternet())
        {
            setUpGClient();
            getLastLocation();
//            mProgressDialog= ProgressDialog.show(this,"Location","Getting Your Location",false,false);
        }
        else {
            Alertbox alert = new Alertbox(AttendanceCrashActivity.this);
            alert.showAlertbox("Kindly check your Internet Connection");

        }
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onPause() {
        super.onPause();
        try {
            if(googleApiClient!=null)
                if(googleApiClient.isConnected()) {
                    googleApiClient.disconnect();
                    Log.d(TAG, "onPause: " + isCheckedIn);
                    editor.putBoolean("isCheckedIn", isCheckedIn);
                    editor.commit();
                }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @SuppressLint("LongLogTag")
    @Override
    protected void onResume() {
        super.onResume();
        if(googleApiClient!=null)
            if(!alreadyCalled && googleApiClient.isConnected())
            {
                getNetworkStatus();
            }
        alreadyCalled=false;
        Log.d(TAG, "onResume: "+isCheckedIn);

    }
    public void getLastLocation()
    {
        int permissionLocation = ContextCompat.checkSelfPermission(AttendanceCrashActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
//            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
//            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
//            mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            locationRequest = new LocationRequest();
            locationRequest.setInterval(100);
            locationRequest.setFastestInterval(100);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setSmallestDisplacement(10);
            loc=true;
        }
    }

    private synchronized void setUpGClient() {
        try {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(this, this).build();
//            googleApiClient.connect();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(AttendanceCrashActivity.this);
                        alertDialog.setMessage("If you don't enable GPS, your travel cannot be tracked. Do you want to close the app?");
                        alertDialog.setTitle("Fumigation");
                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });

                        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AskLocationPermission();
                            }
                        });

                        alertDialog.show();
                        break;
                    default:
                        finish();
                        break;
                }
                break;
        }
    }
    @SuppressLint("LongLogTag")
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
        if(googleApiClient!=null)
            if (googleApiClient.isConnected()) {
                googleApiClient.disconnect();
            }
    }

    @SuppressLint("LongLogTag")
    private void CheckInternet() {

        Log.d(TAG, "CheckInternet: lat and long");
        NetworkConnection network = new NetworkConnection(AttendanceCrashActivity.this);
        if (network.CheckInternet()) {
            // relativeLayout.setVisibility(View.VISIBLE);
            Log.d(TAG, "CheckInternet: RelativeLayout");
            ShowCurrentLocationMarker();
        } else {
            Alertbox alert = new Alertbox(AttendanceCrashActivity.this);
            box.showAlertbox("Kindly check your Internet Connection");
            //  retry.setVisibility(View.VISIBLE);
            //relativeLayout.setVisibility(View.GONE);
        }
    }
    @SuppressLint("LongLogTag")
    private void ShowCurrentLocationMarker() {
        if (mLastLocation != null) {
            Log.d(TAG, "ShowCurrentLocationMarker: latitude "+mLastLocation.getLatitude()+" logitude "+mLastLocation.getLongitude());

            new GeocodeAsyncTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());     // Sets the center of the map to location user
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onLocationChanged(Location location) {
        mProgressDialog.dismiss();
        // check_button.setVisibility(View.VISIBLE);
        if (location != null) {
            mylocation = location;
            latitude=mylocation.getLatitude();
            longitude=mylocation.getLongitude();
            editor.putString("latitude",latitude.toString());
            editor.putString("longitude",longitude.toString());
            Log.d(TAG, "onLocationChanged: "+mylocation.getLatitude()+" long "+mylocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
            //Or Do whatever you want with your location
        }
        else if(mylocation!=null) {
            latitude=mylocation.getLatitude();
            longitude=mylocation.getLongitude();
            editor.putString("latitude",latitude.toString());
            editor.putString("longitude",longitude.toString());
            Log.d(TAG, "onLocationChanged: "+mylocation.getLatitude()+" long "+mylocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionFineLocation = ContextCompat.checkSelfPermission(AttendanceCrashActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCoarseLocation = ContextCompat.checkSelfPermission(AttendanceCrashActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permissionFineLocation == PackageManager.PERMISSION_GRANTED || permissionCoarseLocation == PackageManager.PERMISSION_GRANTED) {
            AskLocationPermission();
        }
        else {
            EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
        }
//        if(requestCode==MY_PERMISSIONS_REQUEST_SEND_SMS)
//        {
//            if (grantResults.length > 0
//                    && grantResults[0] == PackageManager.PERMISSION_GRANTED){
//             //   new startAttendance().execute("start","");
//            }else {
//                Toast.makeText(getApplicationContext(),
//                        "Please allow to acces Your SMS", Toast.LENGTH_LONG).show();
//                return;
//            }
//
//
//        }
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:

                // NO need to show the dialog;
                AskLocationPermission();

                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(AttendanceCrashActivity.this, REQUEST_CHECK_SETTINGS_GPS);
                } catch (IntentSender.SendIntentException e) {
                    //failed to show
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (CheckLocationIsEnabled()) {
            // if location is enabled show place picker activity to user
            startLocationUpdates();
        } else {
            // if location is not enabled show request to enable location to user
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(
                            googleApiClient,
                            builder.build()

                    );
            result.setResultCallback(this);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        AskLocationPermission();
    }




//    public void showworld() {
//        StyleableToast st = new StyleableToast(AttendanceCrashActivity.this, "Enter values", Toast.LENGTH_SHORT);
//        st.setBackgroundColor(AttendanceCrashActivity.this.getResources().getColor(R.color.colorPrimary));
//        st.setTextColor(Color.WHITE);
//        st.setMaxAlpha();
//        st.show();
//    }

    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";


        @SuppressLint("LongLogTag")
        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(AttendanceCrashActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }

                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            } else {
                new GetGeoCodeAPIAsynchTask().execute(latlang[0], latlang[1]);
            }

            return null;
        }

        @SuppressLint("LongLogTag")
        protected void onPostExecute(Address addresss) {

            if (addresss == null) {
                new GetGeoCodeAPIAsynchTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
                Log.d(TAG, "onPostExecute: *****");
            } else {
//                progressBar.setVisibility(View.GONE);
                address = addresss.getAddressLine(0);
                editor.putString("ToAddress", address);
                editor.commit();
                editor.apply();
                City = addresss.getLocality();
                Log.d(TAG, "onPostExecute: **************************" + City);
                String city = addresss.getLocality();
                String state = addresss.getAdminArea();
                //create your custom title
                String title = city + "-" + state;
                address2.setText(address +
                        "\n"
                        + title);
                editor.putString("FromAddress", address);
                Geocoder geocoder = new Geocoder(AttendanceCrashActivity.this);
                try {
                    ArrayList<Address> addresses = (ArrayList<Address>) geocoder.getFromLocationName("karur", 50);
                    for (Address address3 : addresses) {
                        double lat = address3.getLatitude();
                        double lon = address3.getLongitude();
//                        address2.setText(lat +
//                                "\n"
//                                + lon);

                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
                //create your custom title
//                String title = city + "-" + state;
//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertboxwithback("Your Current location is "+city);


            }
        }
    }



    private class startAttendance extends AsyncTask<String,Void,String>
    {
        String name,formattedtime,formattedDate,description,locate,lat,lon;
        int empid;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //date
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            formattedDate = df.format(c.getTime());

            //Time
            Calendar cal = Calendar.getInstance();
            Date currentTime = cal.getTime();
            SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
            formattedtime= dftime.format(currentTime);
            loading = ProgressDialog.show(AttendanceCrashActivity.this,"Loading","Please wait",false);
            name=myshare.getString("name","");
            description=myshare.getString("usertype","");
            //address=myshare.getString("address","");
            // locate=myshare.getString("worklocation","");
            // locate=getIntent().getStringExtra("worklocation");
            empid=myshare.getInt("id",0);
            lat=myshare.getString("latitude","");
            lon=myshare.getString("longitude","");
            code=myshare.getString("customercode","");
        }

        @Override
        protected String doInBackground(String... strings) {
            final String string=strings[0];
            try {
                final APIService service = RetrofitClient.getApiService();
                Call<AttendanceReport> call = service.attendnace(empid,name,formattedDate,formattedtime,selectwork,address,description,address,lat,lon,lat,lon,code);
                call.enqueue(new Callback<AttendanceReport>() {
                    @Override
                    public void onResponse(Call<AttendanceReport> call, Response<AttendanceReport> response) {
                        if (response.body().getResult().equals("Success")) {
                            editor.putInt("trackid", response.body().getData().getId());
                            editor.putInt("sid", response.body().getData().getEmpId());
                            if (string.equals("start")) {
                                editor.putString("fingerType", "noFingerPrint");
                                startService(new Intent(AttendanceCrashActivity.this, BGServicenormal.class));
                                showalertbox(formattedtime);
                                attendancebtn.setText("Out Time");
                                istiming = true;
                                isCheckedIn = true;
                                servuce = false;
                                editor.putBoolean("isCheckedIn", isCheckedIn);
                                editor.putBoolean("istiming", istiming);
                                editor.putBoolean("istiming", istiming);
                                editor.putString("formateddate", formattedDate);
                                editor.putString("formatedtime", formattedtime);
                                editor.putBoolean("service", servuce);
                                editor.putString("worklocation", selectwork);
                                editor.commit();

//                     getNetworkStatus();

                            } else if (string.equals("stop")) {
                                stopService(new Intent(AttendanceCrashActivity.this, BGServicenormal.class));
                                inflateAlertBox(formattedtime);
                                attendancebtn.setText("InTime");
                                isCheckedIn = false;
                                islinit = true;
                                istiming = false;
                                servuce = false;
                                editor.putBoolean("isCheckedIn", isCheckedIn);
                                editor.putBoolean("istiming", istiming);
                                editor.putBoolean("islinit", islinit);
                                editor.putBoolean("service", servuce);
                                editor.putString("formateddate", formattedDate);
                                editor.putString("formatedtime", formattedtime);
                                editor.putString("worklocation", selectwork);
                                editor.commit();
                            }

                        } else if (response.body().getResult().equals("Already Punched")) {
                            editor.putString("formateddate", formattedDate);
                            editor.commit();
                            showalert();
                        } else {
                            Toast.makeText(getApplicationContext(), "Please try again later...", Toast.LENGTH_SHORT).show();
                        }
                    }


                    @Override
                    public void onFailure(Call<AttendanceReport> call, Throwable t) {
                        loading.dismiss();
                        Toast.makeText(getApplicationContext(), "Attempt Failed. Please try again later...", Toast.LENGTH_SHORT).show();
                    }
                });
            }catch (Exception e){
                loading.dismiss();
                Toast.makeText(getApplicationContext(), "Server not responding...", Toast.LENGTH_SHORT).show();
            }
            loading.dismiss();
            return "";
        }


    }
    private void showalertbox(String formattedtime) {
        new FancyAlertDialog.Builder((Activity) AttendanceCrashActivity.this)
                .setTitle("Fumigation Services Field Force")
                .setBackgroundColor(Color.parseColor("#303F9F"))  //Don't pass R.color.colorvalue
                .setMessage("Your Current Time "+formattedtime+" is marked as your In Time Attendance")
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground(Color.parseColor("#FF4081"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("Ok")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.SIDE)
                .isCancellable(false)
                .setIcon(R.drawable.checked, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {
                        Intent i=new Intent(AttendanceCrashActivity.this, SupervisorHomeActivity.class);
                        AttendanceCrashActivity.this.startActivity(i);
                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }

    private void showalert() {
        new FancyAlertDialog.Builder((Activity) AttendanceCrashActivity.this)
                .setTitle("Fumigation Services Field Force")
                .setBackgroundColor(Color.parseColor("#303F9F"))  //Don't pass R.color.colorvalue
                .setMessage("You have already Punched Today")
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground(Color.parseColor("#FF4081"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("Ok")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.SIDE)
                .isCancellable(false)
                .setIcon(R.drawable.cancel, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {
                        Intent i=new Intent(AttendanceCrashActivity.this, SupervisorHomeActivity.class);
                        AttendanceCrashActivity.this.startActivity(i);
                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }

    private class GetGeoCodeAPIAsynchTask extends AsyncTask<Double, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String[] doInBackground(Double... latlang) {
            String response;
            try {
                String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang[0] + "," + latlang[1] + "&key=" + APIKEY;
                Log.v("URL", URL);
                response = getLatLongByURL(URL);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @SuppressLint("LongLogTag")
        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                address = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(0).getString("long_name");

                editor.putString("FromAddress",address);
                editor.putString("ToAddress",address);
                editor.commit();
                editor.apply();

                City = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(2).getString("long_name");

                String state = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(4).getString("long_name");

                Log.d(TAG, "onPostExecute: "+City);

//                String title = city + "-" + state;

//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertbox("Your Current location is "+city);

//                if (fetchType == FROMADDRESS) {
//
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.requestFocus();
//                    fromaddress = address;
//                    mPickupText.setText(address);
//
//                    editor.putString("FromAddress", fromaddress);
//                    editor.apply();
//                    editor.commit();
//
//                    Log.i("FromAddress1", address);
//                    Log.i("FromAddress2", title);
//                } else {
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.setText(address);
//                    toaddress = address;
//                    editor.putString("ToAddress", toaddress).commit();
//
//                    Log.i("ToAddress1", address);
//                    Log.i("ToAddress2", title);
//
//                }


                Log.d("Address", "" + address);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            progressBar.setVisibility(View.GONE);

        }
    }
    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }



//    private class MyBroadCastReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            servuce=true;
//            editor.putBoolean("service",servuce);
//            editor.commit();
//            String data = intent.getStringExtra("data");
//            attendancebtn.setText(data);
//            locatemat.setVisibility(View.VISIBLE);
//            locatetext.setVisibility(View.GONE);
//        }
//    }
}
