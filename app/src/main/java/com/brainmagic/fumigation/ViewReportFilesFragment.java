package com.brainmagic.fumigation;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Pix;
import com.fxn.utility.Utility;

import java.io.File;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import alert.Alertbox;
import api.models.Upload.UploadActivity;
import api.models.Upload.upload;
import api.models.UploadAudio.AudioUploadResult;
import api.models.Uploadimage.ImageUploadResult;
import api.models.VisitReport.Visit;
import api.models.VisitReport.VisitReportData;
import api.models.VisitReport.request.VisitReportReqData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import droidninja.filepicker.utils.Orientation;
import network.NetworkConnection;
import nl.changer.audiowife.AudioWife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uploadService.ProgressRequestBody;

import static android.content.Context.MODE_PRIVATE;
import static com.fxn.utility.Utility.getExifCorrectedBitmap;

public class ViewReportFilesFragment extends Fragment implements EasyPermissions.PermissionCallbacks,
        ProgressRequestBody.UploadCallbacks {
    private ImageView image;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private static final int IMAGE_PICKER = 1;
    public static final int RC_AUDIO_REC_PREM = 456;
    private static final int AUIDO_PICKER = 0;
    private ArrayList<String> photoPaths = new ArrayList<>();
    private static final int REQUEST_RECORD_AUDIO = 0;
    private int imageSize;
    private Button mSelectAudio, logsubmit;
    private View mPlayMedia;
    private View mPauseMedia;
    private SeekBar mMediaSeekBar;
    private TextView mRunTime;
    private TextView mTotalTime;
    private LinearLayout mAudio_layout;
    private Button mUploadAudio, uplaod_images;
    public String cusname1, mobile1, address1, date1, time1;
    private int i = 0;
    private static final String AUDIO_FILE_PATH =
            Environment.getExternalStorageDirectory().getPath() + "/recorded_audio.mp3";
    private File audio;
    private Context context;
    private EditText rename;
    private ProgressDialog loading;
    private String cusname, mobile, address, date, time;
    private Alertbox box;
    private String remarks;
    private  String value;
    private VisitReportReqData visitReportReqData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_view_report_files_fragment, container, false);
        context = ((VisitReportFragment) getActivity());
        box = new Alertbox(getContext());
        myshare = getActivity().getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();


        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
//        Bundle bumdle = getArguments();
//        if (bumdle != null) {
//            cusname = bumdle.getString("customername");
//            mobile = bumdle.getString("mobilenumber");
//            address = bumdle.getString("address");
//            date = bumdle.getString("date");
//            time = bumdle.getString("time");
//        }

        //     cusname=getArguments().getString("customername");
//        cusname=myshare.getString("cusname","");
//        mobile=myshare.getString("mobile","");
//        address=myshare.getString("address","");
//        date=myshare.getString("date","");
//        time=myshare.getString("time","");


        image = (ImageView) rootView.findViewById(R.id.image1);
        rename = (EditText) rootView.findViewById(R.id.rename);
        // audio player controll
        mPlayMedia = rootView.findViewById(R.id.play_btn);
        mPauseMedia = rootView.findViewById(R.id.pause_btn);
        mMediaSeekBar = (SeekBar) rootView.findViewById(R.id.media_seekbar);
        mRunTime = (TextView) rootView.findViewById(R.id.run_time);
        mTotalTime = (TextView) rootView.findViewById(R.id.total_time);
        mAudio_layout = (LinearLayout) rootView.findViewById(R.id.audio_layout);

        mSelectAudio = (Button) rootView.findViewById(R.id.select_audio);
        logsubmit = (Button) rootView.findViewById(R.id.logsubmit);
        mUploadAudio = (Button) rootView.findViewById(R.id.upload_audio);
        uplaod_images = (Button) rootView.findViewById(R.id.uplaod_images);
        loading = new ProgressDialog(context);
        loading.setMessage("Please wait...");
        loading.setTitle("Uploading");
        loading.setCancelable(false);
        loading.setIndeterminate(true);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pix.start(getActivity(), 100, 2);

            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Pix.start(ViewReportFilesFragment.this, 100, 1);
                imagepicker();
            }
        });

//        image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Pix.start(VisitReportFilesActivity.this, 100, 1);
//                ImagePicker();
//
//            }
//        });
        mSelectAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File oldAudio = new File(AUDIO_FILE_PATH);
                if (oldAudio.exists()) {
                    oldAudio.delete();
                }
                AudioPicker();
            }
        });


        uplaod_images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!photoPaths.isEmpty())
                    ChecInterNet("image");
                else
                    box.showAlertbox("Pick or capture image");
                //   Toast.makeText(context,"Pick or capture image",Toast.LENGTH_SHORT).show();
            }
        });


        mUploadAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audio = new File(AUDIO_FILE_PATH);
                if (audio.exists())
                    ChecInterNet1("audio");
                else
                    box.showAlertbox("Record a audio");
            }
        });


        logsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getAlldata();

            }
        });
        mPlayMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audio = new File(AUDIO_FILE_PATH);
                if (!audio.exists()) {
                    Toast.makeText(context, "Record an audio file before playing", Toast.LENGTH_LONG).show();
                } else {
                    AudioWife.getInstance()
                            .init(context, Uri.fromFile(audio))
                            .setPlayView(mPlayMedia)
                            .setPauseView(mPauseMedia)
                            .setSeekBar(mMediaSeekBar)
                            .setRuntimeView(mRunTime)
                            .setTotalTimeView(mTotalTime);

                    AudioWife.getInstance().addOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mp.reset();
                        }
                    });
                }
            }
        });
        photoPaths.clear();
        return rootView;

    }

    private void imagepicker() {
        FilePickerBuilder.getInstance()
                .setMaxCount(1)
                .setSelectedFiles(photoPaths)
                .setActivityTitle("Please select Images")
                .setActivityTheme(R.style.AppTheme)
                .enableCameraSupport(true)
                .enableVideoPicker(false)
                .showGifs(false)
                .showFolderView(false)
                .enableSelectAll(true)
                .enableImagePicker(true)
                .setCameraPlaceholder(R.drawable.custom_camera)
                .withOrientation(Orientation.UNSPECIFIED)
                .pickPhoto(this, IMAGE_PICKER);
    }


    public void Value(String s1, String s2, String s3, String s4, String s5) {
       cusname1 = s1;
       mobile1 = s2;
       address1 = s3;
       date1 = s4;
       time1 = s5;


    }

    private void getAlldata() {
        try {

            final ProgressDialog loading = ProgressDialog.show(context, "Fumigation", "Loading...", false, false);
            if(photoPaths.size() == 0){
                visitReportReqData=new VisitReportReqData();
                visitReportReqData.setName(cusname1);
                visitReportReqData.setMobileNo(mobile1);
                visitReportReqData.setPlaceOfVisit(address1);
                visitReportReqData.setDate(date1);
                visitReportReqData.setTime(time1);
              //  visitReportReqData.setImage(new File(photoPaths.get(0)).getName());
                visitReportReqData.setAudio(cusname1+new File(AUDIO_FILE_PATH).getName());
                visitReportReqData.setRemarks(rename.getText().toString());
            }else{
                visitReportReqData=new VisitReportReqData();
                visitReportReqData.setName(cusname1);
                visitReportReqData.setMobileNo(mobile1);
                visitReportReqData.setPlaceOfVisit(address1);
                visitReportReqData.setDate(date1);
                visitReportReqData.setTime(time1);
                 visitReportReqData.setImage(new File(photoPaths.get(0)).getName());
                visitReportReqData.setAudio(cusname1+new File(AUDIO_FILE_PATH).getName());
                visitReportReqData.setRemarks(rename.getText().toString());
            }

            APIService service = RetrofitClient.getApiService();
            Call<VisitReportData> call;
            call = service.visit(visitReportReqData);
            call.enqueue(new Callback<VisitReportData>() {
                @Override
                public void onResponse(Call<VisitReportData> call, Response<VisitReportData> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            SuccessRegister(response.body().getData());
                        }else{
                            SuccessRegister(response.body().getData());
                        }

                        // NO_CLEAR makes the notification stay when the user performs a "delete all" command
                    } else if (response.body().getResult().equals("NotSuccess")) {
                        box.showAlertbox(getString(R.string.server_error));
                    } else {
                        box.showAlertbox(getResources().getString(R.string.server_error));
                    }
                }


                @Override
                public void onFailure(Call<VisitReportData> call, Throwable t) {
                    loading.dismiss();
                    final AlertDialog alertDialogbox = new AlertDialog.Builder(
                            context).create();

                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.alertbox, null);
                    alertDialogbox.setView(dialogView);

                    TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                    Button okay = (Button) dialogView.findViewById(R.id.okay);
                    log.setText("Visit Report Successfully");
                    okay.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            // TODO Auto-generated method stub
                            Intent go = new Intent(context, SupervisorHomeActivity.class);
                            go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(go);
                            alertDialogbox.dismiss();

                        }
                    });
                    alertDialogbox.show();
                    t.printStackTrace();
                    // box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
        }

    }

//            public void getItems(String date)
//            {
//                this.date=date;
//            }

    private void SuccessRegister(Visit data) {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxlinear, null);
        alertDialogbox.setView(dialogView);

        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText("Visit Report Successfully");
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // TODO Auto-generated method stub
                Intent go = new Intent(context, SupervisorHomeActivity.class);
                go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(go);
                alertDialogbox.dismiss();

            }


        });
        alertDialogbox.show();

    }



    private void ChecInterNet(String image) {
        NetworkConnection isnet = new NetworkConnection(context);
        if (isnet.CheckInternet()) {
            UploadImages();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }

    private void ChecInterNet1(String from) {
        if (from.equals("audio"))
            UplaodAudioFiles();
        else{
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }

    }

    private void UplaodAudioFiles() {
        try {

            if (!loading.isShowing())
                loading.show();


            File audioFile = new File(AUDIO_FILE_PATH);
            //String FilterType = "audio";
            String type = URLConnection.guessContentTypeFromName(audioFile.getName());

            //  ProgressRequestBody fileBody = new ProgressRequestBody(audioFile, JobCardFeedbackActivity.this);
            RequestBody requestBody = RequestBody.create(MediaType.parse(type), audioFile);
           // RequestBody r_id = RequestBody.create(MediaType.parse("text/plain"), cusname1);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("audio", cusname1+audioFile.getName(), requestBody);
            APIService service = RetrofitClient.getApiUplaodService();
            Call<AudioUploadResult> request = service.audioimage(filePart);
            request.enqueue(new Callback<AudioUploadResult>() {
                @Override
                public void onResponse(Call<AudioUploadResult> call, Response<AudioUploadResult> response) {
                    loading.dismiss();
                    if (response.body().getMessage().equals("Success.")) {
                        box.showAlertbox("Audio Uploaded Successfully");
                        editor.putBoolean("isFilesAdded", true);
                        editor.commit();
                        editor.apply();
                    } else {
                        box.showAlertbox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<AudioUploadResult> call, Throwable t) {
                    loading.dismiss();
                    Log.v("Upload Exception", t.getMessage());
                    t.printStackTrace();
                    box.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception ex) {
            if (!loading.isShowing())
                loading.dismiss();
            Log.v("Upload Exception", ex.getMessage());
            box.showAlertbox(getString(R.string.server_error));
        }
    }

    private void UploadImages() {
        try {

            if (!loading.isShowing())
                loading.show();

            APIService service = RetrofitClient.getApiUplaodService();

            if (i >= photoPaths.size()) {
                return; //loop is finished;
            }

            File imageFile = new File(photoPaths.get(i));

            Uri uri = Uri.fromFile(imageFile);

            String type = URLConnection.guessContentTypeFromName(imageFile.getName());
            //ProgressRequestBody fileBody = new ProgressRequestBody(imageFile, JobCardFeedbackActivity.this);
            RequestBody requestBody = RequestBody.create(MediaType.parse(type), imageFile);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", imageFile.getName(), requestBody);

            final Call<ImageUploadResult> request = service.uploadImage(filePart);
            request.enqueue(new Callback<ImageUploadResult>() {
                @Override
                public void onResponse(Call<ImageUploadResult> call, Response<ImageUploadResult> response) {
                    i++;
                    UploadImages();
                    if (response.body().getMessage().equals("Success.")) {
                        if (i == photoPaths.size()) {
                            loading.dismiss();
                            editor.putBoolean("isFilesAdded", true);
                            editor.commit();
                            editor.apply();
                            //  image1.setImageResource(R.drawable.image_upload);
                            //   image2.setImageResource(R.drawable.image_upload);
                            box.showAlertbox("Images Uploaded Successfully");
                        }
                    } else {
                        box.showAlertbox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<ImageUploadResult> call, Throwable t) {
                    Log.v("Upload Exception", t.getMessage());
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getString(R.string.server_error));
                }

            });

        } catch (Exception ex) {
            if (loading.isShowing())
                loading.dismiss();
            Log.v("Exception", ex.getMessage());
            box.showAlertbox(getString(R.string.server_error));
        }
    }

    public void AudioPicker() {

        if (EasyPermissions.hasPermissions(context, Manifest.permission.RECORD_AUDIO)) {
            OpenAudioRecorder();
        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_audio_recorder), RC_AUDIO_REC_PREM, Manifest.permission.RECORD_AUDIO);
        }

    }

    private void OpenAudioRecorder() {
        AndroidAudioRecorder.with(getActivity())
                // Required
                .setFilePath(AUDIO_FILE_PATH)
                .setColor(ContextCompat.getColor(context, R.color.red))
                .setRequestCode(REQUEST_RECORD_AUDIO)
                // Optional
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.STEREO)
                .setSampleRate(AudioSampleRate.HZ_48000)
                .setAutoStart(false)
                .setKeepDisplayOn(true)

                // Start recording
                .record();
        audio = new File(AUDIO_FILE_PATH);
        mAudio_layout.setVisibility(View.VISIBLE);
    }

    private void ImagePicker() {
        FilePickerBuilder.getInstance()
                .setMaxCount(1)
                .setSelectedFiles(photoPaths)
                .setActivityTitle("Please select Images")
                .setActivityTheme(R.style.FilePickerTheme)
                .enableCameraSupport(true)
                .enableVideoPicker(false)
                .showGifs(false)
                .showFolderView(false)
                .enableSelectAll(true)
                .enableImagePicker(true)
                .setCameraPlaceholder(R.drawable.custom_camera)
                .withOrientation(Orientation.UNSPECIFIED)
                .pickPhoto(this, IMAGE_PICKER);

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case IMAGE_PICKER:
                if (resultCode == Activity.RESULT_OK) {
                    photoPaths = new ArrayList<>();
                    photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                    SetImages(photoPaths);
                }
                break;
            case AUIDO_PICKER:
                if (resultCode == Activity.RESULT_OK) {
                    SetAudio();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
        }
//        switch (requestCode) {
//            case IMAGE_PICKER:
//                if (resultCode == Activity.RESULT_OK && data != null) {
//                    photoPaths = new ArrayList<>();
//                    photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
//                    SetImages(photoPaths);
//                }
//                break;
//            case AUIDO_PICKER:
//                if (resultCode == Activity.RESULT_OK) {
//                    SetAudio();
//                }
//        }


    private void SetAudio() {
        audio = new File(AUDIO_FILE_PATH);
        if (!audio.exists()) {
            Toast.makeText(context, "Record an audio file before playing", Toast.LENGTH_LONG).show();
        } else {
            AudioWife.getInstance()
                    .init(context, Uri.fromFile(audio))
                    .setPlayView(mPlayMedia)
                    .setPauseView(mPauseMedia)
                    .setSeekBar(mMediaSeekBar)
                    .setRuntimeView(mRunTime)
                    .setTotalTimeView(mTotalTime);

            AudioWife.getInstance().addOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.reset();
                }
            });
        }
    }

    private void SetImages(ArrayList<String> photoPaths) {
        if (!photoPaths.isEmpty()) {
            if (photoPaths.size() == 1) {
                Glide.with(context)
                        .load(new File(photoPaths.get(0)))
                        .apply(RequestOptions.centerCropTransform()
                                .dontAnimate()
                                .override(imageSize, imageSize)
                                .placeholder(droidninja.filepicker.R.drawable.image_placeholder))
                        .thumbnail(0.5f)
                        .into(image);

                image.setImageResource(R.drawable.ic_add_a_photo_black_24dp);


            }
        } else {
            image.setImageResource(R.drawable.ic_add_a_photo_black_24dp);

        }
    }
}
