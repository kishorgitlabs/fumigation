package com.brainmagic.fumigation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.List;

import adapter.ViewMaterialDataReqAdapter;
import adapter.ViewMaterialDataReqAdapterapproved;
import alert.Alertbox;
import api.models.viewmaterialreq.ViewMaterialDataReq;
import api.models.viewmaterialreq.ViewMaterialReq;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import logout.Logout;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewMaterialObjectActivity extends AppCompatActivity {
    private ListView listView;
    private NetworkConnection networkConnection;
    private Alertbox box1 = new Alertbox(this);
    private ImageView menu, back;
    private String status,tittle;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String requestno,approved;
    private List<ViewMaterialReq>data;
    private ViewMaterialDataReqAdapter viewMaterialDataReqAdapter;
    private ViewMaterialDataReqAdapterapproved viewMaterialDataReqAdapter1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_material_object);
        listView=(ListView)findViewById(R.id.list_materialproduct);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        menu = (ImageView) findViewById(R.id.menu);
        back = (ImageView) findViewById(R.id.back);
        requestno=getIntent().getStringExtra("requestno");
      //  approved=getIntent().getStringExtra("tittle1");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ViewMaterialObjectActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(ViewMaterialObjectActivity.this).log_out();
                                return true;
                            case R.id.changepassword:
                                Intent i2 = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;

                            case R.id.Materialrequest:
                                AlertMaterial();
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menubranchviewmaterial);
                popupMenu.show();
                ;
            }
        });

        checkInternet();

    }

    private void checkInternet() {
        networkConnection=new NetworkConnection(ViewMaterialObjectActivity.this);
        if(networkConnection.CheckInternet()){
            viewmaterial();
        }else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }

    private void viewmaterial() {
        try {
            final ProgressDialog loading = ProgressDialog.show(ViewMaterialObjectActivity.this, "Fumigation", "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<ViewMaterialDataReq> call = service.viewmaterialreq(requestno);
            call.enqueue(new Callback<ViewMaterialDataReq>() {
                @Override
                public void onResponse(Call<ViewMaterialDataReq> call, Response<ViewMaterialDataReq> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data = (List<ViewMaterialReq>) response.body().getData();
                            if(data.size() == 0){
                                final AlertDialog alertDialogbox = new AlertDialog.Builder(
                                        ViewMaterialObjectActivity.this).create();

                                LayoutInflater inflater = getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.alertboxlinear, null);
                                alertDialogbox.setView(dialogView);

                                TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                                Button okay = (Button) dialogView.findViewById(R.id.okay);
                                log.setText("No Material");
                                okay.setOnClickListener(new View.OnClickListener() {

                                    @Override
                                    public void onClick(View arg0) {
                                        // TODO Auto-generated method stub
                                        Intent go = new Intent(ViewMaterialObjectActivity.this, SupervisorHomeActivity.class);
                                        go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(go);
                                        alertDialogbox.dismiss();
                                        finish();
                                    }
                                });
                                alertDialogbox.show();
                            }else {
                                viewMaterialDataReqAdapter=new ViewMaterialDataReqAdapter(ViewMaterialObjectActivity.this,data);
                                listView.setAdapter(viewMaterialDataReqAdapter);
                            }


                        } else if (response.body().getResult().equals("NotSuccess")) {
                            final AlertDialog alertDialogbox = new AlertDialog.Builder(
                                    ViewMaterialObjectActivity.this).create();

                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.alertboxlinear, null);
                            alertDialogbox.setView(dialogView);

                            TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                            Button okay = (Button) dialogView.findViewById(R.id.okay);
                            log.setText("No Material");
                            okay.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View arg0) {
                                    // TODO Auto-generated method stub
                                    Intent go = new Intent(ViewMaterialObjectActivity.this, SupervisorHomeActivity.class);
                                    go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(go);
                                    alertDialogbox.dismiss();
                                    finish();
                                }
                            });
                            alertDialogbox.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                    }


                @Override
                public void onFailure(Call<ViewMaterialDataReq> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AlertMaterial() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                ViewMaterialObjectActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxmaterial, null);
        alertDialogbox.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(true);
        alertDialogbox.setView(dialogView);
        Button requestforbranch = (Button) dialogView.findViewById(R.id.textView1);
        Button requestforstore = (Button) dialogView.findViewById(R.id.textView2);
        requestforbranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), MaterialRequestActivity.class).putExtra("Tiltle","Branch");

                startActivity(i);
                alertDialogbox.dismiss();
            }
        });

        requestforstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), MaterialRequestActivity.class).putExtra("Tiltle","Store");
                startActivity(i);
                alertDialogbox.dismiss();
            }
        });
        alertDialogbox.show();
    }
}
