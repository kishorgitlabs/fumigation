package com.brainmagic.fumigation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.zxing.integration.android.IntentIntegrator;

import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentResult;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import adapter.ViewAllNameAdapter;
import alert.Alertbox;
import api.models.Upload.UploadActivity;
import api.models.UploadAudio.AudioUploadResult;
import api.models.Uploadimage.ImageUploadResult;
import api.models.alertnames.GetAllName;
import api.models.alertnames.GetAllNameData;
import api.models.jobcardera.request.ChemicalsDetails;
import api.models.jobcardera.request.Chemicalser;
import api.models.workreport1.request.TW;
import api.models.workreport1.request.WorkReportDataValues;
import api.models.workreport1.resposnse.Data;
import api.models.workreport1.resposnse.WorkReportresponse;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import droidninja.filepicker.utils.Orientation;
import logout.Logout;
import network.NetworkConnection;
import nl.changer.audiowife.AudioWife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import persistance.ServerConnection;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;
import uploadService.ProgressRequestBody;

public class JobCardFeedbackActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks,
        ProgressRequestBody.UploadCallbacks {
    //initialise the Data........
    private String containernumber;
    private IntentIntegrator qrScan;
    private EditText edcontainer;
    private MaterialSpinner edspinner, matstatus, chemicalssp;
    private ImageView image1, image2;
    private static final int IMAGE_PICKER = 1;
    public static final int RC_AUDIO_REC_PREM = 456;
    private static final int REQUEST_RECORD_AUDIO = 0;
    private static final String AUDIO_FILE_PATH =
            Environment.getExternalStorageDirectory().getPath() + "/recorded_audio.mp3";
    private TextView mRunTime;
    private TextView mTotalTime;
    private Button mSelectAudio;
    private int i = 0;
    private Button uploadimages;
    private View mPlayMedia;
    private View mPauseMedia;
    private SeekBar mMediaSeekBar;
    private ProgressDialog loading;
    private File audio;
    private LinearLayout mAudio_layout;
    private static final int AUIDO_PICKER = 0;
    private ArrayList<String> workreport, chemicallist1, chemicallist2;
    private ArrayList<String> status;
    private Button mUploadImages, browse,workreport1;
    //initialize the local database...
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private LinearLayout parentLayout;
    private int hint = 0;
    private EditText chemical, quatity;
    Button Add;
    Button remove;
    private int i1 = 0;
    private EditText qty, remarks, qty1, quantityt;
    private Button Addchemicals, scanning, scanning2;
    private ScrollView scrollView1;
    private Toasts toasts = new Toasts(this);
    LinearLayout container;
    private ImagePicker imagePicker;
    private boolean IsImageUpload = false;
    private int imageSize;
    private File mImageFile;
    private String mImageName = "";
    private UCrop uCrop;
    private Alertbox box = new Alertbox(this);
    private ArrayList<String> photoPaths = new ArrayList<>();
    private ArrayList<File> photoPathlist = new ArrayList<>();
    private ArrayList<String> chemcals;
    private Button mUploadAudio, submit;
    private String selectstatus, selectreport, remarks1, chemicallist, selectchemicals, selectchemicals1,
            Quantitylist, jobcard, serialnumber;
    private Integer cusid;
    private ChemicalsDetails chemicalmodel = new ChemicalsDetails();
    private MaterialSpinner chemicals;
    private LinearLayout chemicals1, addllinear, imagelinear, audiolinear, status1, remarkslinear, scanninglin;
    private Button scanning1;
    private EditText serialnumbers;
    private static final String TAG = "JobCardFeedbackActivity";
    private ImageView menu, back;
    private String image11, image22;
    private Boolean imageboolena1, imageboolean2;
    EditText quantityedit;
    EditText serialno;
    MaterialSpinner chemicalsedit;
    List<String> addchemicals = new ArrayList();
    private int i2 = 1;
    private int i3 = 1;
    private Integer quantity2, quantitydynamic;
    public boolean addlinear = false;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private Bitmap mImageBitmap;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private String mCurrentPhotoPath;
    private static final int CAMERA_REQUEST = 1888;
    private static final int REQUEST_CAPTURE_IMAGE = 100;
    Uri file;
    private Boolean image111 = false;
    private Boolean image222 = false;
    private Uri filePath;
    private Bitmap mphoto;
    String path;
    private Uri mCropImageUri;
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    private List<String>allnames;
    private ViewAllNameAdapter viewAllNameAdapter;
    private ListView listView;
    private Button submit1;
    private String branchname,bramchcode;
    private List<TW>selectlist;
    private HashSet<TW> selectlist1;
    private WorkReportDataValues workReportDataValues;
    private List<GetAllName>data;
    private List<String> productnames;
    private  MaterialSpinner chemicals_spin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_card_feedback);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        //Add = (Button) findViewById(R.id.add1);
        edcontainer = (EditText) findViewById(R.id.edcont);
        quantityt = (EditText) findViewById(R.id.quantityt);
        edspinner = (MaterialSpinner) findViewById(R.id.edspinner);
        matstatus = (MaterialSpinner) findViewById(R.id.matstatus);
        image1 = (ImageView) findViewById(R.id.image1);
        image2 = (ImageView) findViewById(R.id.fumigation_image2);
        chemicals1 = (LinearLayout) findViewById(R.id.chemicallinear);
        addllinear = (LinearLayout) findViewById(R.id.addllinear);
        imagelinear = (LinearLayout) findViewById(R.id.imagelinear);
        audiolinear = (LinearLayout) findViewById(R.id.audiolinear);
        status1 = (LinearLayout) findViewById(R.id.status1);
        scanninglin = (LinearLayout) findViewById(R.id.scanninglin);
        remarkslinear = (LinearLayout) findViewById(R.id.remarkslinear);
        Addchemicals = (Button) findViewById(R.id.addchemicals);
        serialnumbers = (EditText) findViewById(R.id.serialnumber);
        chemicalssp = (MaterialSpinner) findViewById(R.id.chemicalssp);
        scanning = (Button) findViewById(R.id.scanning);
        scanning2 = (Button) findViewById(R.id.scanning1);
        //workreport1 = (Button) findViewById(R.id.workreport);
        scrollView1 = (ScrollView) findViewById(R.id.scrollView1);
        uploadimages = (Button) findViewById(R.id.uplaod_images);
        mPlayMedia = findViewById(R.id.play_btn);
        mPauseMedia = findViewById(R.id.pause_btn);
        back = (ImageView) findViewById(R.id.back);
        remarks = (EditText) findViewById(R.id.remarks);
        mMediaSeekBar = (SeekBar) findViewById(R.id.media_seekbar);
        mRunTime = (TextView) findViewById(R.id.run_time);
        mTotalTime = (TextView) findViewById(R.id.total_time);
        mAudio_layout = (LinearLayout) findViewById(R.id.audio_layout);
        submit = (Button) findViewById(R.id.logsubmit);
        mSelectAudio = (Button) findViewById(R.id.select_audio);
        mUploadAudio = (Button) findViewById(R.id.upload_audio);
        menu = (ImageView) findViewById(R.id.menu);
        allnames=new ArrayList<>();
        productnames = new ArrayList<>();
        containernumber = getIntent().getStringExtra("containernumber");
        edcontainer.setText(containernumber);
        qrScan = new IntentIntegrator(this);
        chemicallist = myshare.getString("chemicals", "");
        Quantitylist = myshare.getString("quantity", "");
        cusid = myshare.getInt("id", 0);
        jobcard = getIntent().getStringExtra("jobcardod");
        chemcals = new ArrayList<>();
        status = new ArrayList<>();
        status.add("*Select Status");
        status.add("Pending");
        status.add("Inprogress");
        status.add("Closed");
        matstatus.setItems(status);
        branchname=myshare.getString("branchname","");
        bramchcode=myshare.getString("branchcode","");
        matstatus.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectstatus = item.toString();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        workreport = new ArrayList<>();
        workreport.add("*Select WorkReport");
        workreport.add("ND");
        workreport.add("FUMI");
        workreport.add("SPARYING");
        edspinner.setItems(workreport);

        edspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectreport = item.toString();
                switch (selectreport) {
                    case "ND":
                        chemicals1.setVisibility(View.VISIBLE);
                        addllinear.setVisibility(View.VISIBLE);
                        imagelinear.setVisibility(View.VISIBLE);
                        audiolinear.setVisibility(View.VISIBLE);
                        status1.setVisibility(View.VISIBLE);
                        remarkslinear.setVisibility(View.VISIBLE);
                        scanninglin.setVisibility(View.GONE);
                        serialnumbers.setVisibility(View.GONE);
                        break;
                    case "FUMI":
                        chemicals1.setVisibility(View.VISIBLE);
                        addllinear.setVisibility(View.VISIBLE);
                        imagelinear.setVisibility(View.VISIBLE);
                        audiolinear.setVisibility(View.VISIBLE);
                        status1.setVisibility(View.VISIBLE);
                        remarkslinear.setVisibility(View.VISIBLE);
                        serialnumbers.setVisibility(View.VISIBLE);
                        scanninglin.setVisibility(View.VISIBLE);
                        break;
                    default:
                        chemicals1.setVisibility(View.VISIBLE);
                        addllinear.setVisibility(View.VISIBLE);
                        imagelinear.setVisibility(View.VISIBLE);
                        serialnumbers.setVisibility(View.VISIBLE);
                        audiolinear.setVisibility(View.VISIBLE);
                        status1.setVisibility(View.VISIBLE);
                        remarkslinear.setVisibility(View.VISIBLE);
                }
            }
        });
//        chemicallist1 = new ArrayList<>();
//        chemicallist1.add("*Select Chemicals");
//        chemicallist1.add("Phostoxin");
//        chemicallist1.add("Talunex Tablets");
//        chemicallist1.add("Topex Applicator");
//        chemicalssp.setItems(chemicallist1);
        checkInternet1();

        chemicalssp.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectchemicals = item.toString();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(JobCardFeedbackActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(JobCardFeedbackActivity.this).log_out();
                                return true;
                            case R.id.attendance:
                                Intent i = new Intent(getApplicationContext(), AttendanceViewNewActivity.class);
                                startActivity(i);
                                return true;
                            case R.id.home:
                                Intent i5 = new Intent(getApplicationContext(), SupervisorHomeActivity.class);
                                startActivity(i5);
                                return true;
                            case R.id.viewvisit:
                                Intent i6 = new Intent(getApplicationContext(), ViewReportActivity.class);
                                startActivity(i6);
                                return true;
                            case R.id.changepassword:
                                Intent i2 = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;
                            case R.id.visitreport:
                                Intent i4 = new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i4);
                                return true;

                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menujobcard);
                popupMenu.show();
                ;
            }
        });
        scanning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quantityedit.getText().toString().equals("")) {
                    toasts.ShowErrorToast("Enter Quantity");
                } else {
                    quantitydynamic = Integer.valueOf(quantityedit.getText().toString());
                    if (i3 <= quantitydynamic) {
                        qrScan.setBeepEnabled(true);
                        qrScan.setOrientationLocked(false);
                        qrScan.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                        qrScan.setCaptureActivity(AnyOrientationCaptureActivity.class);
                        qrScan.initiateScan();
                    } else {
                        toasts.ShowErrorToast("Quantity Reached");
                    }
                }

            }

        });
        scanning2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (quantityt.getText().toString().equals("")) {
                    toasts.ShowErrorToast("Enter Quantity");
                } else {
                    quantity2 = Integer.valueOf(quantityt.getText().toString());
                    if (i2 <= quantity2) {

                        qrScan.setBeepEnabled(true);
                        qrScan.setOrientationLocked(false);
                        qrScan.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                        qrScan.setCaptureActivity(AnyOrientationCaptureActivity.class);
                        qrScan.initiateScan();
                    } else {
                        toasts.ShowErrorToast("Quantity Reached");
                    }
                }
            }

        });

        loading = new ProgressDialog(JobCardFeedbackActivity.this);
        loading.setMessage("Please wait...");
        loading.setTitle("Uploading");
        loading.setCancelable(false);
        loading.setIndeterminate(true);
        image1.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
//                imageboolena1 = true;
//                imageboolean2 = false;
//                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                imagepicker();
//                onSelectImageClick(v);
            }
        });
        //Pix.start(JobCardFeedbackActivity.this, 100, 2);



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectreport != "ND") {
                    if (edcontainer.getText().toString().equals("*Select WorkReport")) {
                        toasts.ShowErrorToast("Enter Container Number");
                    } else if (edspinner.getText().toString().equals("*Select WorkReport")) {
                        toasts.ShowErrorToast("Enter Report");
                    } else if (chemicalssp.getText().toString().equals("*Select Chemicals")) {
                        toasts.ShowErrorToast("Enter Chemicals");
                    } else if (quantityt.getText().toString().equals("")) {
                        toasts.ShowErrorToast("Enter Quantity");
                    } else if (serialnumbers.getText().toString().equals("")) {
                        toasts.ShowErrorToast("Enter SerialNumber");
                    } else if (matstatus.getText().toString().equals("*Select Status")) {
                        toasts.ShowErrorToast("*Enter Status");
                    } else if (remarks.getText().toString().equals("")) {
                        toasts.ShowErrorToast("Enter Remarks");
                    } else if (remarks.getText().toString().equals("")) {
                        toasts.ShowErrorToast("Enter Remarks");
                    } else {
                        checkInternet();
                    }
                } else {
                    if (edcontainer.getText().toString().equals("")) {
                        toasts.ShowErrorToast("Enter Container Number");
                    } else if (edspinner.getText().toString().equals("*Select WorkReport")) {
                        toasts.ShowErrorToast("Enter Work Report");
                    } else if (chemicalssp.getText().toString().equals("*Select Chemicals")) {
                        toasts.ShowErrorToast("Enter Chemicals");
                    } else if (quantityt.getText().toString().equals("")) {
                        toasts.ShowErrorToast("Enter Quantity");
                    } else if (matstatus.getText().toString().equals("*Select Status")) {
                        toasts.ShowErrorToast("Enter Status");
                    } else if (remarks.getText().toString().equals("")) {
                        toasts.ShowErrorToast("Enter Remarks");
                    } else {
                        checkInternet();
                    }
                }

            }
        });

        Addchemicals.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (selectreport != null) {

                   // quantityt.setEnabled(false);
                    addlinear = true;
                    final LinearLayout linearLayoutForm = (LinearLayout) findViewById(R.id.linearLayoutForm);
                    i3 = 1;

                    scrollView1.setVisibility(View.VISIBLE);

               /* scanning.setVisibility(View.GONE);
                scanning2.setVisibility(View.VISIBLE);*/
                    final LinearLayout newView = (LinearLayout) getLayoutInflater().inflate(R.layout.chemicals, null);
                    newView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    ImageButton btnRemove = (ImageButton) newView.findViewById(R.id.btnRemove);
                    quantityedit = (EditText) newView.findViewById(R.id.quantityedit);
                    serialno = (EditText) newView.findViewById(R.id.serialno);
                    chemicals_spin = (MaterialSpinner) newView.findViewById(R.id.chemicalsedit);
                    scanning2.setVisibility(View.GONE);
                    scanning.setVisibility(View.VISIBLE);
                    switch (selectreport) {
                        case "ND":
                            serialno.setVisibility(View.GONE);
                            break;
                        default:
                            serialno.setVisibility(View.VISIBLE);
                            break;
                    }

                    chemicallist1 = new ArrayList<>();
                    chemicallist1.add("*Select Chemicals");
                    chemicallist1.add("Phostoxin");
                    chemicallist1.add("Talunex Tablets");
                    chemicallist1.add("Topex Applicator");
                    chemicals_spin.setItems(chemicallist1);


                    chemicals_spin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                            selectchemicals1 = item.toString();
                            if (!selectchemicals1.equals("*Select Chemicals"))
                                addchemicals.add(item.toString());

                        }
                    });
                    new  AddChemicals().execute();
                    btnRemove.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            scanning.setVisibility(View.GONE);
                            scanning2.setVisibility(View.VISIBLE);
                            linearLayoutForm.removeView(newView);
                        }
                    });
                    linearLayoutForm.addView(newView);
                } else {
                    toasts.ShowErrorToast("Enter Work Report");
                }
            }


        });

//        Add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (selectreport == "ND") {
//                    if (chemicalssp.equals("")) {
//                        toasts.ShowErrorToast("Enter Chemical");
//                    } else if (quantityt.getText().toString().equals("")) {
//                        toasts.ShowErrorToast("Enter Quantitiy");
//                    } else {
//                        final ProgressDialog loading = ProgressDialog.show(JobCardFeedbackActivity.this, "Fumigation", "Loading...", false, false);
//                        LinearLayout scrollViewlinerLayout = (LinearLayout) findViewById(R.id.linearLayoutForm);
//                        List<Chemicalser> msg = new ArrayList<>();
//                        Chemicalser chemicals = new Chemicalser();
//                        //String chemicalst=chemicalssp.getText().toString();
//                        String chemicalst = selectchemicals;
//                        String serial = serialnumbers.getText().toString();
//                        String qtysty = quantityt.getText().toString();
//                        chemicals.setChemicals(chemicalst);
//                        chemicals.setQuatity(qtysty);
//                        chemicals.setSerialNumber(serial);
//                        msg.add(chemicals);
//                        for (int i = 0; i < scrollViewlinerLayout.getChildCount(); i++) {
//                            Chemicalser chemicals1 = new Chemicalser();
//                            EditText quantityedit, serialno;
//                            MaterialSpinner chemicalsedit;
//                            LinearLayout innerLayout = (LinearLayout) scrollViewlinerLayout.getChildAt(i);
//                            chemicalsedit = (MaterialSpinner) innerLayout.findViewById(R.id.chemicalsedit);
//                            quantityedit = (EditText) innerLayout.findViewById(R.id.quantityedit);
//                            serialno = (EditText) innerLayout.findViewById(R.id.serialno);
//                            String chemicals11 = selectchemicals1;
//                            String quantity1 = quantityedit.getText().toString();
//                            String serial1 = serialno.getText().toString();
//                            chemicals1.setChemicals(chemicals11);
//                            chemicals1.setQuatity(quantity1);
//                            chemicals1.setSerialNumber(serial1);
//                            msg.add(chemicals1);
//                        }
//
//                        chemicalmodel.setChemicals1(msg);
//                        box.showAlertbox("Chemicals Added Successfully");
//                        loading.dismiss();
//                    }
//
//
//                } else {
//                    if (chemicalssp.equals("")) {
//                        toasts.ShowErrorToast("Enter Chemical");
//                    } else if (quantityt.getText().toString().equals("")) {
//                        toasts.ShowErrorToast("Enter Quantitiy");
//                    } else if (serialnumbers.getText().toString().equals("")) {
//                        toasts.ShowErrorToast("Enter Serial Number");
//                    } else {
//                        final ProgressDialog loading = ProgressDialog.show(JobCardFeedbackActivity.this, "Fumigation", "Loading...", false, false);
//                        LinearLayout scrollViewlinerLayout = (LinearLayout) findViewById(R.id.linearLayoutForm);
//                        List<Chemicalser> msg = new ArrayList<>();
//                        Chemicalser chemicals = new Chemicalser();
//                        //String chemicalst=chemicalssp.getText().toString();
//                        String chemicalst = selectchemicals;
//                        String serial = serialnumbers.getText().toString();
//                        String qtysty = quantityt.getText().toString();
//                        chemicals.setChemicals(chemicalst);
//                        chemicals.setQuatity(qtysty);
//                        chemicals.setSerialNumber(serial);
//                        msg.add(chemicals);
//                        for (int i = 0; i < scrollViewlinerLayout.getChildCount(); i++) {
//                            Chemicalser chemicals1 = new Chemicalser();
//                            EditText quantityedit, serialno;
//                            MaterialSpinner chemicalsedit;
//                            LinearLayout innerLayout = (LinearLayout) scrollViewlinerLayout.getChildAt(i);
//                            chemicalsedit = (MaterialSpinner) innerLayout.findViewById(R.id.chemicalsedit);
//                            quantityedit = (EditText) innerLayout.findViewById(R.id.quantityedit);
//                            serialno = (EditText) innerLayout.findViewById(R.id.serialno);
//                            String chemicals11 = selectchemicals1;
//                            String quantity1 = quantityedit.getText().toString();
//                            String serial1 = serialno.getText().toString();
//                            chemicals1.setChemicals(chemicals11);
//                            chemicals1.setQuatity(quantity1);
//                            chemicals1.setSerialNumber(serial1);
//                            msg.add(chemicals1);
//                        }
//
//                        chemicalmodel.setChemicals1(msg);
//                        box.showAlertbox("Chemicals Added Successfully");
//                        loading.dismiss();
//                    }
//
//                }
//            }
//
//        });

        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageboolean2 = true;
                imageboolena1 = false;

//                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                 imagepicker();
                // Pix.start(JobCardFeedbackActivity.this, 100, 2);


            }
        });
        mSelectAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File oldAudio = new File(AUDIO_FILE_PATH);
                if (oldAudio.exists()) {
                    oldAudio.delete();
                }
                AudioPicker();
            }
        });
        mPlayMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audio = new File(AUDIO_FILE_PATH);
                if (!audio.exists()) {
                    Toast.makeText(JobCardFeedbackActivity.this, "Record an audio file before playing", Toast.LENGTH_LONG).show();
                } else {
                    AudioWife.getInstance()
                            .init(JobCardFeedbackActivity.this, Uri.fromFile(audio))
                            .setPlayView(mPlayMedia)
                            .setPauseView(mPauseMedia)
                            .setSeekBar(mMediaSeekBar)
                            .setRuntimeView(mRunTime)
                            .setTotalTimeView(mTotalTime);

                    AudioWife.getInstance().addOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mp.reset();
                        }
                    });

                }


            }
        });
        mUploadAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audio = new File(AUDIO_FILE_PATH);
                if (audio.exists())
                    ChecInterNet("audio");
                else
                    box.showAlertbox("Record a audio");
            }
        });

        uploadimages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (photoPaths.size() != 0)
                    ChecInterNet("image");
                else
                    box.showAlertbox("Pick or capture image");
            }

            private void ChecInterNet(String from) {
                NetworkConnection isnet = new NetworkConnection(JobCardFeedbackActivity.this);
                if (isnet.CheckInternet()) {
                    UploadImages();
                } else {
                    box.showAlertbox(getResources().getString(R.string.no_internet));
                }
            }
        });

    }

    private void checkInternet1() {
        NetworkConnection isnet = new NetworkConnection(JobCardFeedbackActivity.this);
        if (isnet.CheckInternet()) {
           new  AddChemicals().execute();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }

    }

    private void onSelectImageClick(View v) {
        CropImage.startPickImageActivity(this);
    }


    private void imagepicker() {
        FilePickerBuilder.getInstance()
                .setMaxCount(2)
//                .setSelectedFiles(photoPaths)
                .setActivityTitle("Please select Images")
                .setActivityTheme(R.style.AppTheme)
                .enableCameraSupport(true)
                .enableVideoPicker(false)
                .showGifs(false)
                .showFolderView(false)
                .enableSelectAll(true)
                .enableImagePicker(true)
                .setCameraPlaceholder(R.drawable.custom_camera)
                .withOrientation(Orientation.UNSPECIFIED)
                .pickPhoto(this, IMAGE_PICKER);
    }


    private void checkInternet() {
        NetworkConnection isnet = new NetworkConnection(JobCardFeedbackActivity.this);
        if (isnet.CheckInternet()) {
            Uploadvalues();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }


    private void Uploadvalues() {
        try {
            boolean bool = false;
            if (photoPaths.size() == 0) {
                remarks1 = remarks.getText().toString();
                chemicalmodel.setId(jobcard);
                chemicalmodel.setRemarks(remarks1);
                chemicalmodel.setStatus(selectstatus);
                chemicalmodel.setWorkreport(selectreport);
                if (AUDIO_FILE_PATH != null) {
                    chemicalmodel.setAudio(jobcard + containernumber + new File(AUDIO_FILE_PATH).getName());
                }
                chemicalmodel.setContainerNo(containernumber);
                if (selectreport.equals("ND")) {
                    LinearLayout scrollViewlinerLayout = (LinearLayout) findViewById(R.id.linearLayoutForm);
                    List<Chemicalser> msg = new ArrayList<>();
                    Chemicalser chemicals = new Chemicalser();
                    //String chemicalst=chemicalssp.getText().toString();
                    String chemicalst = selectchemicals;
                    String serial = serialnumbers.getText().toString();
                    String qtysty = quantityt.getText().toString();
                    chemicals.setChemicals(chemicalst);
                    chemicals.setQuatity(qtysty);
                    chemicals.setSerialNumber(serial);
                    msg.add(chemicals);
                    for (int i = 0; i < scrollViewlinerLayout.getChildCount(); i++) {
                        Chemicalser chemicals1 = new Chemicalser();
                        EditText quantityedit;
                        EditText serialno;
                        MaterialSpinner chemicalsedit;
                        LinearLayout innerLayout = (LinearLayout) scrollViewlinerLayout.getChildAt(i);
                        chemicalsedit = (MaterialSpinner) innerLayout.findViewById(R.id.chemicalsedit);
                        quantityedit = (EditText) innerLayout.findViewById(R.id.quantityedit);
                        serialno = (EditText) innerLayout.findViewById(R.id.serialno);
                        String chemicals11 = addchemicals.get(i);
                        String quantity1 = quantityedit.getText().toString();
                        String serial1 = serialno.getText().toString();

                        if (chemicals11.equals("*Select Chemicals")) {
                            toasts.ShowErrorToast("Enter chemical");
                            bool = true;
                            break;
                        } else if (quantity1.equals("")) {
                            toasts.ShowErrorToast("Enter quantity");
                            bool = true;
                            break;
                        }

                        chemicals1.setChemicals(chemicals11);
                        chemicals1.setQuatity(quantity1);
                        chemicals1.setSerialNumber(serial1);
                        msg.add(chemicals1);

                    }
                    chemicalmodel.setChemicals1(msg);

                } else {
                    LinearLayout scrollViewlinerLayout = (LinearLayout) findViewById(R.id.linearLayoutForm);
                    List<Chemicalser> msg = new ArrayList<>();
                    Chemicalser chemicals = new Chemicalser();
                    //String chemicalst=chemicalssp.getText().toString();
                    String chemicalst = selectchemicals;
                    String serial = serialnumbers.getText().toString();
                    String qtysty = quantityt.getText().toString();
                    chemicals.setChemicals(chemicalst);
                    chemicals.setQuatity(qtysty);
                    chemicals.setSerialNumber(serial);
                    msg.add(chemicals);
                    for (int i = 0; i < scrollViewlinerLayout.getChildCount(); i++) {
                        Chemicalser chemicals1 = new Chemicalser();
                        EditText quantityedit;
                        EditText serialno;
                        MaterialSpinner chemicalsedit;
                        LinearLayout innerLayout = (LinearLayout) scrollViewlinerLayout.getChildAt(i);
                        chemicalsedit = (MaterialSpinner) innerLayout.findViewById(R.id.chemicalsedit);
                        quantityedit = (EditText) innerLayout.findViewById(R.id.quantityedit);
                        serialno = (EditText) innerLayout.findViewById(R.id.serialno);
                        String chemicals11 = addchemicals.get(i);
                        String quantity1 = quantityedit.getText().toString();
                        String serial1 = serialno.getText().toString();
                        if (chemicals11.equals("*Select Chemicals")) {
                            toasts.ShowErrorToast("Enter chemical");
                            bool = true;
                            break;
                        } else if (quantity1.equals("")) {
                            toasts.ShowErrorToast("Enter quantity");
                            bool = true;
                            break;
                        } else if (serial1.equals("")) {
                            toasts.ShowErrorToast("Enter Serial Number");
                            bool = true;
                            break;
                        }
                        chemicals1.setChemicals(chemicals11);
                        chemicals1.setQuatity(quantity1);
                        chemicals1.setSerialNumber(serial1);
                        msg.add(chemicals1);

                    }
                    chemicalmodel.setChemicals1(msg);

                }


            } else {
                remarks1 = remarks.getText().toString();
                chemicalmodel.setId(jobcard);
                chemicalmodel.setRemarks(remarks1);
                if (i != 1 || i > 0) {
                    if (i == 1) {
                        chemicalmodel.setImage1(new File(String.valueOf(photoPaths.get(0))).getName());
                    } else {
                        chemicalmodel.setImage1(new File(String.valueOf(photoPaths.get(0))).getName());
                        chemicalmodel.setImage2(new File(String.valueOf(photoPaths.get(1))).getName());
                    }
                }
                chemicalmodel.setAudio(jobcard + containernumber + new File(AUDIO_FILE_PATH).getName());
                chemicalmodel.setStatus(selectstatus);
                chemicalmodel.setWorkreport(selectreport);
                chemicalmodel.setContainerNo(containernumber);
                if (selectreport.equals("ND")) {
                    LinearLayout scrollViewlinerLayout = (LinearLayout) findViewById(R.id.linearLayoutForm);
                    List<Chemicalser> msg = new ArrayList<>();
                    Chemicalser chemicals = new Chemicalser();
                    //String chemicalst=chemicalssp.getText().toString();
                    String chemicalst = selectchemicals;
                    String serial = serialnumbers.getText().toString();
                    String qtysty = quantityt.getText().toString();
                    chemicals.setChemicals(chemicalst);
                    chemicals.setQuatity(qtysty);
                    chemicals.setSerialNumber(serial);
                    msg.add(chemicals);
//                    addchemicals.remove(0);
                    for (int i = 0; i < scrollViewlinerLayout.getChildCount(); i++) {
                        Chemicalser chemicals1 = new Chemicalser();
                        EditText quantityedit;
                        EditText serialno;
                        MaterialSpinner chemicalsedit;
                        LinearLayout innerLayout = (LinearLayout) scrollViewlinerLayout.getChildAt(i);
                        chemicalsedit = (MaterialSpinner) innerLayout.findViewById(R.id.chemicalsedit);
                        quantityedit = (EditText) innerLayout.findViewById(R.id.quantityedit);
                        serialno = (EditText) innerLayout.findViewById(R.id.serialno);

//                        String chemicals11 = selectchemicals1;
                        String chemicals11 = addchemicals.get(i);

                        String quantity1 = quantityedit.getText().toString();
                        String serial1 = serialno.getText().toString();
                        if (chemicals11.equals("*Select Chemicals")) {
                            toasts.ShowErrorToast("Enter chemical");
                            bool = true;
                            break;
                        } else if (quantity1.equals("")) {
                            toasts.ShowErrorToast("Enter quantity");
                            bool = true;
                            break;
                        }
                        chemicals1.setChemicals(chemicals11);
                        chemicals1.setQuatity(quantity1);
                        chemicals1.setSerialNumber(serial1);
                        msg.add(chemicals1);

                    }
                    chemicalmodel.setChemicals1(msg);

                } else {

                    LinearLayout scrollViewlinerLayout = (LinearLayout) findViewById(R.id.linearLayoutForm);
                    List<Chemicalser> msg = new ArrayList<>();
                    Chemicalser chemicals = new Chemicalser();
                    //String chemicalst=chemicalssp.getText().toString();
                    String chemicalst = selectchemicals;
                    String serial = serialnumbers.getText().toString();
                    String qtysty = quantityt.getText().toString();
                    chemicals.setChemicals(chemicalst);
                    chemicals.setQuatity(qtysty);
                    chemicals.setSerialNumber(serial);
                    msg.add(chemicals);

                    for (int i = 0; i < scrollViewlinerLayout.getChildCount(); i++) {
                        Chemicalser chemicals1 = new Chemicalser();
                        EditText quantityedit;
                        EditText serialno;
                        MaterialSpinner chemicalsedit;
                        LinearLayout innerLayout = (LinearLayout) scrollViewlinerLayout.getChildAt(i);
                        chemicalsedit = (MaterialSpinner) innerLayout.findViewById(R.id.chemicalsedit);
                        quantityedit = (EditText) innerLayout.findViewById(R.id.quantityedit);
                        serialno = (EditText) innerLayout.findViewById(R.id.serialno);
                        String chemicals11 = addchemicals.get(i);
                        String quantity1 = quantityedit.getText().toString();
                        String serial1 = serialno.getText().toString();
                        if (chemicals11.equals("*Select Chemicals")) {
                            toasts.ShowErrorToast("Enter chemical");
                            break;
                        } else if (quantity1.equals("")) {
                            toasts.ShowErrorToast("Enter quantity");
                            break;
                        } else if (serial1.equals("")) {
                            toasts.ShowErrorToast("Enter Serial Number");
                            break;
                        }
                        chemicals1.setChemicals(chemicals11);
                        chemicals1.setQuatity(quantity1);
                        chemicals1.setSerialNumber(serial1);
                        msg.add(chemicals1);

                    }
                    chemicalmodel.setChemicals1(msg);

                }

            }
            if (bool == false) {
                final ProgressDialog loading = ProgressDialog.show(JobCardFeedbackActivity.this, "Fumigation", "Loading...", false, false);
                APIService service = RetrofitClient.getApiService();
                Call<UploadActivity> call;
                call = service.upload(chemicalmodel);
                call.enqueue(new Callback<UploadActivity>() {
                    @Override
                    public void onResponse(Call<UploadActivity> call, Response<UploadActivity> response) {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            showAlertbox();
                         //  SuccessRegister(response.body().getData());
                        } else if (response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox(getString(R.string.server_error));
                        } else if (response.body().equals("null")) {
                            StyleableToast st = new StyleableToast(JobCardFeedbackActivity.this, "Please Enter ADD Button", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(JobCardFeedbackActivity.this.getResources().getColor(R.color.colorPrimary));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();
                        } else {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }
                    }


                    @Override
                    public void onFailure(Call<UploadActivity> call, Throwable t) {
                        loading.dismiss();
                        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                                JobCardFeedbackActivity.this).create();

                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.alertboxlinear, null);
                        alertDialogbox.setView(dialogView);

                        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                        Button okay = (Button) dialogView.findViewById(R.id.okay);
                        log.setText("Upload Successfully");
                        okay.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View arg0) {
                                // TODO Auto-generated method stub
                                Intent go = new Intent(JobCardFeedbackActivity.this, SupervisorHomeActivity.class);
                                go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(go);
                                alertDialogbox.dismiss();
                                finish();
                            }
                        });
                        alertDialogbox.show();
                        t.printStackTrace();
                        // box.showAlertbox(getResources().getString(R.string.server_error));
                    }

                });
            }
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
        }

    }

    private void showAlertbox() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                JobCardFeedbackActivity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.multiplechooser, null);


        alertDialogbox.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(false);
        alertDialogbox.setView(dialogView);
        listView=(ListView)dialogView.findViewById(R.id.listview) ;
        submit1=(Button)dialogView.findViewById(R.id.submit1) ;
        submit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                selectlist= new ArrayList<TW>(viewAllNameAdapter.getWork());

                selectlist=new ArrayList<TW>(viewAllNameAdapter.getWork()) {
                };


                selectlist1();
                alertDialogbox.dismiss();
            }
        });
       // new allnames().execute();
        allnames1();

        alertDialogbox.show();
    }

    private void allnames1() {
        try {
            final ProgressDialog loading = ProgressDialog.show(JobCardFeedbackActivity.this, "Fumigation", "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<GetAllNameData> call = service.getallname(branchname,bramchcode);
            call.enqueue(new Callback<GetAllNameData>() {
                @Override
                public void onResponse(Call<GetAllNameData> call, Response<GetAllNameData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data= response.body().getData();
                            viewAllNameAdapter=new ViewAllNameAdapter(JobCardFeedbackActivity.this,data);
                            listView.setAdapter(viewAllNameAdapter);

                        } else if (response.body().getResult().equals("NotSuccess")) {
                            final AlertDialog alertDialogbox = new AlertDialog.Builder(
                                    JobCardFeedbackActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.alertboxlinear, null);
                            alertDialogbox.setView(dialogView);

                            TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                            Button okay = (Button) dialogView.findViewById(R.id.okay);
                            log.setText("No Material");
                            okay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View arg0) {
                                    // TODO Auto-generated method stub
                                    Intent go = new Intent(JobCardFeedbackActivity.this, SupervisorHomeActivity.class);
                                    go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(go);
                                    alertDialogbox.dismiss();
                                    finish();
                                }
                            });
                            alertDialogbox.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<GetAllNameData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void selectlist1() {
        try {
            workReportDataValues=new WorkReportDataValues();
            workReportDataValues.setJobcardid(jobcard);
            workReportDataValues.setContainerNo(containernumber);
            workReportDataValues.setTW(selectlist);
            final ProgressDialog loading = ProgressDialog.show(JobCardFeedbackActivity.this, "Fumigation", "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<WorkReportresponse> call = service.workreport(workReportDataValues);
            call.enqueue(new Callback<WorkReportresponse>() {
                @Override
                public void onResponse(Call<WorkReportresponse> call, Response<WorkReportresponse> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {

                            SuccessRegister(response.body().getData());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }
                @Override
                public void onFailure(Call<WorkReportresponse> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SuccessRegister(Data data) {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                JobCardFeedbackActivity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxlinear, null);
        alertDialogbox.setView(dialogView);

        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText("Upload Successfully");
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                    Intent go = new Intent(JobCardFeedbackActivity.this, SupervisorHomeActivity.class);
                    go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(go);
                    alertDialogbox.dismiss();
                    finish();
            }
        });
        alertDialogbox.show();
    }

    private void ChecInterNet(String from) {
        if (from.equals("audio"))
            UplaodAudioFiles();
        else box.showAlertbox(getResources().getString(R.string.no_internet));

    }

    private void UplaodAudioFiles() {
        try {

            if (!loading.isShowing())
                loading.show();
            File audioFile = new File(AUDIO_FILE_PATH);
            //String FilterType = "audio";
            String type = URLConnection.guessContentTypeFromName(jobcard + containernumber + audioFile.getName());
            //  ProgressRequestBody fileBody = new ProgressRequestBody(audioFile, JobCardFeedbackActivity.this);
            RequestBody requestBody = RequestBody.create(MediaType.parse(type), audioFile);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("audio", jobcard + containernumber + audioFile.getName(), requestBody);
            APIService service = RetrofitClient.getApiUplaodService();
            Call<AudioUploadResult> request = service.audioimage(filePart);
            request.enqueue(new Callback<AudioUploadResult>() {
                @Override
                public void onResponse(Call<AudioUploadResult> call, Response<AudioUploadResult> response) {
                    loading.dismiss();
                    if (response.body().getMessage().equals("Success.")) {
                        box.showAlertbox("Audio Uploaded Successfully");
                        editor.putBoolean("isFilesAdded", true);
                        editor.commit();
                        editor.apply();
                    } else {
                        box.showAlertbox(getString(R.string.server_error));
                    }
                }
                @Override
                public void onFailure(Call<AudioUploadResult> call, Throwable t) {
                    loading.dismiss();
                    Log.v("Upload Exception", t.getMessage());
                    t.printStackTrace();
                    box.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception ex) {
            if (!loading.isShowing())
                loading.dismiss();
            Log.v("Upload Exception", ex.getMessage());
            box.showAlertbox(getString(R.string.server_error));
        }

    }


    private void AudioPicker() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.RECORD_AUDIO)) {
            OpenAudioRecorder();
        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_audio_recorder), RC_AUDIO_REC_PREM, Manifest.permission.RECORD_AUDIO);
        }
    }

    private void OpenAudioRecorder() {
        AndroidAudioRecorder.with(JobCardFeedbackActivity.this)
                // Required
                .setFilePath(AUDIO_FILE_PATH)
                .setColor(ContextCompat.getColor(this, R.color.red))
                .setRequestCode(REQUEST_RECORD_AUDIO)
                // Optiona          `l
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.STEREO)
                .setSampleRate(AudioSampleRate.HZ_48000)
                .setAutoStart(false)
                .setKeepDisplayOn(true)

                // Start recording
                .record();
        audio = new File(AUDIO_FILE_PATH);
        mAudio_layout.setVisibility(View.VISIBLE);
    }

    private void UploadImages() {
        try {

            if (!loading.isShowing())
                loading.show();

            APIService service = RetrofitClient.getApiUplaodService();
            if (i >= photoPaths.size()) {
                return; //loop is finished;
            }
            File imageFile = new File(String.valueOf(photoPaths.get(i)));
            Uri uri = Uri.fromFile(imageFile);
            String type = URLConnection.guessContentTypeFromName(imageFile.getName());
            //ProgressRequestBody fileBody = new ProgressRequestBody(imageFile, JobCardFeedbackActivity.this);
            RequestBody requestBody = RequestBody.create(MediaType.parse(type), imageFile);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", imageFile.getName(), requestBody);
            final Call<ImageUploadResult> request = service.uploadImage(filePart);
            request.enqueue(new Callback<ImageUploadResult>() {
                @Override
                public void onResponse(Call<ImageUploadResult> call, Response<ImageUploadResult> response) {
                    i++;
                    UploadImages();
                    if (response.body().getMessage().equals("Success.")) {
                        if (i == photoPaths.size()) {
                            loading.dismiss();
                            editor.putBoolean("isFilesAdded", true);
                            editor.commit();
                            editor.apply();
                            //  image1.setImageResource(R.drawable.image_upload);
                            //   image2.setImageResource(R.drawable.image_upload);
                            box.showAlertbox("Images Uploaded Successfully");
                        }
                    } else {
                        box.showAlertbox(getString(R.string.server_error));
                    }
                }
                @Override
                public void onFailure(Call<ImageUploadResult> call, Throwable t) {
                    Log.v("Upload Exception", t.getMessage());
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getString(R.string.server_error));
                }

            });
        } catch (Exception ex) {
            if (loading.isShowing())
                loading.dismiss();
            Log.v("Exception", ex.getMessage());
            box.showAlertbox(getString(R.string.server_error));
        }
    }


    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 49374) {
            super.onActivityResult(requestCode, resultCode, data);
            //Log.e("val", "requestCode ->  " + requestCode+"  resultCode "+resultCode);
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            serialnumber = result.getContents();

            if (result != null) {
                // Integer quanti=Integer.valueOf(quantityedit.getText().toString());

//                if (serialnumbers.getText().length() == 0) {
                //if qrcode has nothing in it
                if (result.getContents() == null) {
                    //Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
                } else {
                    if (addlinear != false) {
                        try {
                            Log.d(TAG, "onActivityResult: serialnumber" + serialnumber);
                            //if (quatity!=null)
                            String tempcode = serialno.getText().toString();
                            String code = "";
                            if (!TextUtils.isEmpty(tempcode)) {

                                code = tempcode.concat("," + serialnumber);
                            } else {
                                code = code.concat(serialnumber);
                            }
                            serialno.setText(code);
                            i3++;

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Log.d(TAG, "onActivityResult: serialnumber" + serialnumber);
                            //if (quatity!=null)
                            String tempcode = serialnumbers.getText().toString();
                            String code = "";
                            if (!TextUtils.isEmpty(tempcode)) {

                                code = tempcode.concat("," + serialnumber);
                            } else {
                                code = code.concat(serialnumber);
                            }
                            serialnumbers.setText(code);
                            i2++;

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    //if qr contains data


                }
//                } else {
//
//                    final LinearLayout linearLayoutForm = (LinearLayout) findViewById(R.id.linearLayoutForm);
//                    scrollView1.setVisibility(View.VISIBLE);
//                    scanning.setVisibility(View.GONE);
//                    scanning2.setVisibility(View.VISIBLE);
//                    final LinearLayout newView = (LinearLayout) getLayoutInflater().inflate(R.layout.chemicals, null);
//                    newView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//                    ImageButton btnRemove = (ImageButton) newView.findViewById(R.id.btnRemove);
//                    MaterialSpinner chemicals_spin = (MaterialSpinner) newView.findViewById(R.id.chemicalsedit);
//                    EditText chemicals_serial = (EditText) newView.findViewById(R.id.quantityedit);
//                    EditText serialno1 = (EditText)newView.findViewById(R.id.serialno);
//                    serialno1.setText(serialnumber);
//                    chemicallist1 = new ArrayList<>();
//                    chemicallist1.add("*Select Chemicals");
//                    chemicallist1.add("Phostoxin");
//                    chemicallist1.add("Talunex Tablets");
//                    chemicallist1.add("Topex Applicator");
//                    chemicals_spin.setItems(chemicallist1);
//
//                    btnRemove.setOnClickListener(new View.OnClickListener() {
//
//                        @Override
//                        public void onClick(View v) {
//                            scanning.setVisibility(View.VISIBLE);
//                            scanning2.setVisibility(View.GONE);
//                            linearLayoutForm.removeView(newView);
//                        }
//                    });
//                    linearLayoutForm.addView(newView);
//
//
//                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else if (requestCode == 1) {

//            if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
//                Uri imageUri = CropImage.getPickImageResultUri(this, data);
//                if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
//                    // request permissions and handle the result in onRequestPermissionsResult()
//                    mCropImageUri = imageUri;
//                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
//                } else {
//                    // no permissions required or already grunted, can start crop image activity
//                    startCropImageActivity(imageUri);
//                }
//
//                if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
//                    if (resultCode == RESULT_OK) {
//                        ((ImageView) findViewById(R.id.image1)).setImageURI(result.getUri());
//                        Toast.makeText(this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
//                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                        Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
//                    }
//                }

            //}
            if (resultCode == Activity.RESULT_OK && data != null) {
                photoPaths = new ArrayList<>();
                photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                SetImages(photoPaths);
            }


        } else {
            if (resultCode == Activity.RESULT_OK) {
                SetAudio();
            }

        }
//                Bitmap photo = (Bitmap) data.getExtras().get("data");
//                image1.setImageBitmap(photo);

//            switch (requestCode){
//                case IMAGE_PICKER:
//                    if (resultCode == Activity.RESULT_OK) {
//                        photoPaths = new ArrayList<>();
//                        photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
//                        SetImages(photoPaths);
//                    }
//                    break;
//            }
//                if (resultCode == Activity.RESULT_OK) {
//                    SetAudio();
//                }
    }

    private Uri getImageUri(Context applicationContext, Bitmap mphoto) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        mphoto.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        path = MediaStore.Images.Media.insertImage(applicationContext.getContentResolver(), mphoto, "Title", null);
        return Uri.parse(path);

    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    private void SetImages(ArrayList<String> photoPaths) {
        if (!photoPaths.isEmpty()) {
            if (photoPaths.size() == 1) {
                Glide.with(JobCardFeedbackActivity.this)
                        .load(new File(photoPaths.get(0)))
                        .apply(RequestOptions.centerCropTransform()
                                .dontAnimate()
                                .override(imageSize, imageSize)
                                .placeholder(droidninja.filepicker.R.drawable.image_placeholder))
                        .thumbnail(0.5f)
                        .into(image1);

                image2.setImageResource(R.drawable.ic_add_a_photo_black_24dp);


            } else if (photoPaths.size() == 2) {
                Glide.with(JobCardFeedbackActivity.this)
                        .load(new File(photoPaths.get(0)))
                        .apply(RequestOptions.centerCropTransform()
                                .dontAnimate()
                                .override(imageSize, imageSize)
                                .placeholder(droidninja.filepicker.R.drawable.image_placeholder))
                        .thumbnail(0.5f)
                        .into(image1);

                Glide.with(JobCardFeedbackActivity.this)
                        .load(new File(photoPaths.get(1)))
                        .apply(RequestOptions.centerCropTransform()
                                .dontAnimate()
                                .override(imageSize, imageSize)
                                .placeholder(droidninja.filepicker.R.drawable.image_placeholder))
                        .thumbnail(0.5f)
                        .into(image2);

            } else {
                image1.setImageResource(R.drawable.ic_add_a_photo_black_24dp);
                image2.setImageResource(R.drawable.ic_add_a_photo_black_24dp);

            }
        }
    }


    private void SetAudio() {
        audio = new File(AUDIO_FILE_PATH);
        if (!audio.exists()) {
            Toast.makeText(JobCardFeedbackActivity.this, "Record an audio file before playing", Toast.LENGTH_LONG).show();
        } else {
            AudioWife.getInstance()
                    .init(JobCardFeedbackActivity.this, Uri.fromFile(audio))
                    .setPlayView(mPlayMedia)
                    .setPauseView(mPauseMedia)
                    .setSeekBar(mMediaSeekBar)
                    .setRuntimeView(mRunTime)
                    .setTotalTimeView(mTotalTime);

            AudioWife.getInstance().addOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.reset();
                }
            });
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
//        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
//                Intent cameraIntent = new
//                        Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(cameraIntent, CAMERA_REQUEST);
//            } else {
//                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
//            }
//        }
    }



    private void startCropImageActivity(Uri mCropImageUri) {
        CropImage.activity(mCropImageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);

}

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }


    private class allnames extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(JobCardFeedbackActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            allnames.clear();
//            descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//            quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//            productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);


        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = "select name  FROM ERPView where BranchName ='"+branchname+"'";
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    allnames.add(resultSet.getString("Name"));


                }
                if (!allnames.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
//                viewAllNameAdapter=new ViewAllNameAdapter(JobCardFeedbackActivity.this,allnames);
//                listView.setAdapter(viewAllNameAdapter);

            } else if (s.equals("empty")) {
                box.showAlertbox("No data found!");
            } else {
                box.showAlertbox("Kindly check your Internet Connection");
            }
        }
    }

    private class AddChemicals extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(JobCardFeedbackActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            final LinearLayout newView = (LinearLayout) getLayoutInflater().inflate(R.layout.product, null);
            productnames.clear();
//            descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//            quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//            productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);


        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = "Select ProductName from StockMaster ";
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    productnames.add(resultSet.getString("ProductName"));
                }
                if (!productnames.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                productnames.add(0, "Select ProductName");
                if(chemicals_spin == null){
                    chemicalssp.setItems(productnames);
                }else{
                    chemicals_spin.setItems(productnames);
                }


            } else if (s.equals("empty")) {
                box.showAlertbox("No data found!");
            } else {
                box.showAlertbox("Kindly check your Internet Connection");
            }
        }
    }
}

