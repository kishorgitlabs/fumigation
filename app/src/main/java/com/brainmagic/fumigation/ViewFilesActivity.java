package com.brainmagic.fumigation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


import com.halilibo.bettervideoplayer.subtitle.CaptionsView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.view.TransformImageView;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import adapter.ContainerDetailsAdapter;
import alert.Alertbox;
import api.models.jobcardcontainer.ContainerData;
import api.models.viewreportid.ViewReportId;
import api.models.viewreportid.ViewReportIdData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import logout.Logout;
import network.NetworkConnection;
import nl.changer.audiowife.AudioWife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.senab.photoview.PhotoView;

public class ViewFilesActivity extends AppCompatActivity {
    private ImageView  play_btn, pause_btn;
    private ImageView image1;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private int id;
    private ViewReportId data;
    private Alertbox box = new Alertbox(ViewFilesActivity.this);
    private View mPlayMedia;
    private View mPauseMedia;
    private SeekBar mMediaSeekBar;
    private TextView rename;
    private TextView mRunTime;
    private View customView;
    private TextView mTotalTime;
    private LinearLayout mRelativeLayout;
    private LinearLayout mAudio_layout;
    private static final int REQUEST_RECORD_AUDIO = 0;
    public static final int RC_AUDIO_REC_PREM = 456;
    public static MediaPlayer mediaPlayer = null;
    private ImageView menu;
    private PopupWindow mPopupWindow;
    PhotoView photoView;
    private File audio1;
    private TextView dateview,timeview;
    private File audio;
    private static final String AUDIO_FILE_PATH =
            Environment.getExternalStorageDirectory().getPath() + "/recorded_audio.wav";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_files);
        mediaPlayer = new MediaPlayer();
        menu = (ImageView) findViewById(R.id.menu);
        image1 = (ImageView) findViewById(R.id.image1);
        mPlayMedia = findViewById(R.id.play_btn);
        mPauseMedia = findViewById(R.id.pause_btn);
        mMediaSeekBar = (SeekBar) findViewById(R.id.media_seekbar);
        mRunTime = (TextView) findViewById(R.id.run_time);
        mTotalTime = (TextView) findViewById(R.id.total_time);
        dateview = (TextView) findViewById(R.id.dateview);
        timeview = (TextView) findViewById(R.id.timeview);
        mAudio_layout = (LinearLayout) findViewById(R.id.audio_layout);
        mRelativeLayout = (LinearLayout) findViewById(R.id.imagecapture);
        rename=(TextView) findViewById(R.id.rename);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        // Inflate the custom layout/view
        customView = inflater.inflate(R.layout.pop_photo, null);

        // Initialize a news instance of popup window
        mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);

        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }

        // Get a reference for the custom view close button
        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.close);

        // Set a click listener for the popup window close button
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                mPopupWindow.dismiss();
            }
        });
        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ViewFilesActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(ViewFilesActivity.this).log_out();
                                return true;
                            case R.id.changepassword:
                                Intent i2=new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;
                            case R.id.attendance:
                                Intent i3=new Intent(getApplicationContext(), AttendanceViewNewActivity.class);
                                startActivity(i3);
                                return true;
                            case R.id.jobcard:
                                Intent i4=new Intent(getApplicationContext(), Jobcarddetails.class);
                                startActivity(i4);
                                return true;
                            case R.id.visitreport:
                                Intent i5=new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i5);
                                return true;
                            case  R.id.home:
                                Intent i6=new Intent(getApplicationContext(), SupervisorHomeActivity.class);
                                startActivity(i6);
                                return true;


                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menuviewvisit);
                popupMenu.show();
                ;
            }
        });
        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
                photoView = (PhotoView) customView.findViewById(R.id.pop_photo);
                Picasso.with(ViewFilesActivity.this).load("http://fumigation.brainmagicllc.com/Images/" + data.getImage())
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE).into(photoView);
            }

        });
        // id =myshare.getInt("id",0);
        id = getIntent().getIntExtra("id", 0);
        checkInternet();
    }

    private void checkInternet() {
        NetworkConnection networkConnection = new NetworkConnection(ViewFilesActivity.this);
        if (networkConnection.CheckInternet()) {
            GetFiles();
        } else {
            box.showAlertbox("No Internet Connection");
        }
    }

    public static boolean exists(String URLName) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //        HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con =
                    (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void GetFiles() {
        try {
            final ProgressDialog loading = ProgressDialog.show(ViewFilesActivity.this, "Fumigation", "Loading...", false, false);
            final APIService service = RetrofitClient.getApiService();
            Call<ViewReportIdData> call = service.viewreportid(id);
            call.enqueue(new Callback<ViewReportIdData>() {
                @Override
                public void onResponse(Call<ViewReportIdData> call, Response<ViewReportIdData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data = response.body().getData();
                         //   setAudio("http://fumigation.brainmagicllc.com/Audio/"+data.getAudio());
                            rename.setText(String.valueOf(data.getRemarks()));
                            if(data.getDate() !=null){
                                String[] date =String.valueOf(data.getDate()).split("T");
                                dateview.setText(date[0]);
                            }
                            if(data.getTime() != null){
                                String[] time =String.valueOf(data.getTime()).split(":");
                                String times=time[0]+":"+time[1];
                                timeview.setText(times);
                            }

                            if(data.getImage().equals("")){
                                Picasso.with(ViewFilesActivity.this).load(R.drawable.noimage)
                                        .resize(250, 250)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .centerCrop().into(((ImageView) findViewById(R.id.image1)));
                            }else{
                                Picasso.with(ViewFilesActivity.this).load("http://fumigation.brainmagicllc.com/Images/" + data.getImage())
                                        .resize(250, 250)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .centerCrop().into(((ImageView) findViewById(R.id.image1)));
                            }
                            setAudio();


//                            image1.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
//                                    customView = inflater.inflate(R.layout.pop_photo, null);
//                                    mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
//                                    ImageButton closeButton = (ImageButton) customView.findViewById(R.id.close);
//                                    mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
//                                    photoView = (PhotoView) customView.findViewById(R.id.pop_photo);
//                                    Picasso.with(ViewFilesActivity.this).load("http://fumigation.brainmagicllc.com/Images/" + data.getImage())
//                                            .memoryPolicy(MemoryPolicy.NO_CACHE )
//                                            .networkPolicy(NetworkPolicy.NO_CACHE).into(photoView);
//                                    closeButton.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//                                            mPopupWindow.dismiss();
//                                        }
//                                    });
//                                }
//                            });



                            mPlayMedia.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    audio = new File("http://fumigation.brainmagicllc.com/Audio/" + data.getAudio());
                                    if (audio.exists()) {
                                        box.showAlertbox("File Not Exists");
                                    } else {
                                        AudioWife.getInstance()
                                                .init(ViewFilesActivity.this, Uri.parse("http://fumigation.brainmagicllc.com/Audio/" + data.getAudio()))
                                                .setPlayView(mPlayMedia)
                                                .setPauseView(mPauseMedia)
                                                .setSeekBar(mMediaSeekBar)
                                                .setRuntimeView(mRunTime)
                                                .setTotalTimeView(mTotalTime);

                                        AudioWife.getInstance().addOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                            @Override
                                            public void onCompletion(MediaPlayer mp) {
                                                try{
                                                    AudioWife.getInstance().release();



                                                }catch (Exception e){
                                                    e.printStackTrace();
                                                }


                                            }
                                        });

                                    }
//                                        if (exists(data.getAudio1())) {
//                                            AudioWife.getInstance().init(ViewFilesActivity.this,
//                                                    Uri.parse("http://fumigation.brainmagicllc.com/Audio/" + data.getAudio()))
//                                                    .setPlayView(mPlayMedia)
//                                                    .setPauseView(mPauseMedia)
//                                                    .setSeekBar(mMediaSeekBar)
//                                                    .setRuntimeView(mRunTime)
//                                                    .setTotalTimeView(mTotalTime);
//
//                                            AudioWife.getInstance().addOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                                                @Override
//                                                public void onCompletion(MediaPlayer mp) {
//                                                    Toast.makeText(getBaseContext(), "Completed", Toast.LENGTH_SHORT).show();
//                                                    // do you stuff.
//                                                    mp.release();
//                                                }
//                                            });
//                                        } else {
//
//                                            box.showAlertbox("File Not Exists");
//
//                                        }

                                }
                            });

                        } else if (response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox("Not Success");

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<ViewReportIdData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setAudio() {
        if (exists(data.getAudio1())) {
            AudioWife.getInstance().init(ViewFilesActivity.this,
                    Uri.parse("http://fumigation.brainmagicllc.com/Audio/" + data.getAudio()))
                    .setPlayView(mPlayMedia)
                    .setPauseView(mPauseMedia)
                    .setSeekBar(mMediaSeekBar)
                    .setRuntimeView(mRunTime)
                    .setTotalTimeView(mTotalTime);

            AudioWife.getInstance().addOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    Toast.makeText(getBaseContext(), "Completed", Toast.LENGTH_SHORT).show();
                    // do you stuff.
                    mp.release();
                }
            });
        } else {

            box.showAlertbox("File Not Exists");

        }
    }


    private void ViewMedia() {
        File audio1;
        File fle = new File(data.getAudio1());
        audio1 = fle;
        if (audio1.exists()) {
            Toast.makeText(ViewFilesActivity.this, "Record an audio file before playing", Toast.LENGTH_LONG).show();

        } else {
            try {
                AudioWife.getInstance()
                        .init(ViewFilesActivity.this, Uri.fromFile(audio1))
                        .setPlayView(mPlayMedia)
                        .setPauseView(mPauseMedia)
                        .setSeekBar(mMediaSeekBar)
                        .setRuntimeView(mRunTime)
                        .setTotalTimeView(mTotalTime);

                AudioWife.getInstance().addOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if(mp != null){
                            mp.release();
                        }

                    }
                });

            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }
}

