package com.brainmagic.fumigation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import adapter.ProductAdapter;
import api.models.materialrequest.request.MaterialreqData;
import api.models.materialrequest.request.Materialrequest;
import api.models.materialrequest.response.Data;
import api.models.materialrequest.response.MaterialData;
import persistance.ServerConnection;
import alert.Alertbox;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import logout.Logout;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class  MaterialRequestActivity extends AppCompatActivity {
    //intialize the Details
    private EditText description, quantity;
    private MaterialSpinner productname, brsnchsp, productUnit;
    private Alertbox box = new Alertbox(this);
    private Button submit, add, addproductname;
    //intialize the local Storage...
    private SharedPreferences myshare;
    private ImageView back, menu;
    private SharedPreferences.Editor editor;
    private ScrollView scrollView1;
    private String formattedDate, formattedDate1;
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    private ProgressDialog loading;
    private List<String> productnames, branchdata, allnamedata, productdata, locationlist;
    private String selectproduct, branchname, supervisorename, selectdynamic, selectallname, branchcode, selectproductunit, selectproductdynamic;
    private Toasts toasts = new Toasts(this);
    private EditText descriptiondy, quantitydy, c;
    private MaterialSpinner productdy, allname, productUnitdy, selectlocate;
    private MaterialreqData chemicalmodel = new MaterialreqData();
    private String requested;
    private TextView request, date;
    public boolean addedproduct = false;
    private ListView list;
    private ProductAdapter productAdapter;
    List<Materialrequest> msg = new ArrayList<>();
    private String branch,locate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_request);
        //local Database Declaration
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        productnames = new ArrayList<>();
        branchdata = new ArrayList<>();
        allnamedata = new ArrayList<>();
        productdata = new ArrayList<>();
        locationlist = new ArrayList<>();
        supervisorename = myshare.getString("name", "");
        branchname = myshare.getString("branchname", "");
        branchcode = myshare.getString("branchcode", "");
        menu = (ImageView) findViewById(R.id.menu);
        date = (TextView) findViewById(R.id.dateformat);
        quantity = (EditText) findViewById(R.id.quantity);
        list = (ListView) findViewById(R.id.listmat);
        productname = (MaterialSpinner) findViewById(R.id.productname);
        productUnit = (MaterialSpinner) findViewById(R.id.productUnit);
        selectlocate = (MaterialSpinner) findViewById(R.id.selectlocate);
        addproductname = (Button) findViewById(R.id.addproduct);
        submit = (Button) findViewById(R.id.matsubmit);
        scrollView1 = (ScrollView) findViewById(R.id.scrollView1);
        back = (ImageView) findViewById(R.id.back);
        request = (TextView) findViewById(R.id.request);
        Calendar cal = Calendar.getInstance();
        //date
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        formattedDate = df.format(c);
        SimpleDateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
        formattedDate1 = df1.format(c);
        date.setText(formattedDate1);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                onBackPressed();
            }
        });
        productname.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                //Get the product
                selectproduct = item.toString();
                //Get the description
                new description1().execute();
            }
        });

        selectlocate.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                locate=item.toString();
            }
        });
        productUnit.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectproductunit = item.toString();
            }
        });
//        brsnchsp.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                selectbranch = item.toString();
//
//            }
//        });
//        allname.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                selectallname=item.toString();
//            }
//        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (msg.isEmpty()) {
                    toasts.ShowErrorToast("No Product Added");
                } else {
                    //assign the method......
                    checkInternet1();
                }
            }
        });
        addproductname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productname.getText().toString().equals("Select ProductName")) {
                    toasts.ShowErrorToast("Enter Product Name");
                } else if (productUnit.getText().toString().equals("Select ProductUnit")) {
                    toasts.ShowErrorToast("Enter Product Unit");
                } else if (quantity.getText().toString().equals("")) {
                    toasts.ShowErrorToast("Enter Quantity");
                } else {
                    //assign the product
                    checkProduct();


                }
            }
        });
//        add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (productname.getText().toString().equals("")) {
//                    toasts.ShowErrorToast("Enter product");
//                }else if (quantity.getText().toString().equals("")) {
//                    toasts.ShowErrorToast("Enter quantity");
//                } else {
//                    final ProgressDialog loading = ProgressDialog.show(MaterialRequestActivity.this, "Fumigation", "Loading...", false, false);
//                    LinearLayout scrollViewlinerLayout = (LinearLayout) findViewById(R.id.linearLayoutForm);
//                    List<Materialrequest> msg = new ArrayList<>();
//                    Materialrequest req = new Materialrequest();
//                    //String chemicalst=chemicalssp.getText().toString();
//                    String productname = selectproduct;
//                    String description1 = description.getText().toString();
//                    String quantity1=quantity.getText().toString();
//                    req.setProductName(productname);
//                    req.setDescription(description1);
//                    req.setReqQty(quantity1);
//                    msg.add(req);
//                    for (int i = 0; i < scrollViewlinerLayout.getChildCount(); i++) {
//                        Materialrequest req1 = new Materialrequest();
//                        EditText description,quantity;
//                        MaterialSpinner productnames;
//                        LinearLayout innerLayout = (LinearLayout) scrollViewlinerLayout.getChildAt(i);
//                        EditText descriptiondy = (EditText) innerLayout.findViewById(R.id.descriptiondynamic);
//                        EditText c = (EditText) innerLayout.findViewById(R.id.quantitydynamic);
//                        MaterialSpinner productdy = (MaterialSpinner) innerLayout.findViewById(R.id.productdynname);
//
//                        String productnamedy = selectdynamic;
//                        String descriptiondy1 = descriptiondy.getText().toString();
//                        String quantitydy1=c.getText().toString();
//                        req1.setProductName(productnamedy);
//                        req1.setDescription(descriptiondy1);
//                        req1.setReqQty(quantitydy1);
//                        msg.add(req1);
//                    }
//
//                    chemicalmodel.setData(msg);
//                    box.showAlertbox("product Added Successfully");
//                    loading.dismiss();
//                }
//            }
//        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(MaterialRequestActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(MaterialRequestActivity.this).log_out();
                                return true;
                            case R.id.changepassword:
                                Intent i2 = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;
                            case R.id.Viewaterialrequest:
                                AlertViewMaterial();
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menubranchmaterialreq);
                popupMenu.show();

            }

        });

//        addproductname.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                final LinearLayout linearLayoutForm = (LinearLayout) findViewById(R.id.linearLayoutForm);
//                 addedproduct =true;
//
//                scrollView1.setVisibility(View.VISIBLE);
//               /* scanning.setVisibility(View.GONE);
//                scanning2.setVisibility(View.VISIBLE);*/
//                final LinearLayout newView = (LinearLayout) getLayoutInflater().inflate(R.layout.product, null);
//                newView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//                ImageButton btnRemove = (ImageButton) newView.findViewById(R.id.btnRemove);
//               // descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//                quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//                productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);
//                productUnitdy = (MaterialSpinner) newView.findViewById(R.id.productUnitdy);
//                checkInternet2();
//                productdy.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//
//                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                        selectdynamic = item.toString();
//                        new description1().execute();
//                    }
//                });
//                productUnitdy.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                        selectproductdynamic=item.toString();
//                    }
//                });;
//
//                btnRemove.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View v) {
//                        linearLayoutForm.removeView(newView);
//                    }
//                });
//                linearLayoutForm.addView(newView);
//            }
//
//            private void checkInternet2() {
//                NetworkConnection isnet = new NetworkConnection(MaterialRequestActivity.this);
//                if (isnet.CheckInternet()) {
//                    new Orders().execute();
//
//
//                } else {
//
//
//                    box.showAlertbox(getResources().getString(R.string.nointernetmsg));
//                }
//            }
//        });
        //Assign Methods
        checkInternet();


    }

    private void checkProduct() {
        Materialrequest req1 = new Materialrequest();
        req1.setProductName(selectproduct);
        req1.setProductUnit(selectproductunit);
        req1.setReqQty(quantity.getText().toString());
        msg.add(req1);
        productname.setText("Select ProductName");
        productUnit.setText("Product Unit");
        quantity.setText("");
        //Get the Data set to the adapter.........
        productAdapter = new ProductAdapter(MaterialRequestActivity.this, msg);
        list.setAdapter(productAdapter);

    }

    private void AlertViewMaterial() {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                MaterialRequestActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxviewmaterial, null);
        alertDialogbox.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
        alertDialogbox.setCanceledOnTouchOutside(true);
        alertDialogbox.setView(dialogView);
        Button pendingmaterial = (Button) dialogView.findViewById(R.id.textView1);
        Button approvedmaterial = (Button) dialogView.findViewById(R.id.textView2);
        // Button receivedmaterial = (Button) dialogView.findViewById(R.id.textView3);
        pendingmaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("Tiltle", "Pending");
                startActivity(i);
                alertDialogbox.dismiss();

            }
        });

        approvedmaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("Tiltle", "Approved");
                startActivity(i);
                alertDialogbox.dismiss();

            }
        });
        alertDialogbox.show();
//        receivedmaterial.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i=new Intent(getApplicationContext(), ViewMaterialRequestActivity.class).putExtra("Tiltle","Received");
//                startActivity(i);
//                alertDialogbox.dismiss();
//
//            }
//        });
        alertDialogbox.show();
    }


    private void checkInternet1() {
        NetworkConnection isnet = new NetworkConnection(MaterialRequestActivity.this);
        if (isnet.CheckInternet()) {
            //get the method......
            GetMaterialrequest();

        } else {

            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }

    private void GetMaterialrequest() {
        try {
//                LinearLayout scrollViewlinerLayout = (LinearLayout) findViewById(R.id.linearLayoutForm);
//                List<Materialrequest> msg = new ArrayList<>();
//                Materialrequest req = new Materialrequest();
//                //String chemicalst=chemicalssp.getText().toString();
//                String productname = selectproduct;
//                String productunit=selectproductunit;
//                //   String description1 = description.getText().toString();
//                String quantity1 = quantity.getText().toString();
//                req.setProductName(productname);
//                req.setProductUnit(productunit);
//                // req.setDescription(description1);
//                req.setReqQty(quantity1);
//                msg.add(req);
//                for (int i = 0; i < scrollViewlinerLayout.getChildCount(); i++) {
//                    Materialrequest req1 = new Materialrequest();
//                    EditText  quantity;
//                    MaterialSpinner productnames,productcodedynamic;
//                    LinearLayout innerLayout = (LinearLayout) scrollViewlinerLayout.getChildAt(i);
//                    //EditText descriptiondy = (EditText) innerLayout.findViewById(R.id.descriptiondynamic);
//                    quantity = (EditText) innerLayout.findViewById(R.id.quantitydynamic);
//                    productnames = (MaterialSpinner) innerLayout.findViewById(R.id.productdynname);
//                    productcodedynamic = (MaterialSpinner) innerLayout.findViewById(R.id.productUnitdy);
//                    String productnamedy = selectdynamic;
//                    String productcodedynamic1=selectproductdynamic;
//                   // String descriptiondy1 = descriptiondy.getText().toString();
//                    String quantitydy1 = quantity.getText().toString();
//                    req1.setProductName(productnamedy);
//                    req1.setProductUnit(productcodedynamic1);
//                    // req1.setDescription(descriptiondy1);
//                    req1.setReqQty(quantitydy1);
//                    msg.add(req1);
//
//                }
            chemicalmodel.setData(msg);
            chemicalmodel.setReqPersonName(supervisorename);
            chemicalmodel.setReqBranchName(branchname);
            chemicalmodel.setReqBranchCode(branchcode);
            //  chemicalmodel.setBranchId(Integer.parseInt(myshare.getString("id","")));
            // chemicalmodel.setRequestTo(requested);

            final ProgressDialog loading = ProgressDialog.show(MaterialRequestActivity.this, "Fumigation", "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<MaterialData> call;
            call = service.material(chemicalmodel);
            call.enqueue(new Callback<MaterialData>() {
                @Override
                public void onResponse(Call<MaterialData> call, Response<MaterialData> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        //assign the method....
                        SuccessRegister(response.body().getData());

                    } else if (response.body().getResult().equals("NotSuccess")) {
                        box.showAlertbox(getString(R.string.server_error));
                    } else {
                        box.showAlertbox(getResources().getString(R.string.server_error));
                    }
                }
                @Override
                public void onFailure(Call<MaterialData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
        }
    }


    private void SuccessRegister(Data data) {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                MaterialRequestActivity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxlinear, null);
        alertDialogbox.setView(dialogView);

        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText("Material Request Successfully.Your Request Number is " + data.getMlist().get(0).getRequestedNo());
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent go = new Intent(MaterialRequestActivity.this, SupervisorHomeActivity.class);
                go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(go);
                alertDialogbox.dismiss();
                finish();
            }
        });
        alertDialogbox.show();
    }


    private void checkInternet() {
        NetworkConnection isnet = new NetworkConnection(MaterialRequestActivity.this);
        if (isnet.CheckInternet()) {
            //Assign Methods
            new Orders().execute();
            //Assign Methods
            new worklocation().execute();
            //  new Branch().execute();
            //  new allname1().execute();

        } else {

            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }


    private class Orders extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MaterialRequestActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            final LinearLayout newView = (LinearLayout) getLayoutInflater().inflate(R.layout.product, null);
            productnames.clear();
//            descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//            quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//            productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);


        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = "Select ProductName from StockMaster ";
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    //Get the Productname
                    productnames.add(resultSet.getString("ProductName"));
                }
                if (!productnames.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                productnames.add(0, "Select ProductName");
                //set the productdetails in productname
                productname.setItems(productnames);
                if (productdy != null)
                    productdy.setItems(productnames);

            } else if (s.equals("empty")) {
                box.showAlertbox("No data found!");
            } else {
                box.showAlertbox("Kindly check your Internet Connection");
            }
        }
    }

    private class description1 extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MaterialRequestActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            productdata.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = "select * from StockMaster where ProductName = '" + selectproduct + "' ";
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    //Get the productdetails
                    productdata.add(resultSet.getString("ProductUnit"));
                }
                if (!productnames.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "error_insertion";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("success")) {
                //set the product in productunit
                productdata.add(0, "Select Product Unit");
                productUnit.setItems(productdata);
                productUnit.setSelectedIndex(0);

            } else if (s.equals("empty")) {
                box.showAlertbox("No data found!");
            } else {
                box.showAlertbox("Kindly check your Internet Connection");
            }
        }
    }

    private class Branch extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MaterialRequestActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = "Select BranchName FROM BranchMaster";
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    branchdata.add(resultSet.getString("BranchName"));

                }
                if (!branchdata.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                branchdata.add(0, "Select Branch");
                brsnchsp.setItems(branchdata);
            } else if (s.equals("empty")) {
                box.showAlertbox("No data found!");
            } else {
                box.showAlertbox("Kindly check your Internet Connection");
            }
        }
    }


    private class allname1 extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MaterialRequestActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = " select Name from ERPView";
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    allnamedata.add(resultSet.getString("Name"));

                }
                if (!allnamedata.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                allnamedata.add(0, "Select Name");
                allname.setItems(allnamedata);
            } else if (s.equals("empty")) {
                box.showAlertbox("No data found!");
            } else {
                box.showAlertbox("No data found!");
            }
        }
    }

    private class worklocation extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MaterialRequestActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = "select Location From WorkDoneLocation where Branch ='"+branchname+"'";
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    //Get the Worklocation...
                    locationlist.add(resultSet.getString("Location"));
                }
                if (!locationlist.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                //get the location set the selectlocate....
                locationlist.add(0, "Select Location");
                selectlocate.setItems(locationlist);
            } else if (s.equals("empty")) {
                box.showAlertbox("No data found!");
            } else {
                box.showAlertbox("No data Found!");
            }
        }
    }
}

