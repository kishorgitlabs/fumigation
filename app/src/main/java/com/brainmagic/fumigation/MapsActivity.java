package com.brainmagic.fumigation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import persistance.ServerConnection;
import alert.Alertbox;
import api.getPolyline;
import history.MapAnimatorHistory;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Alertbox alert = new Alertbox(MapsActivity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private List<LatLng> listLatLng = new ArrayList<>();
    private List<LatLng> listLatLngRoutes = new ArrayList<>();
    private String fromaddress,toaddress,name;
    private Integer tracid,sid;
    private LatLng source,destination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        //tracid=myshare.getInt("trackid",0);
        sid=myshare.getInt("id",0);
        name=myshare.getString("name","");
        fromaddress=getIntent().getStringExtra("fromadd");
        toaddress=getIntent().getStringExtra("toadd");
        source = new LatLng(Double.valueOf(getIntent().getStringExtra("fromlatitude")), Double.valueOf(getIntent().getStringExtra("fromlongitude")));
        destination = new LatLng(Double.valueOf(getIntent().getStringExtra("tolatitude")), Double.valueOf(getIntent().getStringExtra("tolongitude")));
        checkInternet();
    }

    private void checkInternet() {
        NetworkConnection net = new NetworkConnection(MapsActivity.this);
        if (net.CheckInternet()) {
            new GetTravelHistory().execute("select distinct * from coordinate where S_id = '" + sid + "'and saname= '"+name+"'");
        } else {
            alert.showAlertbox("Please check your network connection and try again!");
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    private class GetTravelHistory extends AsyncTask<String, Void, String> {
        private ProgressDialog loading;
        private Connection connection;
        private Statement stmt;
        private ResultSet rset;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = ProgressDialog.show(MapsActivity.this, "Loading", "Please wait", false);
            listLatLng.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            String query = params[0];
            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                Log.v("Query", query);
                rset = stmt.executeQuery(query);
                ArrayList<LatLng> points = new ArrayList<LatLng>();
                while (rset.next()) {
                    double lat = Double.parseDouble(rset.getString("latitude"));
                    double lng = Double.parseDouble(rset.getString("langtitude"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }
                if (points.isEmpty()) {
                    connection.close();
                    stmt.close();
                    rset.close();
                    return "nodata";

                } else {
//                    points.add(0,source);
//                    int size=points.size();
//                    points.add(size,destination);
                    listLatLng.addAll(points);
                    connection.close();
                    rset.close();
                    return "success";
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.v("list", e.getMessage());
                return "error";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            loading.dismiss();
            if (s.equals("error")) {
                alert.showAlertbox("Server Connection Failed");
            } else {
                if (s.equals("nodata")) {
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapsActivity.this);
                    alertDialog.setMessage("This Trip was not travelled");
                    alertDialog.setTitle("Fumigation");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            setUpPolyLine();
                        }
                    });
                    alertDialog.show();
                } else {
                    setUpPolyLine();
                }

            }
        }
    }

    protected void setUpPolyLine() {
        if (source != null && destination != null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://maps.googleapis.com/maps/api/directions/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            getPolyline polyline = retrofit.create(getPolyline.class);

            polyline.getPolylineData(source.latitude + "," + source.longitude, destination.latitude + "," + destination.longitude,"AIzaSyB9kdMz4eGNnXSMSMQ0cGLG7tHq6bNLr18")
                    .enqueue(new Callback<JsonObject>() {

                        @Override
                        public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                            JsonObject gson = new JsonParser().parse(response.body().toString()).getAsJsonObject();
                            try {

                                Single.just(parse(new JSONObject(gson.toString())))
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new io.reactivex.functions.Consumer<List<List<HashMap<String, String>>>>() {
                                            @Override
                                            public void accept(List<List<HashMap<String, String>>> lists) throws Exception {

                                                drawPolyline(lists, source, destination);
                                            }
                                        });

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<JsonObject> call, Throwable t) {
                            Toast.makeText(MapsActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();

    }
    public List<List<HashMap<String, String>>> parse(JSONObject jObject) {

        List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;

        try {

            jRoutes = jObject.getJSONArray("routes");

            /** Traversing all routes */
            for (int i = 0; i < jRoutes.length(); i++) {
                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                List path = new ArrayList<HashMap<String, String>>();

                /** Traversing all legs */
                for (int j = 0; j < jLegs.length(); j++) {
                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                    /** Traversing all steps */
                    for (int k = 0; k < jSteps.length(); k++) {
                        String polyline = "";
                        polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);

                        /** Traversing all points */
                        for (int l = 0; l < list.size(); l++) {
                            HashMap<String, String> hm = new HashMap<String, String>();
                            hm.put("lat", Double.toString(((LatLng) list.get(l)).latitude));
                            hm.put("lng", Double.toString(((LatLng) list.get(l)).longitude));
                            path.add(hm);
                        }
                    }
                    routes.add(path);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception ignored) {
        }
        return routes;
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void drawPolyline(List<List<HashMap<String, String>>> result, LatLng source, LatLng destination) {

        ArrayList<LatLng> points = null;
        PolylineOptions lineOptions = null;
        listLatLngRoutes.clear();
        // Traversing through all the routes
        if (result.size() != 0) {
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();
                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));

                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                this.listLatLngRoutes.addAll(points);
            }
            animatePolyLine(listLatLng, source, destination,listLatLngRoutes);


        } else {
            setUpPolyLine();
        }

    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void animatePolyLine(List<LatLng> listLatLng, LatLng source, LatLng destination, List<LatLng> listLatLngRoutes) {
        if (mMap != null && listLatLng != null && destination != null) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                MapAnimatorHistory.getInstance().animateRoute(MapsActivity.this, mMap, listLatLng, source, destination,fromaddress,toaddress,listLatLngRoutes);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Map not ready", Toast.LENGTH_LONG).show();
        }
    }

}
