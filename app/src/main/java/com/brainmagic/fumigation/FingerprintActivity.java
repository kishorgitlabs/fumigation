package com.brainmagic.fumigation;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.service.carrier.CarrierMessagingService;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.style.ReplacementSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.net.ssl.HttpsURLConnection;

import alert.Alertbox;
import logout.Logout;
import network.NetworkConnection;
import pub.devrel.easypermissions.EasyPermissions;
import service.BGService;
import service.BGServicenormal;




public class FingerprintActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks,ResultCallback<LocationSettingsResult>,
        GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener,com.google.android.gms.location.LocationListener,LocationListener,FingerprintHandler.RunBackgroundService{
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private int REQUEST_CHECK_SETTINGS = 100;
    private static final int TWENTY_MINUTES = 1* 60 * 1000;
    private Intent service;
    private GoogleApiClient mGoogleApiClient;
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    private Double FromLatitude, FromLongitude;
    private String toaddress, fromaddress;
    private int fromYDelta=0;
    private int fromXDelta=325;
    private ProgressDialog mProgressDialog;
    private String City="";
    private Alertbox box = new Alertbox(FingerprintActivity.this);
    private LocationManager mlocationManager;
    private int MIN_DISTANCE_CHANGE_FOR_UPDATES=500;
    private Location mLastLocation;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private KeyStore keyStore;
    private static final String TAG = "FingerprintActivity";
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "androidHive";
    private Cipher cipher;
    private TextView textView,nameedit;
    private String empname,empid,address;
    public static final String APIKEY =  "AIzaSyBRmYkOOy9QhrI53Fp3h_Tt8t7amWNa4Q0";
    private Double latitude,longitude;
    public static final String ACTION_START_FOREGROUND_SERVICE = "ACTION_START_FOREGROUND_SERVICE";
    public static final String ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_FOREGROUND_SERVICE";
    private ImageView menu;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprint);
        // Initializing both Android Keyguard Manager and Fingerprint Manager
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
        //LOCATION SERVICE
        mlocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        menu=(ImageView)findViewById(R.id.menu) ;
        textView = (TextView) findViewById(R.id.errorText);
        nameedit=(TextView)findViewById(R.id.nameedit);

        empname = myshare.getString("name","");
                //empid=getIntent().getStringExtra("id1");
        nameedit.setText(empname);

        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(FingerprintActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(FingerprintActivity.this).log_out();
                                return true;
                            case R.id.jobcard:
                                Intent i=new Intent(getApplicationContext(), ContainerDetailsActivity.class);
                                startActivity(i);
                                return true;
                            case R.id.visitreport:
                                Intent i4=new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i4);
                                return true;

                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menu);
                popupMenu.show();
                ;
            }
        });

        // Check whether the device has a Fingerprint sensor.
        if(!fingerprintManager.isHardwareDetected()){
            /**
             * An error message will be displayed if the device does not contain the fingerprint hardware.
             * However if you plan to implement a default authentication method,
             * you can redirect the user to a default authentication activity from here.
             * Example:
             * Intent intent = new Intent(this, DefaultAuthenticationActivity.class);
             * startActivity(intent);
             */
            textView.setText("Your Device does not have a Fingerprint Sensor");
        }else {
            // Checks whether fingerprint permission is set on manifest
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                textView.setText("Fingerprint authentication permission not enabled");
            }else{
                // Check whether at least one fingerprint is registered
                if (!fingerprintManager.hasEnrolledFingerprints()) {
                    textView.setText("Register at least one fingerprint in Settings");
                }else{
                    // Checks whether lock screen security is enabled or not
                    if (!keyguardManager.isKeyguardSecure()) {
                        textView.setText("Lock screen security not enabled in Settings");
                    }else{
                        generateKey();

                        if (cipherInit()) {
                            FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                            FingerprintHandler helper = new FingerprintHandler(FingerprintActivity.this,this);
                            helper.startAuth(fingerprintManager, cryptoObject);
                        }
                    }
                }
            }
        }
        if (getServicesAvailable()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
            //  Toast.makeText(this, "Google Service Is Available!!", Toast.LENGTH_SHORT).show();

        }
        AskLocationPermission();
    }


    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(AppConstants.UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(AppConstants.FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(AppConstants.DISPLACEMENT);
    }

    protected synchronized void buildGoogleApiClient() {
        try {
            Log.d(TAG, "buildGoogleApiClient: ");
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(this, this).build();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean getServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {

            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
            return false;
        } else {
            Toast.makeText(this, "Cannot Connect To Play Services", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void AskLocationPermission() {
        if(EasyPermissions.hasPermissions(FingerprintActivity.this,Manifest.permission.ACCESS_FINE_LOCATION)){
            if(CheckLocationIsEnabled()){
                startLocationUpdates();
            } else{
                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(mLocationRequest);

                PendingResult<LocationSettingsResult> result =
                        LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @Override
                    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                        final Status status = locationSettingsResult.getStatus();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                startLocationUpdates();
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    status.startResolutionForResult(
                                            FingerprintActivity.this,
                                            REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException e) {
                                    Log.e(TAG, "Exception : " + e.getMessage());
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e(TAG, "Location settings are not satisfied.");
                                break;
                        }
                    }
                });
            }


        }else{
            // Request one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location), MY_PERMISSIONS_REQUEST_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);

        }
    }

    private void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates: ");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if(mGoogleApiClient!=null) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                }
//                if(mLastLocation==null)
                {
                    mProgressDialog= ProgressDialog.show(this,"Location","Getting Your Location",false,false);
                    mlocationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            1000,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES,this);
                    mlocationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            1000,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES,this);
                }
//                else
//                    {
//                    mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
//
//                    CheckInternet();
//                }
//                CheckInternet();
//                mProgressDialog=ProgressDialog.show(this,"Location","Getting Your Location",false,false);
                Log.d(TAG, "startLocationUpdates: ");
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
                Log.d(TAG, "Permission Not Granted");
            }

        } else {
            Log.d(TAG, "startLocationUpdates: else");
            if(mGoogleApiClient!=null){
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            }
//            if(mLastLocation==null)
            {
                mProgressDialog=ProgressDialog.show(this,"Location","Getting Your Locate",false,false);
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                mlocationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        1000,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES,this);
                mlocationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        1000,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES,this);
            }
//            else
//            {
//                mProgressDialog=ProgressDialog.show(this,"Location","Getting Your Location",false,false);
//                CheckInternet();
//            }

        }

    }

    private void CheckInternet() {
        Log.d(TAG, "CheckInternet: lat and long");
        NetworkConnection network = new NetworkConnection(FingerprintActivity.this);
        if (network.CheckInternet()) {
           // relativeLayout.setVisibility(View.VISIBLE);
            Log.d(TAG, "CheckInternet: RelativeLayout");
            ShowCurrentLocationMarker();
        } else {
            Alertbox alert = new Alertbox(FingerprintActivity.this);
            box.showAlertbox("Kindly check your Internet Connection");
          //  retry.setVisibility(View.VISIBLE);
            //relativeLayout.setVisibility(View.GONE);
        }
    }

    private void ShowCurrentLocationMarker() {
        if (mLastLocation != null) {
            Log.d(TAG, "ShowCurrentLocationMarker: latitude "+mLastLocation.getLatitude()+" logitude "+mLastLocation.getLongitude());

            new GeocodeAsyncTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());     // Sets the center of the map to location user
        }
    }

    private boolean CheckLocationIsEnabled() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        if(mGoogleApiClient!=null)
            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);

        if (mLastLocation == null) {
            return false;
        } else {

            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }


    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mProgressDialog.dismiss();
        if (location != null) {
            mLastLocation = location;
            latitude=mLastLocation.getLatitude();
            longitude=mLastLocation.getLongitude();
            editor.putString("latitude",latitude.toString());
            editor.putString("longitude",longitude.toString());
            Log.d(TAG, "onLocationChanged: "+location.getLatitude()+" long "+location.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(location.getLatitude(), location.getLongitude());
            //Or Do whatever you want with your location
        }
        else if(mLastLocation!=null) {
            latitude=mLastLocation.getLatitude();
            longitude=mLastLocation.getLongitude();
            editor.putString("latitude",latitude.toString());
            editor.putString("longitude",longitude.toString());
            Log.d(TAG, "onLocationChanged: "+mLastLocation.getLatitude()+" long "+mLastLocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        }

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }
    @Override
    public void runBackgroundService(String string) {
        if(string.equals("start")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                if (string.equals("start")){
                    ContextCompat.startForegroundService(   FingerprintActivity.this,new Intent(FingerprintActivity.this, BGService.class).setAction(ACTION_START_FOREGROUND_SERVICE));
                }else {
                    stopService(new Intent(FingerprintActivity.this, BGServicenormal.class).setAction(ACTION_STOP_FOREGROUND_SERVICE));
                }

            }
                startService(new Intent(FingerprintActivity.this, BGService.class));
        }
        else {
            stopService(new Intent(FingerprintActivity.this, BGService.class));
        }

    }


//    private class GeocodeAsyncTask extends AsyncTask<Double, Void , Address> {
//        String errorMessage = "";
//
//
//
//        @Override
//        protected Address doInBackground(Double... latlang) {
//            Geocoder geocoder = new Geocoder(FingerprintActivity.this, Locale.getDefault());
//            List<Address> addresses = null;
//            if (geocoder.isPresent()) {
//                try {
//                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
//                    Log.d(TAG, "doInBackground: ************");
//                } catch (IOException ioException) {
//                    errorMessage = "Service Not Available";
//                    Log.e(TAG, errorMessage, ioException);
//                } catch (IllegalArgumentException illegalArgumentException) {
//                    errorMessage = "Invalid Latitude or Longitude Used";
//                    Log.e(TAG, errorMessage + ". " +
//                            "Latitude = " + latlang[0] + ", Longitude = " +
//                            latlang[1], illegalArgumentException);
//                }
//
//                if (addresses != null && addresses.size() > 0)
//                    return addresses.get(0);
//            }
//
//            return null;
//        }
//
//        protected void onPostExecute(Address addresss) {
//
//            if (addresss == null) {
//              //  new GetGeoCodeAPIAsynchTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
//                Log.d(TAG, "onPostExecute: *****"+City);
//            } else {
////                progressBar.setVisibility(View.GONE);
//                Double lat=mLastLocation.getLatitude();
//                Double lon=mLastLocation.getLongitude();
//                String address = addresss.getAddressLine(0); //0 to obtain first possible address
//                editor.putString("FromAddress",address);
//                editor.putString("ToAddress",address);
//                editor.putString("lat", String.valueOf(lat));
//                editor.putString("lon", String.valueOf(lon));
//                editor.commit();
//                editor.apply();
//                City = addresss.getLocality();
//                Log.d(TAG, "onPostExecute: **************************"+City);
//                String state = addresss.getAdminArea();
//                mProgressDialog.dismiss();
//                //create your custom title
////                String title = city + "-" + state;
////                Alertbox alertbox=new Alertbox(Fing.this);
////                alertbox.showAlertboxwithback("Your Current location is "+city);
//
//
//            }
//
//        }
//    }
private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

    String errorMessage = "";


    @Override
    protected Address doInBackground(Double... latlang) {
        Geocoder geocoder = new Geocoder(FingerprintActivity.this, Locale.getDefault());
        List<Address> addresses = null;
        if (geocoder.isPresent()) {
            try {
                addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                Log.d(TAG, "doInBackground: ************");
            } catch (IOException ioException) {
                errorMessage = "Service Not Available";
                Log.e(TAG, errorMessage, ioException);
            } catch (IllegalArgumentException illegalArgumentException) {
                errorMessage = "Invalid Latitude or Longitude Used";
                Log.e(TAG, errorMessage + ". " +
                        "Latitude = " + latlang[0] + ", Longitude = " +
                        latlang[1], illegalArgumentException);
            }

            if (addresses != null && addresses.size() > 0)
                return addresses.get(0);
        } else {
            new GetGeoCodeAPIAsynchTask().execute(latlang[0], latlang[1]);
        }

        return null;
    }

    protected void onPostExecute(Address addresss) {

        if (addresss == null) {
            new GetGeoCodeAPIAsynchTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            Log.d(TAG, "onPostExecute: *****");
        } else {
//                progressBar.setVisibility(View.GONE);
            address = addresss.getAddressLine(0); //0 to obtain first possible address
            editor.putString("FromAddress",address);
            editor.putString("ToAddress",address);
            editor.commit();
            editor.apply();
            City = addresss.getLocality();
            Log.d(TAG, "onPostExecute: **************************"+City);
            String state = addresss.getAdminArea();

            //create your custom title
//                String title = city + "-" + state;
//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertboxwithback("Your Current location is "+city);


        }
    }
}

    private class GetGeoCodeAPIAsynchTask extends AsyncTask<Double, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String[] doInBackground(Double... latlang) {
            String response;
            try {
                String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang[0] + "," + latlang[1] + "&key=" + APIKEY;
                Log.v("URL", URL);
                response = getLatLongByURL(URL);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                address = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(0).getString("long_name");

                editor.putString("FromAddress",address);
                editor.putString("ToAddress",address);
                editor.commit();
                editor.apply();

                City = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(2).getString("long_name");

                String state = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(4).getString("long_name");

                Log.d(TAG, "onPostExecute: "+City);

//                String title = city + "-" + state;

//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertbox("Your Current location is "+city);

//                if (fetchType == FROMADDRESS) {
//
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.requestFocus();
//                    fromaddress = address;
//                    mPickupText.setText(address);
//
//                    editor.putString("FromAddress", fromaddress);
//                    editor.apply();
//                    editor.commit();
//
//                    Log.i("FromAddress1", address);
//                    Log.i("FromAddress2", title);
//                } else {
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.setText(address);
//                    toaddress = address;
//                    editor.putString("ToAddress", toaddress).commit();
//
//                    Log.i("ToAddress1", address);
//                    Log.i("ToAddress2", title);
//
//                }


                Log.d("Address", "" + address);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            progressBar.setVisibility(View.GONE);

        }
    }
    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}


